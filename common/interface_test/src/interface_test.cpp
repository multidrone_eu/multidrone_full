//Test parameters
#include <ros/ros.h>
#include <stdlib.h>
#include <fstream>
#include <interface_test/interface_test.h>


void test_setup(std::string test_id)
{
    ros::NodeHandle pnh_(test_id);
    pnh_.getParam("desired_freq",desired_freq[test_id]);
    pnh_.getParam("max_allowed_time",max_allowed_time[test_id]);
    pnh_.getParam("max_allowed_freq_shift",max_allowed_freq_shift[test_id]);
    pnh_.getParam("max_allowed_latency",max_latency_time[test_id]);

     std::cout << "desired freq is " << desired_freq[test_id] << std::endl;
     std::cout << "max_allowed_time is " << max_allowed_time[test_id] << std::endl;
     std::cout << "max_allowed_freq_shift is " << max_allowed_freq_shift[test_id] << std::endl;
}

void run_test(std::string test_id, ros::Time timestamp, bool &interface_test_flag)
{

  double received;
  static double avg_freq = 0;
  double freq;
  static double latency = 0;
  static double avg_latency = 0;
  received = ros::Time::now().toSec();

  if(first_received[test_id] == 0) {
   first_received[test_id] = received;
   last_received[test_id] = ros::Time::now().toSec();
   freq_min[test_id] = 1000;
   freq_max[test_id] = 0;
   latency_max[test_id]=0;
   test_nr[test_id] = 0;
   count[test_id] = 0;
 }

 if (count[test_id] > 0) {
   freq = 1/(received - last_received[test_id]);
   latency = received - timestamp.toSec();
   latency_max[test_id] = std::max(latency_max[test_id], latency);
   freq_min[test_id] = std::min(freq_min[test_id], freq);
   freq_max[test_id] = std::max(freq_max[test_id], freq);
 }

 if (count[test_id] <= desired_freq[test_id]) {
   freq_sum[test_id] += freq;
   latency_sum[test_id]+=latency;
   count[test_id]++;
  }
  else if ( (avg_freq = freq_sum[test_id]/desired_freq[test_id]) <= desired_freq[test_id] + max_allowed_freq_shift[test_id] && avg_freq >= desired_freq[test_id] - max_allowed_freq_shift[test_id]){//} && (avg_latency = latency_sum[test_id]/desired_freq) < max_latency_time) {

     interface_test_flag = false;

     std::ofstream log_file;
     log_file.open(test_id+".txt", std::ios::out | std::ios::app);

     if (log_file.is_open()){
     log_file << "SUCCESS at "<<ros::Time::now().toSec()<< "seconds, \nTest nr: "<<test_nr[test_id]<<"\nAverage frequency: "
                               <<avg_freq<<"\nTime Elapsed: "<<last_received[test_id] - first_received[test_id]<<" \nMax Frequency: "<< freq_max[test_id]<<"\nMin Frequency: "
                               <<freq_min[test_id] <<"\nAverage latency: "<<avg_latency<<"\nMax Latency: "<<latency_max[test_id]<<"\n"<<std::endl;

     ROS_INFO("Test %s Succeeded", test_id.c_str());
     }

     log_file.close();
     // std::cout << "Test " << test_id << " SUCCESS at " << ros::Time::now().toSec() << "seconds" << std::endl << "Test nr: " << test_nr[test_id]
     // << std::endl << "Average frequency: " << avg[test_id] << std::endl << "Time Elapsed: " << last_time[test_id] - first_received[test_id]
     // << std::endl << "Max Frequency: " << freq_max[test_id] << std::endl << "Min Frequency: " << freq_min[test_id] << std::endl;

  } else if (received - first_received[test_id]<max_allowed_time[test_id]){
     count[test_id] = 0;
     freq_sum[test_id] = 0;
     test_nr[test_id] ++;
     count[test_id]++;
     ROS_INFO("\nTest: %s, \nTest nr: %f,\nDesired frequency: %f \nAverage frequency: %f, \nTime Elapsed: %f, \nMax Frequency: %f,\nMin Frequency: %f, \nMax Latency: %f\n\n", test_id.c_str(), test_nr[test_id], desired_freq[test_id], avg_freq, received - first_received[test_id], freq_max[test_id], freq_min[test_id], latency_max[test_id]);
     freq_min[test_id] = 1000;
     freq_max[test_id] = 0;
     // first_received[test_id] = received;
    }

    else {
      ROS_INFO("Test %s failed - timeout", test_id.c_str());
      interface_test_flag = false;
    }

    last_received[test_id] = received;
}

void run_test(std::string test_id, bool &interface_test_flag)
{

  double received;
  static double avg_freq = 0;
  double freq;
  static double latency = 0;
  static double avg_latency = 0;
  received = ros::Time::now().toSec();

  if(first_received[test_id] == 0) {
   first_received[test_id] = received;
   last_received[test_id] = ros::Time::now().toSec();
   freq_min[test_id] = 1000;
   freq_max[test_id] = 0;
   latency_max[test_id]=0;
   test_nr[test_id] = 0;
   count[test_id] = 0;
 }

 if (count[test_id] > 0) {
   freq = 1/(received - last_received[test_id]);
   latency_max[test_id] = std::max(latency_max[test_id], latency);
   freq_min[test_id] = std::min(freq_min[test_id], freq);
   freq_max[test_id] = std::max(freq_max[test_id], freq);
 }

 if (count[test_id] <= desired_freq[test_id]) {
   freq_sum[test_id] += freq;
   latency_sum[test_id]+=latency;
   count[test_id]++;
  }
  else if ( (avg_freq = freq_sum[test_id]/desired_freq[test_id]) <= desired_freq[test_id] + max_allowed_freq_shift[test_id] && avg_freq >= desired_freq[test_id] - max_allowed_freq_shift[test_id]){

     interface_test_flag = false;

     std::ofstream log_file;
     log_file.open(test_id+".txt", std::ios::out | std::ios::app);

     if (log_file.is_open()){
     log_file << "SUCCESS at "<<ros::Time::now().toSec()<< "seconds, \nTest nr: "<<test_nr[test_id]<<"\nAverage frequency: "
                               <<avg_freq<<"\nTime Elapsed: "<<last_received[test_id] - first_received[test_id]<<" \nMax Frequency: "<< freq_max[test_id]<<"\nMin Frequency: "
                               <<freq_min[test_id] <<"\nAverage latency: "<<avg_latency<<"\nMax Latency: "<<latency_max[test_id]<<"\n"<<std::endl;

     ROS_INFO("Test %s Succeeded", test_id.c_str());
     }

     log_file.close();
     // std::cout << "Test " << test_id << " SUCCESS at " << ros::Time::now().toSec() << "seconds" << std::endl << "Test nr: " << test_nr[test_id]
     // << std::endl << "Average frequency: " << avg[test_id] << std::endl << "Time Elapsed: " << last_time[test_id] - first_received[test_id]
     // << std::endl << "Max Frequency: " << freq_max[test_id] << std::endl << "Min Frequency: " << freq_min[test_id] << std::endl;

  } else if (received - first_received[test_id]<max_allowed_time[test_id]){
     count[test_id] = 0;
     freq_sum[test_id] = 0;
     test_nr[test_id] ++;
     count[test_id]++;
     ROS_INFO("\nTest: %s, \nTest nr: %f,\nDesired frequency: %f \nAverage frequency: %f, \nTime Elapsed: %f, \nMax Frequency: %f,\nMin Frequency: %f, \nMax Latency: %f\n\n", test_id.c_str(), test_nr[test_id], desired_freq[test_id], avg_freq, received - first_received[test_id], freq_max[test_id], freq_min[test_id], latency_max[test_id]);
     freq_min[test_id] = 1000;
     freq_max[test_id] = 0;
     // first_received[test_id] = received;
    }

    else {
      ROS_INFO("Test %s failed - timeout", test_id.c_str());
      interface_test_flag = false;
    }

    last_received[test_id] = received;
}
