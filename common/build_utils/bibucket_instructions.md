##  AUTh bitbucket repository instructions ##
1. Create a workspace directory
    `mkdir catkin_ws`
    `cd catkin_ws`
    `catkin_init_workspace`
    `mkdir src`
    
2. Clone repository into src
    `cd src`
    `git clone http://bitbucket.org/multidrone_eu/multidrone_full`
    `cd multidrone_full`
    `git checkout auth_drone`
Note: use `git checkout auth_drone` to work on drone modules, `git checkout auth_ground` to work on ground modules

3. Change directory to catkin_ws & Build
    `cd ../..`
For auth_drone, do:
    `touch src/multidrone_full/ground/CATKIN_IGNORE`
    `touch src/multidrone_full/drone/onboard_3d_tracker/CATKIN_IGNORE`
    `touch src/multidrone_full/drone/onboard_scheduler/CATKIN_IGNORE`
    `touch src/multidrone_full/drone/action_executer/CATKIN_IGNORE`
For auth_ground:
    `touch src/multidrone_full/drone/CATKIN_IGNORE`
    `catkin build`
Note: you may need to install python-catkin-tools:
    `sudo apt-get install python-catkin-tools`
To ignore unrelated packages (e.g., multidrone_planning), add an empty CATKIN_IGNORE file to the package directory:
    `touch src/multidrone_full/ground/multidrone_planning/CATKIN_IGNORE`
The command `catkin build` MUST be entered from the catkin workspace root.
For other problems with building, see multidrone_full/common/build_utils/build_vision_instructions.md & multidrone_full/common/build_utils/README.md


## Basic git usage
 - Check status of local changes (modified, deleted, added files)
    `git status`
 - Commit changes
    `git commit`
will open a text file with modifications to review before commiting & ask you to write a message for the commit
or
    `git commit -m "Commit message"`
to commit without reviewing
 - Push changes (commited changes need to be pushed to the repository)
    `git push`
 - Pull changes from repo
    `git pull`
use git pull to make sure you have the latest versions of all files
 - pull file from repository (abort local changes)
    `git checkout -- filename`
