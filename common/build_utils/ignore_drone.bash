#!/bin/bash
multidrone_msgs_dir=$( rospack find multidrone_msgs )
msgs_subdir="/common/multidrone_msgs"
multidrone_repo=${multidrone_msgs_dir/%$msgs_subdir}
touch "${multidrone_repo}/drone/CATKIN_IGNORE"