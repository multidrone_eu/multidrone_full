#!/bin/bash

sleep 2;

echo 'Executing rosservice call /drone_1/start_shooting "data: true"'
rosservice call /drone_1/starting_trajectory "data: true"
echo 'Executing rosservice call /drone_2/start_shooting "data: true"'
rosservice call /drone_2/start_shooting "data: true"
echo 'Executing rosservice call /drone_3/start_shooting "data: true"'
rosservice call /drone_3/start_shooting "data: true"