# MULTIDRONE FULL - Common #

This folder contains all the code which is common for the whole MULTIDRONE system. It may not contain subfolders but ROS packages.

## List of packages ##

* build_utils (it is only a folder, not a ROS package)
* interface_test
* multidrone_configs
* multidrone_kml_parser
* multidrone_launchers
* multidrone_msgs
* visualanalysis_msgs

### Dependencies ###

* [geodesy (from geographic_info)](http://wiki.ros.org/geodesy): `sudo apt-get install ros-kinetic-geographic-info`
