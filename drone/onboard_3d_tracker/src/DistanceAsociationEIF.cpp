#include <DistanceAsociationEIF.h>

// Constructor
DistanceAsociationEIF::DistanceAsociationEIF(){
    initialize();
}
// Destructor
DistanceAsociationEIF::~DistanceAsociationEIF(){}

// Save the last table with the data of sunshine and EIF
// Also call push_targets3Dtable to save the corresponding part
// of the data in the target table of the time window
void DistanceAsociationEIF::push_EIFasocTable(std::vector <onboard_3d_tracker::EIFasoc_P2P> newTable){
    EIFasocTable = newTable;
    onboard_3d_tracker::Targets3DArray_P2P targetList;
    std::vector <onboard_3d_tracker::Targets3DArray_P2P> targetTable;
    for(unsigned int i = 0; i < newTable.size(); i++){
        targetList.group_id = newTable[i].group_id;
        targetList.destination_id = newTable[i].destination_id;
        targetList.source_id = newTable[i].source_id;
        targetList.type = newTable[i].type;
        targetList.nseq = newTable[i].nseq;
        targetList.targets3D = newTable[i].targets3D;
        targetList.targetsID = newTable[i].targetsID;
        targetTable.push_back(targetList);
    }
    push_targets3Dtable(targetTable);
}

void DistanceAsociationEIF::push_EIFasocTableWithGPSdata(std::vector <onboard_3d_tracker::EIFasoc_P2P> newTable){
    if(lastGPSdata.targets3D.size() > 0){
        newTable.push_back(lastGPSdata);
        //clearGPSdataVector();	// GPS measurements saved since the last iteration of the EIF are deleted, they have already been used
    }

    push_EIFasocTable(newTable);
}

// Enter a new 3D target table to be associated
void DistanceAsociationEIF::push_targets3Dtable(std::vector <onboard_3d_tracker::Targets3DArray_P2P> newTable){
// The new table is added to the beginning of the vector
    targets3Dtables.push_front(newTable);
    // If the maximum size for the time window has been exceeded, the last item is deleted
     if(targets3Dtables.size() > asociation_window){
         targets3Dtables.pop_back();
     }
}

// Update the track list pairings using the distance of each target
// of the new measurement table with the states of each track
// Call in the update phase after adding the new measurement table
// with the push_targets3Dtable function
// After the call of this function the function updateTrackListPairingsTimeWindow must be called
// to match targets that have been marked
void DistanceAsociationEIF::updateTrackListPairingsByState(std::vector <Track> &tl, std::vector <std::vector <bool>> &markedTargets){
    // Each target of each device in the last table added will be traversed. Distance from
    // this with each track (which must have been previously updated) will be calculated.
    // The number of nearby tracks (given a proximity threshold) will indicate pairing
    // of said target or if the target must be marked to be treated by pairing
    // per time window

    // With the GPS measurements you act differently
    // 1. GPS tracks carry the id of the GPS measures themselves. Therefore, if a measure has
    // the same id as a gps track is a measure of that track directly and is only
    // necessary to update the time since the last track pairing
    // 2. If there was no track with the gps measurement id, it is searched if any visual track
    // is close to the gps measurement. In case it looks like 1 track only, it matches that
    // track and the common id of the track is changed to that of the gps measurement (reorder trackList). In other words,
    // the visual track is converted to gps track. In case you are not near any visual track
    // or near several, it is sent to the association by projection.    
    
    if(tl.size() > 0){  // Si no hay pistas no tiene sentido asociar usando los estados de los pistas
        double targetdist;
        unsigned int ndist;
        std::vector <unsigned int> indexList;   // Índices de los tracks con los que el target se encuentra cerca
        target_local_id auxLocalTarget;
        double eudist;
        int track_index;
        bool gps_measure;
        int firstCompareTrackIndex;
        int firstVisualTrackIndex = findFirstVisualTrackIndex();    // Index of the first visual track
        if(firstVisualTrackIndex == -1){    // There are no visual tracks
            // GPS measurements will not be compared with the track list (because either there are no tracks or they are all from GPS)
            firstVisualTrackIndex = tl.size();
        }
        
        for(unsigned int i = 0; i < targets3Dtables[0].size(); i++){ // Loop that goes through the different devices (drones) of the last table            
            for(unsigned int j = 0; j < targets3Dtables[0][i].targets3D.size(); j++){ // Loop that goes through the targets of said device in the last table
                if(targets3Dtables[0][i].source_id == GPS_VIRTUAL_DEVICE_ID){   // Virtual device with the measurements of the sensors
                    track_index = findGPSTrack(targets3Dtables[0][i].targetsID[j]);
                    firstCompareTrackIndex = firstVisualTrackIndex; // only compares to visual tracks
                    gps_measure = true;
                }else{  // visual measurement, follow usual procedure
                    track_index = -1;
                    firstCompareTrackIndex = 0;  // Compare all tracks
                    gps_measure = false;                    
                }
                if(track_index == -1){  // There is no track with the gps id or it is a visual measurement
                    indexList.clear();
                    // If it is a gps measure it only compares with the visual tracks
                    // no point comparing with gps tracks
                    for(unsigned int k = firstCompareTrackIndex; k < tl.size(); k++){    // Loop that runs through the different tracks
                        // Calculate the distance in this last table (last measurement) between the target and the track
                        targetdist = euclideanDistance(targets3Dtables[0][i].targets3D[j],tl[k].state);
                        ndist = 1;
                        // The distances between the same target and track are added at previous moments
                        // (following tables and elements of both time windows)
                        // (as long as the target and the track have measurements and estimates at those moments of time)
                        // The number of distances added is also controlled to calculate the mean
                        for(int t = 1; t < targets3Dtables.size(); t++){
                            // Search in table t, the track and the target of the drone compared just above in table 0
                            auxLocalTarget.device_id = targets3Dtables[0][i].source_id;
                            auxLocalTarget.target_local_id = targets3Dtables[0][i].targetsID[j];
                            if(findTrackTargets3Ddistance(t, k, auxLocalTarget, eudist) != -1){
                                targetdist += eudist;
                                ndist++;                                
                            }/*else{
                            }*/
                        }

                        // The average mean distance is calculated
                        targetdist /= ndist;

                        if(targetdist <= track_asociation_th){
                            indexList.push_back(k);
                        }
                    }
                    // At this point the number of tracks that are close to the analyzed target is known
                    // and the indexes of these tracks
                    // Acts based on the number of tracks close to the target
                    auxLocalTarget.device_id = targets3Dtables[0][i].source_id;
                    auxLocalTarget.target_local_id = targets3Dtables[0][i].targetsID[j];
                    if(indexList.size() == 1){
                        if(!gps_measure){   // Visual measurement
                            // Check if the target is already paired with that track
                            // in that case nothing is done, xk the match is already correct
                            // Otherwise the target is added to the list of paired targets of the track
                            // If the track was already paired to the track, it is not necessary to search if the target
                            // is paired to other tracks to delete them because everything is supposed to work
                            // correctly the target only appears once in the entire track list, in the case
                            // mentioned, on the track where it should be.
                            if(findTargetLocalPairingList(tl[indexList[0]].pairings,auxLocalTarget) == -1){ // The target was not paired on that track
                                // First search and delete the target if it is paired with other tracks                    
                                eraseTargetFromTrackVector(tl,auxLocalTarget);
                                // Once the uniqueness of the target is assured, it is added to the track
                                tl[indexList[0]].pairings.targets.push_back(auxLocalTarget);
                            }                            
                        }else{  // GPS measurement    
                            // Measurement pairing added
                            tl[indexList[0]].pairings.targets.push_back(auxLocalTarget);
                            // The common track id is changed to convert the track to gps track type
                            //tl[indexList[0]].pairings.common_id = targets3Dtables[0][i].targetsID[j];
                            // TrackList is sorted
                            changeTrackID(indexList[0],targets3Dtables[0][i].targetsID[j]);
                        }
                        // In either case the time is refreshed without pairing the track
                        tl[indexList[0]].time_since_last_pairing = 0;
                        // The target is also unchecked so as not to be associated by projection
                        markedTargets[i][j] = false;
                    }else{  // 0 or > 1 tracks close to the target, target sent to association by projection
                    // If the target had previously been paired with any track, it must be
                    // cleared the pairing
                        if(!gps_measure){   // With total security the gps measurement is not paired
                            eraseTargetFromTrackVector(tl,auxLocalTarget);
                        }
                    }
                }else{  // The GPS track had been created previously
                    // Reset time since last pairing
                    trackList[track_index].time_since_last_pairing = 0;
                    // The target is also unchecked so as not to be associated by projection
                    markedTargets[i][j] = false;
                }
            }
        }
    }// If there are no tracks all targets are passed to be associated by projection
}

// Sort the trackList by the tracks id
void DistanceAsociationEIF::sortTrackList(){
    // There must be at least two tracks
    if(trackList.size() > 1){
        Track auxTrack;
        Eigen::MatrixXd aux_sigmat1;
        Eigen::MatrixXd aux_psigmat;
        Eigen::MatrixXd aux_sigmat;

        Eigen::MatrixXd aux_ept1;
        Eigen::MatrixXd aux_pept;
        Eigen::MatrixXd aux_ept;

        Eigen::MatrixXd aux_mut1;
        Eigen::MatrixXd aux_pmut;
    std::vector<Eigen::MatrixXd> covariance1;
        for(unsigned int i = 1; i < trackList.size(); i++){
            if(trackList[i-1].pairings.common_id > trackList[i].pairings.common_id){
                auxTrack = trackList[i];
                aux_sigmat1 = sigmat1[i];
                aux_psigmat = psigmat[i];
                aux_sigmat = sigmat[i];
                aux_ept1 = ept1[i];
                aux_pept = pept[i];
                aux_ept = ept[i];
                aux_mut1 = mut1[i];
                aux_pmut = pmut[i];

                trackList[i] = trackList[i-1];
                sigmat1[i] = sigmat1[i-1];
                psigmat[i] = psigmat[i-1];
                sigmat[i] = sigmat[i-1];
                ept1[i] = ept1[i-1];
                pept[i] = pept[i-1];
                ept[i] = ept[i-1];
                mut1[i] = mut1[i-1];
                pmut[i] = pmut[i-1];

                trackList[i-1] = auxTrack;
                sigmat1[i-1] = aux_sigmat1;
                psigmat[i-1] = aux_psigmat;
                sigmat[i-1] = aux_sigmat;
                ept1[i-1] = aux_ept1;
                pept[i-1] = aux_pept;
                ept[i-1] = aux_ept;
                mut1[i-1] = aux_mut1;
                pmut[i-1] = aux_pmut;
            }
        }
    }
}

// Change the id of a track and reorder the trackList
void DistanceAsociationEIF::changeTrackID(unsigned int track_index, unsigned int newID){
    printf("\n\nCambiando ID del track %d (index: %d), nueva ID: %d\n\n", trackList[track_index].pairings.common_id,track_index, newID);  // DEBUG

    // The id of the indicated track is changed
    trackList[track_index].pairings.common_id = newID;

    // Sort trackList
    sortTrackList();    
}

// This function will add the contributions to each track
// To do this, it will go through the pairing list of each track and will search if they have arrived
// measurements of the corresponding target of the paired drone
// Association must have been made before this step for the last measurement table
void DistanceAsociationEIF::addContributions(){
    int index_device, index_target;
    Eigen::MatrixXd sigmat_contrib(ORDER,ORDER);
    Eigen::MatrixXd ept_contrib(ORDER,1);
    Eigen::MatrixXd T(4,4);
    Eigen::MatrixXd z_visual(2,1);
    Eigen::MatrixXd z_gps(3,1);
    Eigen::MatrixXd Qgps_k(3,3);    // GPS covariance matrix

    if(EIFasocTable.size() > 0){
        for(unsigned int i = 0; i < trackList.size(); i++){
            // The track pairings are traversed to find out if this iteration has received a measurement from them
            for(unsigned int j = 0; j < trackList[i].pairings.targets.size(); j++){            
                // A target of a paired drone might not have sent measurements this iteration
                // therefore it should be checked if measures have been received from the target this iteration and look for their indexes
                // First find the index of the drone (device) in the targets3D table (the indexes will be the same as in
                // the table with the EIF data "EIFasocTable"). The EIFasocTable table is equal to the last table in the
                // time window, only containing the measurement data for the EIF
                index_device = findDeviceTableIndex(0,trackList[i].pairings.targets[j].device_id);
                if(index_device >= 0){  // Device found in the last table
                    // The target index is searched
                    index_target = findIndexUint16(trackList[i].pairings.targets[j].target_local_id, targets3Dtables[0][index_device].targetsID);
                    if(index_target >= 0){  // Target finded
                        // The contribution is calculated and added to the calculation of the EIF filter estimate
                        // Each type of measurement has its observation model
                        if(trackList[i].pairings.targets[j].device_id == GPS_VIRTUAL_DEVICE_ID){    // target GPS
                            z_gps(0) = EIFasocTable[index_device].targets3D[index_target].x;
                            z_gps(1) = EIFasocTable[index_device].targets3D[index_target].y;
                            z_gps(2) = EIFasocTable[index_device].targets3D[index_target].z;
                            
                            if(fixed_sigmaGPS){ // Covariance matrix of fixed GPS measurements
                                calculateContribution(z_gps, Hgps*pmut[i], Hgps, pmut[i], Qgps, ept_contrib, sigmat_contrib);
                            }else{  // Variable gps measure covariance matrix (comes with each measure)
                            // The GPS covariance is recovered to calculate the contribution                         
                                for(int h = 0; h < 3; h++){
                                    for(int k = 0; k < 3; k++){
                                        Qgps_k(h,k) = lastGPSdata.contrib[index_target].covariance[h*6 + k];
                                    }
                                }
                                calculateContribution(z_gps, Hgps*pmut[i], Hgps, pmut[i], Qgps_k, ept_contrib, sigmat_contrib);
                            }
                        }else{  // visual target                        
                            z_visual(0) = EIFasocTable[index_device].measurements[index_target].z.x;
                            z_visual(1) = EIFasocTable[index_device].measurements[index_target].z.y;
                            // Retrieve (by rows) the transformation matrix
                            for(int h = 0; h < 4; h++){
                                for(int k = 0; k < 4; k++){
                                    T(h,k) = EIFasocTable[index_device].measurements[index_target].T[h*4 + k];
                                }
                            }
                            calculateVisualContribution(z_visual, T, pmut[i], Qvisual, ept_contrib, sigmat_contrib);
                        }
                        // Added to the estimate of the corresponding track
                        ept[i] += ept_contrib;
                        sigmat[i] += sigmat_contrib;
                    }
                }
            }
        }

        // EIFasocTable data must be deleted at this point to prevent it from being used again
        EIFasocTable.clear();
    }
}

// Function that saves the vector and information matrix for the next iteration
// It is the last step in the iteration of the EIF filter
void DistanceAsociationEIF::saveEstimation(){
    for(unsigned int i = 0; i < trackList.size(); i++){
        sigmat1[i] = sigmat[i];
	    ept1[i] = ept[i];
    }
}

// Update the status of the tracks with the last calculation made by the EIF filter
// Also save the filter result in the canonical form of the kalman filter
// (mut1 and covariance1)
// Also add one to the lifetime of the track (this function is supposed to be called
// at the end of each iteration)
// If a target has been alive for too long without the refreshment or addition of a pairing
// the track is removed due to disuse
// Also update the last saved in the status time window
void DistanceAsociationEIF::updateTrackListStates(){
    std::vector <unsigned int> indexList;  // List with the indexes of the tracks to delete
    for(unsigned int i = 0; i < trackList.size(); i++){
        covariance1[i] = sigmat[i].inverse();
        mut1[i] = covariance1[i]*ept[i];

        trackList[i].state.x = mut1[i](0);
        trackList[i].state.y = mut1[i](1);
        trackList[i].state.z = mut1[i](2);

        trackList[i].stateTW[0].x = mut1[i](0);
        trackList[i].stateTW[0].y = mut1[i](1);
        trackList[i].stateTW[0].z = mut1[i](2);

        // Update of life times
        trackList[i].life_time++;
        trackList[i].time_since_last_pairing++;
        // The tracks are saved to be deleted due to disuse
        if(trackList[i].time_since_last_pairing >= max_time_without_pairing_refresh){
            indexList.push_back(i);
        }
    }

    // The marked tracks are erased
    if(indexList.size() > 0){
        for(unsigned int i = 0; i < indexList.size(); i++){
            eraseTrack(indexList[i] - i);
        }
    }
}

// Add the prediction to the final EIF estimate for each track
void DistanceAsociationEIF::addPrediction(){
    for(unsigned int i = 0; i < trackList.size(); i++){
        sigmat[i] += psigmat[i];	
		ept[i] += pept[i];
    }
}

// Update the track list with a prediction model
// Remember to call setPredictionModel at least once before calling this function
// without the model the prediction is meaningless
// It is also necessary to have indicated the noise of the model and the measurements
// calling setPredictionModelTrust and setMeasureTrust
void DistanceAsociationEIF::updateTrackListStatePredictionModel(std::vector <Track> &tl){
    // The prediction model is executed and the status of each track is updated
    // (the track status is part of the state vector of the EIF filter)
    geometry_msgs::Point trackPos;
    for(unsigned int i = 0; i < trackList.size(); i++){
        // Prediction (calculations are performed)
        mut1[i] = (sigmat1[i].inverse())*ept1[i];
        psigmat[i] = (G*sigmat1[i].inverse()*G.transpose() + R).inverse();
        pept[i] = psigmat[i]*G*mut1[i];

        // The estimated average is prepared to be sent to the other nodes
        pmut[i] = G*mut1[i];	// g = G

        // The status of the tracks is updated
        trackList[i].state.x = pmut[i](0);
        trackList[i].state.y = pmut[i](1);
        trackList[i].state.z = pmut[i](2);

        // This new state is added to the state time window
        trackPos.x = pmut[i](0);
        trackPos.y = pmut[i](1);
        trackPos.z = pmut[i](2);
        trackList[i].pushStateTimeWindow(trackPos,asociation_window);
    }    
}

// Update the track list with a prediction model
// Call in the prediction phase of the Bayesian filter
void DistanceAsociationEIF::updateTrackListStatePredictionModel(){
    updateTrackListStatePredictionModel(trackList);
}

// Initialize the variables for the new iteration of the EIF
void DistanceAsociationEIF::initilizeEIFiteration(){
    for(unsigned int i = 0; i < trackList.size(); i++){
        sigmat[i] = Eigen::MatrixXd::Zero(ORDER,ORDER);
	    ept[i] = Eigen::MatrixXd::Zero(ORDER,1);
    }    
}

// Update the pairings vector with the targets3D table
// All targets in the last table are added to the process
void DistanceAsociationEIF::updateTrackListPairingsTimeWindow(){
    // Delete the list of targets marked to be
    // associated using the time window
    // Vector to vector is deleted
    for(int i = 0; i < markedTargets.size(); i++){
        markedTargets[i].clear();
    }
    // The vector of vectors is deleted
    markedTargets.clear();

    // The new list of marked targets is prepared for the new table received
    // In this case all targets will be added to the time window process
    if(targets3Dtables.size() > 0){
        for(int i = 0; i < targets3Dtables[0].size(); i++){
            if(targets3Dtables[0][i].targets3D.size() > 0){
                std::vector <bool> auxVector;
                auxVector.assign(targets3Dtables[0][i].targets3D.size(),true);
                markedTargets.push_back(auxVector);
            }
        }
    }

    // Association is called by time window
    updateTrackListPairingsTimeWindow(markedTargets);
}

// Update the pairings vector with the targets3D table
// A track should be created just for them. This is so because it is possible that a target is only
// seen by a single drone. This target is not paired but must have its own track
// The gps targets are paired in the same way as normal projection targets
// However, pairings with gps targets interact with tracks differently
// 1. The target partner of the gps is not associated: A gps track is created with the pair (reorder trackList)
// 2. The even target of the gps is associated to a track: The target gps is associated to the track
// and the target gps id is given to the track, thus converting it to gps track (reorder trackList)
// 3. The target gps has been left alone. It is created with a gps track (reorder tracklist)
void DistanceAsociationEIF::updateTrackListPairingsTimeWindow(std::vector <std::vector <bool>> markedTargets){
    std::vector <std::vector <bool>> aloneTargets; // List of unpaired targets at the end of the time window (or previously)
    aloneTargets = markedTargets;    
    // The distances will be calculated (in the time window) comparing two by two among the given 3D targets
    // for each drone (at the current moment)
    if(targets3Dtables.size() > 0){
        if(targets3Dtables[0].size() > 1){ // There must be at least 2 drones in the first table to pair
            target_local_id auxLocalTarget1;
            target_local_id auxLocalTarget2;
            double pairdist;
            unsigned int ndist;
            double eudist;
            std::vector <pairingCouple> cPairs; // Auxiliary vector used to store the distance of the pairings from the last comparison between the target list of two drones
            std::vector <pairingCoupleIndex> cPairsIndex; // Save the target indices of the proposed pairings
            for(unsigned int h = 0; h < (targets3Dtables[0].size() - 1); h++){
                // Each table is compared to all of the following
                // In this way all tables are compared with all
                for(unsigned int k = h+1; k < targets3Dtables[0].size(); k++){
                    cPairs.clear();
                    cPairsIndex.clear();
                    for(unsigned int i = 0; i < targets3Dtables[0][h].targets3D.size(); i++){
                        // Check that the target has been marked to be associated by the time window
                        if(markedTargets[h][i]){
                            for(unsigned int j = 0; j < targets3Dtables[0][k].targets3D.size(); j++){
                                // Check that the target has been marked to be associated by the time window
                                if(markedTargets[k][j]){
                                    // Calculate the distance in this last table (last measurement) between both targets
                                    pairdist = euclideanDistance(targets3Dtables[0][h].targets3D[i], targets3Dtables[0][k].targets3D[j]);
                                    ndist = 1;

                                    // Add the distances of the same targets at the previous moment (following tables)
                                    // (whenever said targets appeared in the target list of said drones)
                                    // The number of distances added is also controlled to calculate the mean
                                    for(int t = 1; t < targets3Dtables.size(); t++){
                                        // Looking in table t, the targets of the drones compared just above in table 0
                                        auxLocalTarget1.device_id = targets3Dtables[0][h].source_id;
                                        auxLocalTarget1.target_local_id = targets3Dtables[0][h].targetsID[i];
                                        auxLocalTarget2.device_id = targets3Dtables[0][k].source_id;
                                        auxLocalTarget2.target_local_id = targets3Dtables[0][k].targetsID[j];
                                        if(findTargets3Ddistance(t, auxLocalTarget1, auxLocalTarget2, eudist) != -1){
                                            pairdist += eudist;
                                            ndist++;                                
                                        }
                                    }

                                    // The average mean distance is calculated
                                    pairdist /= ndist;

                                    // Now we are going to check if the pairing is accepted
                                    // For this the mean must be less than a threshold
                                    if(pairdist <= asociation_th){
                                        // Added to list of proposed pairings
                                        // only if it is the best match proposed for the target of drone 2
                                        // Explanation: it can happen that several pairings are proposed for a
                                        // same target of drone two that have passed the threshold. So if you already
                                        // there was a proposed pairing for the target of drone 2 (drone k), it will be used
                                        // the one with the shortest distance.
                                        pairingCouple auxPair;
                                        auxPair.target1.id.device_id = targets3Dtables[0][h].source_id;
                                        auxPair.target1.id.target_local_id = targets3Dtables[0][h].targetsID[i];
                                        auxPair.target1.state = targets3Dtables[0][h].targets3D[i];
                                        auxPair.target2.id.device_id = targets3Dtables[0][k].source_id;
                                        auxPair.target2.id.target_local_id = targets3Dtables[0][k].targetsID[j];
                                        auxPair.target2.state = targets3Dtables[0][k].targets3D[j];
                                        auxPair.d = pairdist;
                                        pairingCoupleIndex auxPairIndex;                                        
                                        auxPairIndex.target1.device_index = h;
                                        auxPairIndex.target1.target_local_index = i;
                                        auxPairIndex.target2.device_index = k;
                                        auxPairIndex.target2.target_local_index = j;
                                        if(cPairs.empty()){ // No pairing, pairing is added directly
                                            cPairs.push_back(auxPair);
                                            cPairsIndex.push_back(auxPairIndex);
                                        }else{
                                            // Search for pairing with the same drone k id
                                            // (only the drone targets covered by the internal loop can be repeated)
                                            unsigned int ii = 0;
                                            int index = -1;
                                            while(ii < cPairs.size() && index == -1){
                                                if(cPairs[ii].target2.id.target_local_id == targets3Dtables[0][k].targetsID[j]){
                                                    index = ii;
                                                }
                                                ii++;
                                            }
                                            
                                            // Act based on whether or not another similar match existed
                                            // and if this is better or worse
                                            if(index == -1){    // There is no drone 2 id already paired, the new pairing is added to the list
                                                cPairs.push_back(auxPair);
                                                cPairsIndex.push_back(auxPairIndex);
                                            }else{  // It exists
                                                if(pairdist < cPairs[index].d){ // New pairing with shorter distance
                                                    // The old one is deleted and the new one is added
                                                    cPairs.erase(cPairs.begin() + index);
                                                    cPairs.push_back(auxPair);
                                                    cPairsIndex.push_back(auxPairIndex);
                                                }   // If not, the previously calculated pairing is left
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // At this point a pairing list has been built for two drones
                    // The track list must be updated with this list
                    // Wait until this point to update the track list and it is not done
                    // individually with each matching proposal because you need to find
                    // a single pairing for a target, the one with the shortest distance, therefore it is necessary
                    // go through all the possibilities and keep the best one before updating the list
                    // of tracks

                    // Initially it is the track list itself (which can be updated with a Bayesian filter)
                    // the one used to continue with the target pairing or to delete tracks
                    // However, only the processing of the time window implemented by this function may
                    // create new tracks. On the other hand, if a target has been marked to be paired by
                    // the time window, is because its pairing through the tarck list is not
                    // trust. Therefore, it is assumed that when a target is added to the window process
                    // time, their appearance in the track list has been deleted at that time.

                    // The list of proposed pairings for the pair h, k is traversed
                    for(unsigned int i = 0; i < cPairs.size(); i++){
                        addPairingCouple2TrackList(cPairs[i]);

                        // All targets in the list will be paired in some way
                        // Therefore they are unmarked in the auxiliary list
                        aloneTargets[cPairsIndex[i].target1.device_index][cPairsIndex[i].target1.target_local_index] = false;
                        aloneTargets[cPairsIndex[i].target2.device_index][cPairsIndex[i].target2.target_local_index] = false;                        
                    }
                }
            }
        }

        // Regardless of the number of devices (as long as there is at least one)
        // tracks must be created for targets that have not been matched
        if(targets3Dtables[0].size() > 0){
            target_local targ;
            target_local_id targ_id;
            int aux_index;
            for(unsigned int i = 0; i < aloneTargets.size(); i++){
                // Depending on the type of target, a type of track will be created
                if(targets3Dtables[0][i].source_id == GPS_VIRTUAL_DEVICE_ID){   // GPS type targets
                    for(unsigned int j = 0; j < aloneTargets[i].size(); j++){   // each target of each device
                        // If the target is still marked, it has not been paired in any way
                        if(aloneTargets[i][j]){
                            addGPSTrack(targets3Dtables[0][i].targetsID[j],targets3Dtables[0][i].targets3D[j]);
                        }
                    }
                }else{  // visual targets
                    for(unsigned int j = 0; j < aloneTargets[i].size(); j++){   // each target of each device
                        // If the target is still marked, it has not been paired in any way
                        if(aloneTargets[i][j]){
                            targ_id.device_id = targets3Dtables[0][i].source_id;                        
                            targ_id.target_local_id = targets3Dtables[0][i].targetsID[j];
                            // A new track is added with said target
                            targ.id = targ_id;
                            targ.state = targets3Dtables[0][i].targets3D[j];
                            addVisualTrack(targ);                       
                        }
                    }
                }
            }
        }
    }
}

// Add a proposed pairing to the track list
// It is assumed that the target has not been reliably associated
// by the tracks predicted by the EIF and therefore the matching targets
// do not appear in the Tracks list, and therefore a track must be created. The only one
// exception is that in this same iteration the target has been paired in another
// comparison of drone targets and then if there is a track with any of the
// targets to add from the match. In this case the missing target is added to the
// track, no new one is created
int DistanceAsociationEIF::addPairingCouple2TrackList(pairingCouple pair){
    // All targets of all tracks in the track list are traversed for both local targets
    // 1) If no track is found (the indices remain at -1) a new track is created
    // 2) If one of the targets is already on a track, the other target is added to that track
    // 3) If both targets are already assigned to a track, this pairing is discarded

    // The operation is different if one of the measurements comes from the virtual device of GPS measurements
    if(pair.target1.id.device_id == GPS_VIRTUAL_DEVICE_ID){  // target 1 GPS type
        addGPStargetPair2TrackList(pair.target1, pair.target2);
    }else if(pair.target2.id.device_id == GPS_VIRTUAL_DEVICE_ID){   // target 2 GPS type
        addGPStargetPair2TrackList(pair.target2, pair.target1);
    }else{  // both targets visual type
        int target1_track_index = findTargetLocalTrackList(trackList, pair.target1.id);
        int target2_track_index = findTargetLocalTrackList(trackList, pair.target2.id);

        if(target1_track_index == -1 && target2_track_index == -1){ // Unassigned Targets, create new track
            addVisualTrack(pair);
        }else if(target1_track_index == -1 && target2_track_index > -1){    // Only assigned target 2, target 1 is added to the track where target 2 is
            trackList[target2_track_index].pairings.targets.push_back(pair.target1.id);
            // Refresh time without track pairing
            trackList[target2_track_index].time_since_last_pairing = 0;
        }else if(target1_track_index > -1 && target2_track_index == -1){    // Only target 1 assigned, target 2 is added to the track where target 1 is
            trackList[target1_track_index].pairings.targets.push_back(pair.target2.id);
            // Refresh time without track pairing
            trackList[target2_track_index].time_since_last_pairing = 0;
        }
    }
}

// Add two targets, the first of them type track gps, to the trackList
// 0. During the association process itself, a track has already been created with the past gps as argument.
// This should be checked. If yes, the visual target is added to the gps track
// 1. The even target of the gps and the target gps are not associated: A gps track is created with the pair (reorder trackList)
// 2. The even target of the gps is associated to a track: The target gps is associated to the track
// and the target gps id is given to the track, thus converting it to gps track (reorder trackList)
void DistanceAsociationEIF::addGPStargetPair2TrackList(target_local GPStarget, target_local visualTarget){
    // Check if the visual target was already associated
    int visualTarget_track_index = findTargetLocalTrackList(trackList, visualTarget.id);
    int GPStarget_track_index = findGPSTrack(GPStarget.id.target_local_id);

    if(GPStarget_track_index == -1 && visualTarget_track_index == -1){  // Visual target or unassigned GPS, create new track
        // The local id of the target gps is used for the common id of the new track
        // Use the position of the target gps to initialize the new GPS track
        addGPSTrack(GPStarget.id.device_id,GPStarget.state,visualTarget.id);
    }else if(GPStarget_track_index > -1 && visualTarget_track_index == -1){ // assigned gps target, the visual target is added to the gps track
        trackList[GPStarget_track_index].pairings.targets.push_back(visualTarget.id);
        // Refresh time without track pairing
        trackList[GPStarget_track_index].time_since_last_pairing = 0;
    }else if(GPStarget_track_index == -1 && visualTarget_track_index > -1){    // assigned visual target, the gps target is added to the track where the visual target is
        // And the track id is changed to the target gps, turning it into gps track
        trackList[visualTarget_track_index].pairings.targets.push_back(GPStarget.id);
        // Refresh time without track pairing
        trackList[visualTarget_track_index].time_since_last_pairing = 0;
        // Change the track id and reorder the trackList
        changeTrackID(visualTarget_track_index, GPStarget.id.target_local_id);
    }
}

// Look for a target in the trackList, in case of finding it it returns the index
// of the track where it is. In case of not finding it returns -1
int DistanceAsociationEIF::findTargetLocalTrackList(std::vector <Track> &tl, target_local_id target){
    int index = -1;
    int i = 0;
    while(i < tl.size() && index == -1){
        if(findTargetLocalPairingList(tl[i].pairings, target) != -1){
            index = i;
        }
        i++;
    }

    return index;
}

// Search for a target in a list of pairings, if found it returns the index
// of the target in the list. In case of not finding it returns -1
int DistanceAsociationEIF::findTargetLocalPairingList(pairing p, target_local_id target){
    int index = -1;
    int j = 0;
    while(j < p.targets.size() && index == -1){
        if(p.targets[j].device_id == target.device_id && p.targets[j].target_local_id == target.target_local_id){
            index = j;                
        }
        j++;
    }

    return index;
}

void DistanceAsociationEIF::manageGPSdata(multidrone_msgs::TargetStateArray gpsdata){
    // Save the gps info into a EIFasoc_P2P msg
    // All the sent targets will be traversed (GPS measurements)
    // and this information will be saved in a message type EIFasoc_P2P
    // if there was already a measurement from a GPS, it is updated with the new measurement
    // This ensures that when the EIF algorithm executes its next iteration
    // this uses the most recent received measurement from each GPS
    // So the EIF algorithm during its iteration will have to push this
    // device "virtual" to your most recent table, as if it were visual measurements of a drone
    // and after finishing its use, it must be deleted, to make way for the new GPS measurements
    // The push to the target table aims to associate visual target measurements
    // to GPS tracks.
    onboard_3d_tracker::Contribution contri;
    int target_index;
    for(unsigned int i = 0; i < gpsdata.targets.size(); i++){
        // Check if said gps has already been added      
        target_index = findIndexUint16(gpsdata.targets[i].target_id, lastGPSdata.targetsID);
        if(target_index == -1){ // There is no gps data with that id
            // A new target is added with the gps id
            lastGPSdata.targetsID.push_back(gpsdata.targets[i].target_id);  // id is added
            lastGPSdata.targets3D.push_back(gpsdata.targets[i].pose.pose.position); // target(gps) is added

            // A new target is added with the gps id
            for(unsigned int j = 0; j < ORDER*ORDER; j++){
                contri.covariance[j] = gpsdata.targets[i].pose.covariance[j];
            }
            lastGPSdata.contrib.push_back(contri);
        }else{  // The gps was previously added, the measurement is overwritten with the new one received
            lastGPSdata.targets3D[target_index] = gpsdata.targets[i].pose.pose.position;

            // the covariance is also overwritten
            for(unsigned int j = 0; j < ORDER*ORDER; j++){
                lastGPSdata.contrib[target_index].covariance[j] = gpsdata.targets[i].pose.covariance[j];
            }
        }
    }
}

// Delete the saved GPS measurements so that they are not added and integrated in the filter
// in the next iteration
void DistanceAsociationEIF::clearGPSdataVector(){
    lastGPSdata.targets3D.clear();
    lastGPSdata.targetsID.clear();
    lastGPSdata.contrib.clear();    
}

// Add a track to the track list with the past pairings as argument with the past state as argument
void DistanceAsociationEIF::addTrack(pairing pairings, geometry_msgs::Point trackPos){
    trackList.push_back({pairings,trackPos,0,0});

    // Initialization of the EIF information of the track
    Eigen::VectorXd initialState(ORDER,1);	// Initial state
    initialState << trackPos.x, trackPos.y, trackPos.z, 0, 0, 0;
    Eigen::MatrixXd ept0(ORDER,1);
    ept0 = sigmat1_initial*initialState;

    sigmat1.push_back(sigmat1_initial);
    psigmat.push_back(Eigen::MatrixXd::Zero(ORDER,ORDER));
    sigmat.push_back(sigmat1_initial);

    ept1.push_back(ept0);
    pept.push_back(Eigen::MatrixXd::Zero(ORDER,1));
    ept.push_back(ept0);

    mut1.push_back(Eigen::MatrixXd::Zero(ORDER,1));
    pmut.push_back(initialState); // Very important to initialize the predicted mean for calculating contributions
    covariance1.push_back(Eigen::MatrixXd::Zero(ORDER,ORDER));
}

// Add a track to the track list with a paired pair
// The common id assigned to the track pairings is returned
// The first free id is chosen in the list
// The track is created with a first pairingCouple
// It is assumed that there is no other track with the pair of targets added
int DistanceAsociationEIF::addVisualTrack(pairingCouple pair){
    // Prepare the state that will be saved in the track as initialization, this will be the average position
    // of both local targets (in theory they will be very similar positions because they are the same track)
    geometry_msgs::Point trackPos;
    trackPos.x = (pair.target1.state.x + pair.target2.state.x)/2;
    trackPos.y = (pair.target1.state.y + pair.target2.state.y)/2;
    trackPos.z = (pair.target1.state.z + pair.target2.state.z)/2;
    //printf("\nState: %f, %f, %f\n", trackPos.x, trackPos.y, trackPos.z);  // DEBUG

    // Track initialization
    pairing pairings;
    pairings.common_id = nextFreeIDVisualTrackList();
    pairings.targets.push_back(pair.target1.id);
    pairings.targets.push_back(pair.target2.id);

    // The track is added
    addTrack(pairings, trackPos);

    // The trackList is reordered by ids (it has probably been unordered when adding the last track)
    sortTrackList();

    return pairings.common_id;
}


// Add a track to the track list with a single target
// The common id assigned to the track is returned
// The first free id is chosen in the list
// The track is created with a first target_local
// It is assumed that there is no other track with the added target
int DistanceAsociationEIF::addVisualTrack(target_local t){
    // Prepare the state that will be saved in the track as initialization
    //printf("\nState: %f, %f, %f\n", t.state.x, t.state.y, t.state.z);   // DEBUG

    // Track initialization
    pairing pairings;
    pairings.common_id = nextFreeIDVisualTrackList();
    pairings.targets.push_back(t.id);

    // The track is added
    addTrack(pairings, t.state);

    // The trackList is reordered by ids (it has probably been unordered when adding the last track)
    sortTrackList();

    return pairings.common_id;
}

// Add a track using the GPS itself as the common id
// It is assumed that each GPS has a different id and therefore there will be no two tracks
// with the same id
// Will reorder the list when adding the new track
void DistanceAsociationEIF::addGPSTrack(int common_id, geometry_msgs::Point pos){
    if(common_id >= visual_tracks_common_id_offset){
        printf("\n\nCARE, new track ID exceeds the maximum number of gps allowed");
    }

    // Prepare the state that will be saved in the track as initialization
    //printf("\nState: %f, %f, %f\n", pos.x, pos.y, pos.z);

    // track initilization
    target_local_id t;
    t.target_local_id = common_id;
    t.device_id = GPS_VIRTUAL_DEVICE_ID;

    pairing pairings;
    pairings.common_id = common_id;
    pairings.targets.push_back(t);        

    // The track is added
    addTrack(pairings, pos);

    // The trackList is reordered by ids (it has probably been unordered when adding the last track)
    sortTrackList();
}

// This version of the function also allows adding a visual target in the pairings of the new track
void DistanceAsociationEIF::addGPSTrack(int common_id, geometry_msgs::Point pos, target_local_id visual_target_id){
    if(common_id >= visual_tracks_common_id_offset){
        printf("\n\nCARE, new track ID exceeds the maximum number of gps allowed");
    }

    // Prepare the state that will be saved in the track as initialization
    //printf("\nState: %f, %f, %f\n", pos.x, pos.y, pos.z);   // DEBUG

    // Inicialización del track
    target_local_id t;
    t.target_local_id = common_id;
    t.device_id = GPS_VIRTUAL_DEVICE_ID;

    pairing pairings;
    pairings.common_id = common_id;
    pairings.targets.push_back(t);
    pairings.targets.push_back(visual_target_id);        

    // The track is added
    addTrack(pairings, pos);

    // The trackList is reordered by ids (it has probably been unordered when adding the last track)
    sortTrackList();
}

// Delete a track from the track list given its index
// Also delete the corresponding elements in the EIF data lists
// Returns -1 if the track could not be deleted or 0 on success
int DistanceAsociationEIF::eraseTrack(unsigned int index){
    if(index < trackList.size()){
        trackList.erase(trackList.begin() + index);

        // Erase also EIF data
        sigmat1.erase(sigmat1.begin() + index);
        psigmat.erase(psigmat.begin() + index);
        sigmat.erase(sigmat.begin() + index);

        ept1.erase(ept1.begin() + index);
        pept.erase(pept.begin() + index);
        ept.erase(ept.begin() + index);

        mut1.erase(mut1.begin() + index);
        pmut.erase(pmut.begin() + index);
        covariance1.erase(covariance1.begin() + index);
    }
}

// Search the track list if there is a target for a drone in a pairing
// if yes it deletes the pairing, otherwise it does nothing.
// Returns the number of deleted targets
int DistanceAsociationEIF::eraseTargetFromTrackVector(std::vector <Track> &tl, target_local_id target){
    std::vector <unsigned int> indexList;
    unsigned int erasedTargets = 0;
    // The list of targets is scrolled looking for the target to delete
    // If a track has run out of fixtures it is added to the list of tracks to delete
    for(int i = 0; i < tl.size(); i++){
        erasedTargets += eraseTargetFromPairing(tl[i].pairings, target);
        if(tl[i].pairings.targets.size() == 0){
            indexList.push_back(i);
        }
    }

    // The tracks that are left empty are erased
    for(int i = 0; i < indexList.size(); i++){
        eraseTrack(indexList[i] - i);
    }

    return erasedTargets;
}

// Search in a pairing list if there is a target of a drone in a pairing
// if so it returns the number of deleted targets and clears the pairing,
// otherwise it does nothing and returns -1
int DistanceAsociationEIF::eraseTargetFromPairing(pairing &p, target_local_id target){
    // Se va a recorrer la lista de emparejamientos buscando los índices de 
    // los emparejamientos que contengan al target pasado como argumento
    std::vector <unsigned int> indexList;
    for(int i = 0; i < p.targets.size(); i++){
        if(p.targets[i].device_id == target.device_id && p.targets[i].target_local_id == target.target_local_id){
            indexList.push_back(i);
        }
    }

    // You will go through the list of pairings looking for the indexes of
    // pairings containing the passed target as argument
    if(indexList.size() > 0){
        for(int i = 0; i < indexList.size(); i++){
            p.targets.erase(p.targets.begin() + indexList[i] - i);  // Cada vez que se borra un elemento los indices bajan en una unidas
        }
    }

    return indexList.size();
}

// Search a track list for the next free ID
int DistanceAsociationEIF::nextFreeIDTrackList(){
    unsigned int i = 0; // First valid ID of purely visual tracks = visual_tracks_common_id_offset
    int nextFreeID = -1;
    bool freeIDfinded;
    while(nextFreeID == -1){
        freeIDfinded = true;
        for(int j = 0; j < trackList.size(); j++){
            if(trackList[j].pairings.common_id == i){
                freeIDfinded = false;   // id in use
            }
        }
        if(freeIDfinded == true){
            nextFreeID = i;
        }
        i++;
    }
    
    return nextFreeID;
}

// Search in a track list for the next free ID of the visual tracks
int DistanceAsociationEIF::nextFreeIDVisualTrackList(){
    unsigned int i = visual_tracks_common_id_offset; // First valid ID of purely visual tracks = visual_tracks_common_id_offset
    int nextFreeID = -1;
    bool freeIDfinded;
    while(nextFreeID == -1){
        freeIDfinded = true;
        for(int j = 0; j < trackList.size(); j++){
            if(trackList[j].pairings.common_id == i){
                freeIDfinded = false;   // id in use
            }
        }
        if(freeIDfinded == true){
            nextFreeID = i;
        }
        i++;
    }
    
    return nextFreeID;
}

// Calculate the distance between two targets3D of two different drones in one of the saved tables
// from the list of targets3D tables
// There must be at least two drones in the table
// Returns 0 if the indicated targtes3D have been found, otherwise returns -1
int DistanceAsociationEIF::findTargets3Ddistance(unsigned int table_index, target_local_id target1, target_local_id target2, double &d){
    // There must be at least two drones in the table (and the table must exist)
    if(table_index >= targets3Dtables.size() || table_index < 0 || targets3Dtables[table_index].size() <= 1){   // The indicated table must exist
        return -1;  // The indicated table does not exist      
    }else{  // The table exits
        // The indexes must be found (if they exist) in the table of the drones with drone1_id and drone2_id     
        int index_drone1 = findDeviceTableIndex(table_index,target1.device_id);
        int index_drone2 = findDeviceTableIndex(table_index,target2.device_id);

        // If it does not exist, -1 is returned
        // if the indexes have been found, the search continues
        if(index_drone1 == -1 || index_drone2 == -1){
            return -1;            
        }else{
            // The indices of both targets are searched in the list of drones
            // In case it does not exist, minus 1 is returned
            // If both values are found, the distance between said targets is calculated and 0 is returned
            int index_target1 = findIndexUint16(target1.target_local_id, targets3Dtables[table_index][index_drone1].targetsID);
            int index_target2 = findIndexUint16(target2.target_local_id, targets3Dtables[table_index][index_drone2].targetsID);
            if(index_target1 == -1 || index_target2 == -1){
                return -1;
            }else{
                d = euclideanDistance(targets3Dtables[table_index][index_drone1].targets3D[index_target1], targets3Dtables[table_index][index_drone2].targets3D[index_target2]);
                return 0;
            }
        }        
    }
}

// Calculate the distance between a target and a track in an instant time t
// If there is no estimate of that track at that instant or measure of said target at that instant -1 is returned
// Returns 0 if the track and arget have been found, and therefore the calculated distance
int DistanceAsociationEIF::findTrackTargets3Ddistance(unsigned int table_index, unsigned int track_index, target_local_id target, double &d){
    // First check if the table exists exists
    if(table_index >= targets3Dtables.size() || table_index < 0){        
        return -1;  // Table does not exist exits and returns -1
    }else{  // The table exits
        // Check if the indicated track exists
        if(track_index >= trackList.size() || track_index < 0){ // El track no existe
            return -1;
        }else{  // EL track existe
            // It is verified that the track has an estimate for the instant of time back track_index
            // in your states time window
            // (it is not checked if it is greater than 0 because it was already checked above in the first if)
            if(table_index >= trackList[track_index].stateTW.size()){    // There is no estimate
                return -1;
            }else{  // The estimate exists
                // Search for the target of the drone indicated in the table of the previous instant table_index
                int index_drone = findDeviceTableIndex(table_index,target.device_id);
                // If it does not exist, -1 is returned
                // if the index has been found, the search continues
                if(index_drone == -1){
                    return -1;
                }else{
                    // Search the target index in the drone list
                    // In case it does not exist, minus 1 is returned
                    // If the value is found, the distance between said targets and the corresponding estimate of the track is calculated
                    int index_target = findIndexUint16(target.target_local_id, targets3Dtables[table_index][index_drone].targetsID);
                    if(index_target == -1){ // target not found
                        return -1;
                    }else{  // target found
                        d = euclideanDistance(targets3Dtables[table_index][index_drone].targets3D[index_target], trackList[track_index].stateTW[table_index]);
                        return 0;
                    }
                }
            }
        }
        
    }
}

// Update the pairings by calling the two methods
// association in the correct order
// REMEMBER: call updateTrackListStatePredictionModel before calling this function
void DistanceAsociationEIF::updateTrackListPairings(){
    // Delete the list of targets marked to be
    // associated using the time window
    // Vector to vector is deleted
    if(markedTargets.size() > 0){
        for(int i = 0; i < markedTargets.size(); i++){
            markedTargets[i].clear();
        }
        // The vector of vectors is deleted
        markedTargets.clear();
    }
    // The new list of marked targets is prepared for the new table received
    // By default the targets are sent to the association by time window
    for(int i = 0; i < targets3Dtables[0].size(); i++){
        std::vector <bool> auxVector;
        if(targets3Dtables[0][i].targets3D.size() > 0){   // A marker vector is only created if there are targets in said device           
            auxVector.assign(targets3Dtables[0][i].targets3D.size(),true);
            
        }// If there are no targets, an empty vector is added
        markedTargets.push_back(auxVector);
    }

    // It is associated by the proximity of the state of the tracks
    updateTrackListPairingsByState(trackList,markedTargets);

    // The marked targets are associated through the time window
    updateTrackListPairingsTimeWindow(markedTargets);
}

void DistanceAsociationEIF::initialize(){
    asociation_window = TIME_WINDOW_LENGHT_DEFAULT;
    asociation_th = ASOCIATION_TH_DEFAULT;
    track_asociation_th = TRACK_ASOCIATION_TH_DEFAULT;
    max_time_without_pairing_refresh = MAX_TIME_WITHOUT_PAIRING_REFRESH;
    visual_tracks_common_id_offset = VISUAL_TRACKS_COMMON_ID_OFFSET_DEFAULT;
    lastGPSdata.source_id = GPS_VIRTUAL_DEVICE_ID;
    G = Eigen::MatrixXd::Identity(ORDER,ORDER);
    R = Eigen::MatrixXd::Identity(ORDER,ORDER);
    Qvisual = Eigen::MatrixXd::Identity(2,2);
    Qgps = Eigen::MatrixXd::Identity(3,3);
    Hgps = Eigen::MatrixXd::Zero(3,6);
    // GPS observation model (and Jacobian)
    Hgps << 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0, 0.0, 0.0;
}

// Set the initial values of the EIF filter that will be given when creating the track
void DistanceAsociationEIF::setInitialState(Eigen::MatrixXd sigmat1_m, Eigen::MatrixXd ept1_m){
    sigmat1_initial = sigmat1_m;
}

// Search (if it exists) a target with the id passed as argument in the track list
// In the formative case it returns the index of the track in the list
int DistanceAsociationEIF::findTrack(unsigned int track_id){
    unsigned int i = 0;
    int index = -1;
    while(i < trackList.size() && index == -1){
        if(trackList[i].pairings.common_id == track_id){
            index = i;
        }
        i++;
    }
    return index;
}

// Search (if it exists) a GPS track with the id passed as argument in the track list
// If so, returns the index of the track in the list
int DistanceAsociationEIF::findGPSTrack(unsigned int track_id){
    unsigned int i = 0;
    int index = -1;
    while(i < trackList.size() && trackList[i].pairings.common_id < visual_tracks_common_id_offset && index == -1){
        if(trackList[i].pairings.common_id == track_id){
            index = i;
        }
        i++;
    }
    return index;
}

// Search (if it exists) the index of the vector of a drone in the target3D table given its id (the id of the drone)
// Return -1 if you don't find the drone, or the index if you find it
int DistanceAsociationEIF::findDeviceTableIndex(unsigned int table_index, unsigned int drone_id){
    if(table_index >= targets3Dtables.size() || table_index < 0){   // The indicated table must exist
        return -1;  // The table exists
    }else{  // La tabla existe
        unsigned int i = 0;
        int index = -1;
        while(i < targets3Dtables[table_index].size() && index == -1){
            if(targets3Dtables[table_index][i].source_id == drone_id){
                index = i;
            }
            i++;
        }
        return index;
    }
}

// Search for a value in an unsigned 16-bit integer vector (ID vectors)
// Returns -1 if it doesn't find the value
int DistanceAsociationEIF::findIndexUint16(unsigned short int d, std::vector<unsigned short int> v){
    unsigned int i = 0;
    int index = -1;
    while(i < v.size() && index == -1){
        if(v[i] == d){
            index = i;
        }
        i++;
    }
    return index;
}

// Search the index of the first visual track in the track list
// If there are no visual tracks, -1 is returned
int DistanceAsociationEIF::findFirstVisualTrackIndex(){
    unsigned int i = 0;
    int index = -1;
    while(i < trackList.size() && index == -1){
        if(trackList[i].pairings.common_id >= visual_tracks_common_id_offset){
            index = i;
        }
        i++;
    }
    return index;
}

int DistanceAsociationEIF::GPSTracksCount(){
    int index = findFirstVisualTrackIndex();
    if(index == -1){
        return trackList.size();
    }else{
        return index;
    }
}

// Calculate the Euclidean distance between two points
double DistanceAsociationEIF::euclideanDistance(geometry_msgs::Point p1, geometry_msgs::Point p2){
    return sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z));
}

// Calculate the contribution given the visual measurements of a target
void DistanceAsociationEIF::calculateVisualContribution(const Eigen::Ref<const Eigen::MatrixXd>& z,
const Eigen::Ref<const Eigen::Matrix4d>& T, const Eigen::Ref<const Eigen::MatrixXd>& pmut,
const Eigen::Ref<const Eigen::MatrixXd>& Q, Eigen::Ref<Eigen::MatrixXd> ept_contrib,
Eigen::Ref<Eigen::MatrixXd> omegat_contrib){
	// H and h are updated with the new estimated average (The last position read from the drone is used)
	double Hden, Hnum1, Hnum2;
	Hden = T(2,0)*pmut(0,0) + T(2,1)*pmut(1,0) + T(2,2)*pmut(2,0) + T(2,3);
	Hnum1 = T(0,0)*pmut(0,0) + T(0,1)*pmut(1,0) + T(0,2)*pmut(2,0) + T(0,3);
	Hnum2 = T(1,0)*pmut(0,0) + T(1,1)*pmut(1,0) + T(1,2)*pmut(2,0) + T(1,3);

	Eigen::MatrixXd h(2,1);
	h(0,0) = Hnum1/Hden;
	h(1,0) = Hnum2/Hden;

	Eigen::MatrixXd H = Eigen::MatrixXd::Zero(2,ORDER);	// It is intended to initialize to 0

	H(0,0) = (T(0,0)*Hden - Hnum1*T(2,0))/(Hden*Hden);
	H(1,0) = (T(1,0)*Hden - Hnum2*T(2,0))/(Hden*Hden);

	H(0,1) = (T(0,1)*Hden - Hnum1*T(2,1))/(Hden*Hden);
	H(1,1) = (T(1,1)*Hden - Hnum2*T(2,1))/(Hden*Hden);

	H(0,2) = (T(0,2)*Hden - Hnum1*T(2,2))/(Hden*Hden);
	H(1,2) = (T(1,2)*Hden - Hnum2*T(2,2))/(Hden*Hden);

	omegat_contrib = H.transpose()*Q.inverse()*H;
	ept_contrib = H.transpose()*Q.inverse()*(z - h + H*pmut);
}

// Calculate the contribution given by a measure
void DistanceAsociationEIF::calculateContribution(const Eigen::Ref<const Eigen::MatrixXd>& z, 
const Eigen::Ref<const Eigen::MatrixXd>& h, const Eigen::Ref<const Eigen::MatrixXd>& H, 
const Eigen::Ref<const Eigen::MatrixXd>& pmut, const Eigen::Ref<const Eigen::MatrixXd>& Q, 
Eigen::Ref<Eigen::MatrixXd> ept_contrib, Eigen::Ref<Eigen::MatrixXd> omegat_contrib){
	omegat_contrib = H.transpose()*Q.inverse()*H;
	ept_contrib = H.transpose()*Q.inverse()*(z - h + H*pmut);
}

// Returns the number of trackList elements
int DistanceAsociationEIF::getTrackListSize(){
    return trackList.size();
}

// Return a track from the track list
Track DistanceAsociationEIF::getTrack(unsigned int index){
    return trackList[index];
}

// Set the prediction model
void DistanceAsociationEIF::setPredictionModel(Eigen::MatrixXd m){
    G = m;
}

// Set the prediction model noise
void DistanceAsociationEIF::setPredictionModelTrust(Eigen::MatrixXd m){
    R = m;
}

// Set the noise of the visual measurements
void DistanceAsociationEIF::setVisualMeasureTrust(Eigen::MatrixXd m){
    Qvisual = m;
}

// Set the noise of the GPS measurements
void DistanceAsociationEIF::setGPSMeasureTrust(Eigen::MatrixXd m){
    Qgps = m;
}

// Paint a track vector (trackList)
void DistanceAsociationEIF::print_trackList(){
    printf("\n\nTrackList (%u tracks)\n", (unsigned int)trackList.size());
    for(unsigned int i = 0; i < trackList.size(); i++){
        printf("\n___TRACK %d___ (index: %d)",trackList[i].pairings.common_id,i);
        printf("\n   State -> %.3f,%.3f,%.3f",trackList[i].state.x, trackList[i].state.y, trackList[i].state.z);
        printf("\n   Time Window: ");
        for(unsigned int j = 0; j < trackList[i].stateTW.size(); j++){
            printf(" (%.3f,%.3f,%.3f) -", trackList[i].stateTW[j].x,trackList[i].stateTW[j].y,trackList[i].stateTW[j].z);
        }
        printf("\n   Life time -> %u",trackList[i].life_time);
        printf("\n   Time_since_last_pairing time -> %u",trackList[i].time_since_last_pairing);
        printf("\n   Pairings (device,local_id):");
        for(unsigned int j = 0; j < trackList[i].pairings.targets.size(); j++){
            printf("\n   %d,%d",trackList[i].pairings.targets[j].device_id,trackList[i].pairings.targets[j].target_local_id);
        }
    }
    printf("\n");
}

// Paint a targets3Dtable
void DistanceAsociationEIF::draw_targets3Dtable(unsigned int index){
    if(index < targets3Dtables.size()){
        //printf("\n");
        for(int i = 0; i < targets3Dtables[index].size(); i++){            
            std::cout << "Array procedente de " << targets3Dtables[index][i].source_id << std::endl;        
            if(targets3Dtables[index][i].targets3D.size() > 0){
                for(int j = 0; j < targets3Dtables[index][i].targets3D.size(); j++){
                    std::cout << "target_" << targets3Dtables[index][i].targetsID[j] << ": ";
                    printf("%.3f, %.3f, %.3f\n", targets3Dtables[index][i].targets3D[j].x, targets3Dtables[index][i].targets3D[j].y, targets3Dtables[index][i].targets3D[j].z);
                }
            }
        }
    }
}
// Paint all targets3Dtable
void DistanceAsociationEIF::draw_targets3Dtables(){
    if(targets3Dtables.size() > 0){
        for(int i = 0; i < targets3Dtables.size(); i++){
            printf("\nTabla %d\n", i);
            draw_targets3Dtable(i);
        }
    }
}

void DistanceAsociationEIF::set_asociation_window(int value){
    asociation_window = value;
}
int DistanceAsociationEIF::get_asociation_window(){
    return asociation_window;
}

void DistanceAsociationEIF::set_asociation_th(double value){
    asociation_th = value;
}
double DistanceAsociationEIF::get_asociation_th(){
    return asociation_th;
}

void DistanceAsociationEIF::set_track_asociation_th(double value){
    track_asociation_th = value;
}
double DistanceAsociationEIF::get_track_asociation_th(){
    return track_asociation_th;
}

void DistanceAsociationEIF::set_max_track_time_without_pairing_refresh(unsigned int value){
    max_time_without_pairing_refresh = value;
}
unsigned int DistanceAsociationEIF::get_max_track_time_without_pairing_refresh(){
    return max_time_without_pairing_refresh;
}

void DistanceAsociationEIF::set_visual_tracks_common_id_offset(unsigned int value){
    visual_tracks_common_id_offset = value;
}
unsigned int DistanceAsociationEIF::get_visual_tracks_common_id_offset(){
    return visual_tracks_common_id_offset;
}

void DistanceAsociationEIF::printPairingCoupleVector(std::vector <pairingCouple> v){
    if(v.size() > 0){
        for(int i = 0; i < v.size(); i++){
            printf("id1: %d, id2: %d -> %f\n", v[i].target1.id.target_local_id, v[i].target2.id.target_local_id, v[i].d);
        }
    }
        printf("\n");
}

void DistanceAsociationEIF::drawMarkedTargets(std::vector <std::vector <bool>> m){
    printf("\n\nmarkedTargets (%u devices)\n", (unsigned int)m.size());
    if(m.size() > 0){
        for(unsigned int i = 0; i < m.size(); i++){
            printf("\ndevice %d -> ", i);
            for(unsigned int j = 0; j < m[i].size(); j++){            
                if(m[i][j])
                    printf("1 ");
                else
                    printf("0 ");
            }
        }
    }
    printf("\n");
}

void DistanceAsociationEIF::set_fixed_sigmaGPS(bool value){
    fixed_sigmaGPS = value;
}
bool DistanceAsociationEIF::get_fixed_sigmaGPS(){
    return fixed_sigmaGPS;
}
