#include <Track.h>

// Constructor
Track::Track(){
}
Track::Track(pairing pairings0, geometry_msgs::Point state0, unsigned int time_since_last_pairing0, unsigned int life_time0){
    pairings = pairings0;
    state = state0;
    time_since_last_pairing = time_since_last_pairing0;
    life_time = life_time0;

    // Initialization of state time window
    // The state passed as argument is the first element of the window
    stateTW.push_back(state0);
}
// Destructor
Track::~Track(){}

// Add a new status to the status time window of a track in the track list
// When the states time window is full, the old states are erased
void Track::pushStateTimeWindow(geometry_msgs::Point state, int asociation_window){
    // The new table is added to the start of the vector
    stateTW.push_front(state);
    // If the maximum size for the time window has been exceeded, the last element is deleted
    if(stateTW.size() > asociation_window){
        stateTW.pop_back();
    }
}