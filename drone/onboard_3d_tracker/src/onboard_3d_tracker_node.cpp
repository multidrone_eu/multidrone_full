#include "ros/ros.h"
#include <geometry_msgs/PoseStamped.h>
#include <string>
#include <DistanceAsociationEIF.h>
#include "onboard_3d_tracker/MeasurementInfo.h"
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <signal.h>

// Namespaces
using namespace ros;

/***** DEFINITIONS *****/
// Filter timings
#define DEFAULTALGORITMRATE 1
#define MAXCONTRIBUTIONWAITTIMEDEFAULT 0.02

// Camera
#define FOCAL_LENGTH_DEFAULT 0.0014451673
#define PIXEL_SIZE_DEFAULT 0.00000155
#define FX_DEFAULT 932.366017
#define FY_DEFAULT 932.366017
#define CX_DEFAULT 960.5
#define CY_DEFAULT 540.5

// Offset of gimbal with respect to drone frame
// #define GIMBAL_OFFSET_X_DEFAULT -0.051
// #define GIMBAL_OFFSET_Y_DEFAULT 0.0
// #define GIMBAL_OFFSET_Z_DEFAULT -0.162

// Information filter
#define ORDER 6
#define SIGMAMODEL 10
#define SIGMACAM 0.01	// Desviación típica de las medidas visuales
#define SIGMAGPS 1	// Desviación típica de las medidas gps

// TARGET
#define GPS_ALTITUDE_DEFAULT 0
/************************/

/*** STRUCTURES ***/
/******************/

/***** DECLARATION (Y DEFINITION) OF FUNCTIONS *****/
void tracking2DCB(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& pose2D);

void targetsGPSCB(const multidrone_msgs::TargetStateArray::ConstPtr& gpsinfo);

void RX_EIF_msg(const onboard_3d_tracker::EIFasoc_P2P::ConstPtr &eifmsg);

void tracking3D_calculate(const ros::TimerEvent& event);

void tracking3D_calculate_end(const ros::TimerEvent& event);

Eigen::Matrix4d PoseStamped2Hmatrix(geometry_msgs::PoseStamped poseStamp);

Eigen::Quaterniond QuatFromTwoVectors(Eigen::Vector3d u, Eigen::Vector3d v);
/***************************************************/

/***** GLOBAL VARIABLES *****/
int id;	// Own ID
int group_id;

//Subscripción ros
Subscriber sub_tracking2D;
Subscriber sub_targets_gps;
Subscriber sub_internal_topic;

// Publicación ros
Publisher pub_tracking3D_pose;
Publisher pub_internal_topic;

//Publisher pubTargetPos, pubTargetGPS;	// MATLAB

// Camera parameters
double fx, fy, cx, cy, focal_length, pixel_size;

// Offset of gimbal with respect to drone frame
std::vector <double> gimbalOffset;
std::vector <double> visualOffset;

// Transform of the drone camera
Eigen::Matrix4d globalToCamera;
Eigen::Matrix4d RZ180;

/*** EIF ***/
unsigned int nseq;

double Tp;	// Time between iterations
double sigmaModel; 	// Standard deviation of the model
double sigmaCam, sigmaGPS; 	// Standard deviation of the observation model for both types of measurements
bool fixed_sigmaGPS;				// Indicates whether the fixed Qgps or the Qgps_k that comes with the gps measurement is used
Eigen::MatrixXd I(ORDER,ORDER);	// Identity matrix
Eigen::MatrixXd R(ORDER,ORDER);	// Variance matrix of the prediction model
Eigen::MatrixXd G(ORDER,ORDER);	// Model jacobian
Eigen::MatrixXd Qvisual(2,2);	// Variance matrix of the observation model for visual measurements
Eigen::MatrixXd Qgps(3,3);	// Variance matrix of the observation model for GPS measurements

Eigen::VectorXd initialState(ORDER,1);	// Initial state

Eigen::MatrixXd sigmat0(ORDER,ORDER);	// Initial and restart sigma

Eigen::MatrixXd sigmat1(ORDER,ORDER);
Eigen::MatrixXd ept1(ORDER,1);

onboard_3d_tracker::EIFasoc_P2P enviapMean;
onboard_3d_tracker::EIFasoc_P2P enviaContribution;
onboard_3d_tracker::Contribution contribution;
std::vector <onboard_3d_tracker::Contribution> contrib_vector;
onboard_3d_tracker::MeasurementInfo measure;
std::vector <onboard_3d_tracker::MeasurementInfo> targetMeasurements;

Eigen::Quaterniond oriresQ;
Eigen::Vector3d rotationZero;
Eigen::Vector3d orires;

// Publication of results
multidrone_msgs::TargetStateArray tracking3Dresult;
std::vector<multidrone_msgs::TargetState> targetEstimationList;
multidrone_msgs::TargetState auxTargetState;
Track auxTrack;

int lastnContributions;	// DEBUG

Eigen::Matrix4d drone_pose;
Eigen::Matrix4d cam_ori;
Eigen::Quaterniond cam_oriQ;

Eigen::Matrix4d poseCam;
Eigen::MatrixXd aux_z(2,1);	// Last measurement
std::vector <Eigen::MatrixXd> z;	// Last measurement of each target

// Projection
std::vector <geometry_msgs::Point> targets3Darray;       // List of targets3D
std::vector <unsigned short int> IDtargets3Darray;       // List of IDs of targets3D before association 
Eigen::Vector3d ps, po; // Points that form the line where a target is located
Eigen::Vector3d v; // Direction vector of the line where the target is located
Eigen::Vector4d ps_cam; // Auxiliary point where the position of the point where the line is located is stored
                        // the target, in the camera reference system
geometry_msgs::Point proj_res;  // projection result
double GPS_altitude;			// Height inside the target

// Tracking algorithm execution control
ros::Timer timer;
double tracking3D_rate;
double maxContributionWaitTime;
ros::Timer waitContributionTimer;

// ASOCIATION
int asociation_window;
double asociation_th, track_asociation_th;
DistanceAsociationEIF asoc;
std::vector <onboard_3d_tracker::EIFasoc_P2P> EIFasocTable;    // Vector messages with lists of targets3D

int max_track_time_without_measures;	// Number of iterations without measurements before a track is deleted

// Node Handler ROS pointer
ros::NodeHandle* pnh;	// Let access the ROS node handler out of main function
/*******************************************************/

int main(int argc, char **argv)
{
	ros::init(argc, argv, "onboard_3d_tracker_node");

	// Variable Initialization
	ros::NodeHandle nh("");
	pnh = &nh;

	ros::NodeHandle nhPrivate("~");

	// Transformation matrix
	Eigen::Matrix4d RY90;	
	RY90 <<  0.0, 0.0, 1.0, 0.0,
					 0.0, 1.0, 0.0, 0.0,
					-1.0, 0.0, 0.0, 0.0,
					 0.0, 0.0, 0.0, 1.0;

	Eigen::Matrix4d RZ90n;
	RZ90n <<  0.0, 1.0, 0.0, 0.0,
					 -1.0, 0.0, 0.0, 0.0,
					  0.0, 0.0, 1.0, 0.0,
					  0.0, 0.0, 0.0, 1.0;

	globalToCamera = RY90*RZ90n;

	// EIF initialization
	nseq = 0;

	/*** READ PARAMETERS ***/
	// Parámetros launch
	std::string internal_topic;
	
 	if(!nhPrivate.getParam("drone_id", id)){
   		id = 1;
 	}

 	if(!nhPrivate.getParam("group_id", group_id)){
   		group_id = 0;	// 0 indicates that you can work with any group
 	}

 	if(!nhPrivate.getParam("sigmaModel", sigmaModel)){
   		sigmaModel = SIGMAMODEL;
 	}

 	if(!nhPrivate.getParam("sigmaCam", sigmaCam)){
   		sigmaCam = SIGMACAM;
 	}

	if(!nhPrivate.getParam("sigmaGPS", sigmaGPS)){
   		sigmaGPS = SIGMAGPS;
 	}

	if(!nhPrivate.getParam("fixed_sigmaGPS", fixed_sigmaGPS)){
   		fixed_sigmaGPS = false;
 	}

	if(!nhPrivate.getParam("/focal_length", focal_length)){
   		focal_length = FOCAL_LENGTH_DEFAULT;
 	}
	
	if(!nhPrivate.getParam("/pixel_size", pixel_size)){
   		pixel_size = PIXEL_SIZE_DEFAULT;
 	}
	
	std::vector<double> camera_matrix;
	if(!nhPrivate.getParam("/camera_matrix/data", camera_matrix)){
		fx = FX_DEFAULT;
		fy = FY_DEFAULT;
		cx = CX_DEFAULT;
		cy = CY_DEFAULT;
 	}else{
		fx = camera_matrix[0];
		fy = camera_matrix[4];
		cx = camera_matrix[2];
		cy = camera_matrix[5];
	}

	// GPS height relative to the ground
	if(!nhPrivate.getParam("GPS_altitude", GPS_altitude)){
   		GPS_altitude = GPS_ALTITUDE_DEFAULT;
 	}
	
	// Visual mark relative position to the GPS position of the target
	if(!nhPrivate.getParam("visual_offset_toGPS", visualOffset)){
   		visualOffset.assign(3,0.0);
 	}

	if(!nhPrivate.getParam("gimbalOffset", gimbalOffset)){
   		gimbalOffset.assign(3,0.0);
 	}

 	if(!nhPrivate.getParam("internal_topic", internal_topic)){	// Communications between Tracking 3D nodes of different drones
   		internal_topic = "/comModule";
 	}

 	if(!nhPrivate.getParam("tracking3D_rate", tracking3D_rate)){
   		tracking3D_rate = DEFAULTALGORITMRATE;
 	}

 	if(!nhPrivate.getParam("maxContributionWaitTime", maxContributionWaitTime)){
   		maxContributionWaitTime = MAXCONTRIBUTIONWAITTIMEDEFAULT;
 	}

	if(!nhPrivate.getParam("asociation_window", asociation_window)){
   		asociation_window = 20;
 	}

    if(!nhPrivate.getParam("asociation_th", asociation_th)){
   		asociation_th = 0.3;
 	}

	if(!nhPrivate.getParam("track_asociation_th", track_asociation_th)){
   		track_asociation_th = 0.3;
 	}

	if(!nhPrivate.getParam("max_track_time_without_measures", max_track_time_without_measures)){
   		max_track_time_without_measures = 30;
 	}
	/***********************/

	I = Eigen::MatrixXd::Identity(ORDER,ORDER);
	R = I*sigmaModel*sigmaModel;
	R(0,3) = sigmaModel*sigmaModel;
	R(1,4) = sigmaModel*sigmaModel;
	R(2,5) = sigmaModel*sigmaModel;
	R(3,0) = sigmaModel*sigmaModel;
	R(4,1) = sigmaModel*sigmaModel;
	R(5,2) = sigmaModel*sigmaModel;
	Qvisual = Eigen::MatrixXd::Identity(2,2)*sigmaCam*sigmaCam;
	Qgps = Eigen::MatrixXd::Identity(3,3)*sigmaGPS*sigmaGPS;
	Tp = 1.0/tracking3D_rate;
	initialState << 0, 0, 0, 0, 0, 0;

	cam_ori(3,0) = 0;
	cam_ori(3,1) = 0;
	cam_ori(3,2) = 0;
	cam_ori(3,3) = 1.0;

	ps_cam(3) = 1;  // The fourth element of the homogeneous vector is always 1

	// initial sigma 
	sigmat0 = I*sigmaModel*sigmaModel;
	sigmat0 = sigmat0.inverse();

	sigmat1 = sigmat0;
	ept1 = sigmat1*initialState;
	
	tracking3Dresult.header.frame_id = "/map";
	rotationZero << 1.0, 0.0, 0.0;

	asoc.set_asociation_window(asociation_window);
	asoc.set_asociation_th(asociation_th);
	asoc.set_track_asociation_th(asociation_th);
	// Prediction model (matches your Jacobian in this case)
	G << 1, 0, 0, Tp, 0, 0,
		0, 1, 0, 0, Tp, 0,
		0, 0, 1, 0, 0, Tp,
		0, 0, 0, 1, 0, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 1;
	asoc.setPredictionModel(G);
	asoc.setPredictionModelTrust(R);
	asoc.setVisualMeasureTrust(Qvisual);
	asoc.setGPSMeasureTrust(Qgps);
	asoc.setInitialState(sigmat1,ept1);
	asoc.set_fixed_sigmaGPS(fixed_sigmaGPS);
	asoc.set_max_track_time_without_pairing_refresh(max_track_time_without_measures);

 	// Create the ROS susbcription and publishers
 	// Hay que suscribirse al topic de tracking 2D del propio drone
	sub_tracking2D = nh.subscribe("visual_analysis/target_position_2d", 1, tracking2DCB, ros::TransportHints().tcpNoDelay());

	// You have to subscribe to the topic that sends the GPS measurements of the targets to integrate them into the filter
	sub_targets_gps = nh.subscribe("/targets_pose", 1, targetsGPSCB, ros::TransportHints().tcpNoDelay());

	// You must publish in the topic of internal communications with other Tracking3D nodes
	pub_internal_topic = nh.advertise<onboard_3d_tracker::EIFasoc_P2P> (internal_topic, 1);

	// You must subscribe to the topic of internal communications with other Tracking3D nodes
	sub_internal_topic = nh.subscribe(internal_topic, 1, RX_EIF_msg, ros::TransportHints().tcpNoDelay());

	// You must publish in the 3D tracking topic of the drone itself
	pub_tracking3D_pose = nh.advertise<multidrone_msgs::TargetStateArray> ("target_3d_state", 1);

	// Periodic timer that execute the algoritm with period Tp
	timer = nh.createTimer(ros::Duration(Tp), tracking3D_calculate);

	ros::spin();

	return 0;
}

void tracking3D_calculate(const ros::TimerEvent& event){
  // This function starts the calculation of the target position by
  // a distributed Extended Information Filter

	/*** The last saved result is published ***/
	int trackListSize = asoc.getTrackListSize();
	if(trackListSize > 0){	// Only publish if there are at least one target
		targetEstimationList.clear();
		for(unsigned int i = 0; i < trackListSize; i++){
			// Track i is loaded and the targetState is filled in with the corresponding data
			auxTrack = asoc.getTrack(i);
			auxTargetState.target_id = auxTrack.pairings.common_id;
			auxTargetState.pose.pose.position.x = auxTrack.state.x;
			auxTargetState.pose.pose.position.y = auxTrack.state.y;
			auxTargetState.pose.pose.position.z = auxTrack.state.z;

			// Orientation is calculated as quaternion
			// The orientation is calculated from the velocity vector
			orires(0) = asoc.mut1[i](3);
			orires(1) = asoc.mut1[i](4);
			orires(2) = asoc.mut1[i](5);
			oriresQ = QuatFromTwoVectors(rotationZero, orires.normalized());

			auxTargetState.pose.pose.orientation.x = oriresQ.x();
			auxTargetState.pose.pose.orientation.y = oriresQ.y();
			auxTargetState.pose.pose.orientation.z = oriresQ.z();
			auxTargetState.pose.pose.orientation.w = oriresQ.w();

			targetEstimationList.push_back(auxTargetState);	// The target is added to the list
		}

		// Final message
		tracking3Dresult.header.stamp = ros::Time::now();
		tracking3Dresult.targets = targetEstimationList;

		// Publish
		pub_tracking3D_pose.publish(tracking3Dresult);
	}
	/***********************************************/

	asoc.initilizeEIFiteration();	// EIF matrices are initialized for each track (set to 0 sigmat and ept)
	
	// Received the information from the camera prepared to be processed by the statistical filter
	// In this CB the prediction is made and the predicted average is sent to the rest of the nodes, so that
	// calculate your contribution to the update and send it flying, reaching this node again
	// by CB RX_nodeContributionCB

	// Prediction
	asoc.updateTrackListStatePredictionModel();

	// Prepare the data packet to be sent
	enviapMean.head = 0;
	enviapMean.group_id = group_id;  
	enviapMean.destination_id = 0;	// 0 indicates broadcast. All other nodes must receive the predicted mean (from the same group)
	enviapMean.source_id = id;
	enviapMean.nseq = nseq;
	enviapMean.type = 0; 	// Predicted average type
	// Calculated predicted averages are added to the message
	contrib_vector.clear();
	Track trackinfo;
	for(unsigned int i = 0; i < asoc.getTrackListSize(); i++){
		contribution.state_vector[0] = asoc.pmut[i](0);
		contribution.state_vector[1] = asoc.pmut[i](1);
		contribution.state_vector[2] = asoc.pmut[i](2);
		contribution.state_vector[3] = asoc.pmut[i](3);
		contribution.state_vector[4] = asoc.pmut[i](4);
		contribution.state_vector[5] = asoc.pmut[i](5);
		contrib_vector.push_back(contribution);
	}
	enviapMean.contrib = contrib_vector;

	// The estimate is published so that the remaining drones return their contribution
	pub_internal_topic.publish(enviapMean);

	// After the publication a timer is launched in oneshoot mode that will execute the end of the algorithm calculation
// This will be the maximum time they will have to get the rest of the contributions
	waitContributionTimer = pnh->createTimer(ros::Duration(maxContributionWaitTime), tracking3D_calculate_end, true);

	// Add the prediction to the final estimate of the EIF for each track
	asoc.addPrediction();

	/*** OWN CONTRIBUTION ***/
	// Even if tracking2D has not been received, the own list is put in the last target table to ensure that the table
	// is not empty in any case, even if it has no targets (programming device)

	// The contribution cannot be calculated without having made the association, therefore it is
	// will collect the necessary data for the calculation of the contribution and are saved in a table
	onboard_3d_tracker::EIFasoc_P2P m;
	m.head = 0;
	m.group_id = group_id;  
	m.destination_id = 0;	// 0 indicates broadcast. All other nodes should receive the predicted mean (from the same group)
	m.source_id = id;
	m.nseq = nseq;
	m.type = 1; 	// Type contribution data
	// The list of targets and their ids are also saved
	m.targets3D = targets3Darray;
	m.targetsID = IDtargets3Darray;

	// The camera pose is required
	Eigen::Matrix4d Tpc = poseCam;
	Tpc = Tpc*globalToCamera;
	// The matrix used by the algorithm is the inverse
	Eigen::Matrix4d T = Tpc.inverse();
	for(unsigned int i = 0; i < targets3Darray.size(); i++){		
		// The measurement data of the target i is calculated and the target table with the EIF information begins to be generated.
		measure.z.x = z[i](0);
		measure.z.y = z[i](1);

		// Save (by rows) the transformation matrix
		for(int h = 0; h < 4; h++){
			for(int j = 0; j < 4; j++){
				measure.T[h*4 + j] = T(h,j);
			}
		}

		targetMeasurements.push_back(measure);		
	}	// End of own contribution

	m.measurements = targetMeasurements;
	EIFasocTable.push_back(m);	// is added to the table
	targetMeasurements.clear();

	nseq++;
}

void tracking3D_calculate_end(const ros::TimerEvent& event){
	// At this point the values calculated by the algorithm have been saved, the rest of contributions
	// that arrive until a new calculation is started will not be used (even if the contributions are added,
	// variables are set to 0 when starting a new calculation)

	/*** CLOSURE OF EIF ITERATION ***/
	// The table with contribution data and 3D targets is added to the association tables
	// (latest contribution data and time window)
	asoc.push_EIFasocTableWithGPSdata(EIFasocTable);	

	// With this new targets3D table the pairing table is updated
	asoc.updateTrackListPairings();

	// Once the pairing table has been updated, contributions calculations can be made
	// and make the EIF estimate
	asoc.addContributions();

	// The vector and information matrix are saved for the next iteration
	asoc.saveEstimation();

	// The result is calculated in the dual form (the estimation of the state vector is recovered)
	// and the covariance matrix and the status of each track is updated with it
	asoc.updateTrackListStates();

	// Deletion of vectors and other variables to be filled in the next iteration
	EIFasocTable.clear();	// Clear the table to create a new one in the next iteration
	asoc.clearGPSdataVector();	// DEBUG DELETE (If removed from here, uncomment it from DistanceAsociationEIF)
	/**************************************/
}

// This function is called when an EIF message is received from another node
void RX_EIF_msg(const onboard_3d_tracker::EIFasoc_P2P::ConstPtr &eifmsg){
	// Depending on whether it is a predicted average or contribution message, it is treated differently
	if (eifmsg->type == 0){	// predicted mean
		// The node does not respond to itself
		if(!(eifmsg->source_id == id)){
			// When a predicted average is received, the own contribution data must be sent to the origin
// only sent if the node is the recipient (or the message is broadcast) and the group is the same
			if((eifmsg->group_id == group_id || eifmsg->group_id == 0) && (eifmsg->destination_id == id || eifmsg->destination_id == 0)){
				onboard_3d_tracker::EIFasoc_P2P m;
				m.head = 0;
				m.group_id = group_id;  
				m.destination_id = eifmsg->source_id;	// 0 indicates broadcast. All other nodes must receive the predicted mean (from the same group)
				m.source_id = id;
				m.nseq = nseq;
				m.type = 1; 	// Type contribution data
				// The list of targets and their ids are also saved
				m.targets3D = targets3Darray;
				m.targetsID = IDtargets3Darray;

				// The first step is to read the current position of the camera (the last one read)
				Eigen::Matrix4d Tpc = poseCam;
				Tpc = Tpc*globalToCamera;
				// The matrix used by the algorithm is the inverse
				Eigen::Matrix4d T = Tpc.inverse();
				for(unsigned int i = 0; i < targets3Darray.size(); i++){
					// It is calculated that the measurement data of the target i is collected and the target table is generated with the EIF information
					measure.z.x = z[i](0);
					measure.z.y = z[i](1);

					// The transformation matrix is saved (by columns)
					for(int h = 0; h < 4; h++){
						for(int j = 0; j < 4; j++){
							measure.T[h*4 + j] = T(h,j);
						}
					}

					targetMeasurements.push_back(measure);					
				}
				m.measurements = targetMeasurements;
				// Publish
				pub_internal_topic.publish(m);
				targetMeasurements.clear();
			}
		}
	}else if(eifmsg->type == 1){	// Contribution information
		// Only the message is accepted if this node is for whom it is addressed (and from nodes of the same group)
		// Only nodeContribution with the same sequence number as the current one is accepted
		// In this way the sum of contributions from previous sequences is avoided
		// independent of delays in the arrival of the packages
		if((eifmsg->group_id == group_id || eifmsg->group_id == 0) && (id == eifmsg->destination_id)){

			// The message with the contribution data is saved in the table
			EIFasocTable.push_back(*eifmsg);	// It is added to the table		
		}
	}
}

void targetsGPSCB(const multidrone_msgs::TargetStateArray::ConstPtr& gpsinfo){
	//asoc.manageGPSdata(*gpsinfo);

	// The position of the target will be that of the visual
	// If the GPS position relative to the visual mark is known
	// the position given by the GPS must be adjusted to be integrated with that of the
	// visual accurately
	multidrone_msgs::TargetStateArray auxTargetState = *gpsinfo;
	for(unsigned int i = 0; i < auxTargetState.targets.size(); i++){
		auxTargetState.targets[i].pose.pose.position.x += visualOffset[0];
		auxTargetState.targets[i].pose.pose.position.y += visualOffset[1];
		auxTargetState.targets[i].pose.pose.position.z += visualOffset[2];
	}
	asoc.manageGPSdata(auxTargetState);
}

void tracking2DCB(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& tracking2D){
  /*** EIF measurements ***/
  // The variable that saves the measurements for when the update is performed is updated
  // And for calculating contributions to other nodes
  // A vector with the measurement is saved for each target
  z.clear();	// Previous measurement list deleted
  for(unsigned int i = 0; i < tracking2D->targets.size(); i++){
		if(tracking2D->targets[i].x == -1){
			aux_z(0) = -1;
			aux_z(1) = -1;
		}else{
			//aux_z(0) = (tracking2D->targets[i].x - tracking2D->camera.principalPointX)*tracking2D->camera.pixelSizeX/tracking2D->camera.focalLength;
			//aux_z(1) = -(tracking2D->targets[i].y - tracking2D->camera.principalPointY)*tracking2D->camera.pixelSizeY/tracking2D->camera.focalLength;

			// SIM
			//aux_z(0) = (tracking2D->targets[i].x - cx)/fx;
			//aux_z(1) = -(tracking2D->targets[i].y - cy)/fy;
			// GAZEBO
			aux_z(0) = (tracking2D->targets[i].x - cx)/fx;
			aux_z(1) = (tracking2D->targets[i].y - cy)/fy;
		}
		z.push_back(aux_z);	// Added to the vector of measurements for each target
  }  
  /******************/ 

  /*** Is necesary also the camera pose ***/
  // It can be obtained with drone pose and gimbal orientation
  drone_pose = PoseStamped2Hmatrix(tracking2D->drone_pose);
  cam_oriQ.x() = tracking2D->gimbal.orientation.x;
  cam_oriQ.y() = tracking2D->gimbal.orientation.y;
  cam_oriQ.z() = tracking2D->gimbal.orientation.z;
  cam_oriQ.w() = tracking2D->gimbal.orientation.w;
  cam_ori.block(0,0,3,3) = cam_oriQ.normalized().toRotationMatrix();

  cam_ori(0,3) = gimbalOffset[0];
  cam_ori(1,3) = gimbalOffset[1];
  cam_ori(2,3) = gimbalOffset[2];

  poseCam = drone_pose*cam_ori;
	/****************************************/

  /*** Target list calculation3D (by projection) ***/
  targets3Darray.clear();  // The previous target list is deleted
  IDtargets3Darray.clear();  // The list of previous target IDs is deleted

  // Prepare information that must be sent to other drones for their own association
// and to make your own association
// this information is directly the reconstructed 3D position by projection of each of the targets
// given by tracking2D

// The two points that form the line where the target is located are calculated
// They are calculated in the global reference system
// One of them is the point [0,0,0] in the camera reference system
// this is directly the position of the camera in global
// The Ray where the target is located is heading towards this point
	po(0) = poseCam(0,3);
	po(1) = poseCam(1,3);
	po(2) = poseCam(2,3);
  for(int i = 0; i < tracking2D->targets.size(); i++){
		// Projection calculation for target i
	// Fill in the message vectors (association part)

	// The origin point that forms the line next to point po is the point of the target in
	// the image (sensor plane)
	// First coordinate their coordinates in the camera reference system
		ps_cam(0) = -focal_length;
		ps_cam(1) = (tracking2D->targets[i].x - cx)*pixel_size;
		ps_cam(2) = (tracking2D->targets[i].y - cy)*pixel_size;

		ps = (poseCam*ps_cam).head(3);   // Point of the sensor plane in global coordinates

		v = po - ps;    // Direction vector of the projected ray

		// DEBUG
		proj_res.z = GPS_altitude + visualOffset[2];
		proj_res.x = po(0) + ((proj_res.z-po(2))/v(2))*v(0);
		proj_res.y = po(1) + ((proj_res.z-po(2))/v(2))*v(1);

		//ROS_INFO("Target %d, Projection -> X: %f, Y: %f, Z: %f\n", i, proj_res.x, proj_res.y, proj_res.z);	// DEBUG
		// DEBUG

		// The result is added to the message targets3D vector
		targets3Darray.push_back(proj_res);
		// Your ID is also saved (local ID to the pre-association device)
		IDtargets3Darray.push_back(tracking2D->targets[i].id);
  }
  /***********************************************************/
}

Eigen::Matrix4d PoseStamped2Hmatrix(geometry_msgs::PoseStamped pose){
	Eigen::Matrix4d T = Eigen::MatrixXd::Identity(4,4);
	// Position is obtained
	Eigen::Vector3d P;
	P(0) = pose.pose.position.x;
	P(1) = pose.pose.position.y;
	P(2) = pose.pose.position.z;

	// Orientation is obtained
	Eigen::Quaterniond q;
	q.x() = pose.pose.orientation.x;
	q.y() = pose.pose.orientation.y;
	q.z() = pose.pose.orientation.z;
	q.w() = pose.pose.orientation.w;
	Eigen::Matrix3d Rt = q.normalized().toRotationMatrix();

	//std::cout << Rt << std::endl;
	//ROS_INFO("q: %f, %f, %f, %f", q.x(), q.y(), q.z(), q.w());	// DEBUG

	// The 4x4 matrix of the pose is updated
	T.block(0,3,3,1) = P;
	T.block(0,0,3,3) = Rt;
	T(3,3) = 1.0;

	//ROS_INFO("P>: %f, %f, %f", P(0), P(1), P(2));	// DEBUG
	//std::cout << "\nTrans:\n" << Trans << std::endl;	// DEBUG

	return T;
}

Eigen::Quaterniond QuatFromTwoVectors(Eigen::Vector3d u, Eigen::Vector3d v){
	Eigen::Vector3d w = u.cross(v); 
	Eigen::Quaterniond q(u.dot(v), w(0), w(1), w(2)); 
	q.w() += q.norm(); 
	return q.normalized(); 
}