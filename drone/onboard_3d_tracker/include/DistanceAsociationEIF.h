#include "ros/ros.h"
#include <iostream>
#include <Track.h>
#include "onboard_3d_tracker/Targets3DArray_P2P.h"
#include "onboard_3d_tracker/EIFasoc_P2P.h"
#include <multidrone_msgs/TargetStateArray.h>
#include <multidrone_msgs/TargetState.h>
#include "onboard_3d_tracker/Contribution.h"
#include <math.h>
#include <Eigen/Dense>
#include <vector>
#include <deque>

/***** DEFINITIONS *****/
#define TIME_WINDOW_LENGHT_DEFAULT 10
#define ASOCIATION_TH_DEFAULT 0.2
#define TRACK_ASOCIATION_TH_DEFAULT 0.2
#define MAX_TIME_WITHOUT_PAIRING_REFRESH 30
#define VISUAL_TRACKS_COMMON_ID_OFFSET_DEFAULT 20
#define GPS_VIRTUAL_DEVICE_ID 0
#define ORDER 6
/***********************/

typedef Eigen::Matrix<double, -1, -1, Eigen::ColMajor> Cm;

/*** STRUCTURES ***/
typedef struct target_local_index{
    int device_index;
    int target_local_index;
} target_local_index;

typedef struct target_local{
    target_local_id id;
    geometry_msgs::Point state; // Target state (position)
} target_local;

typedef struct pairingCouple{
    target_local target1;    // Id of the paired item in list 1
    target_local target2;    // Id of the paired item in list 2
    double d;   // Pairing distance (the smaller, the more likely that the pairing is correct)  
} pairingCouple;

typedef struct pairingCoupleIndex{
    target_local_index target1;    // Id of the paired item in list 1
    target_local_index target2;    // Id of the paired item in list 2
    double d;   // Pairing distance (the smaller, the more likely that the pairing is correct)  
} pairingCoupleIndex;
/******************/

class DistanceAsociationEIF
{

public:
    // EIF variables
    std::vector<Eigen::MatrixXd> sigmat1;
    std::vector<Eigen::MatrixXd> psigmat;
    std::vector<Eigen::MatrixXd> sigmat;

    std::vector<Eigen::MatrixXd> ept1;
    std::vector<Eigen::MatrixXd> pept;
    std::vector<Eigen::MatrixXd> ept;

    std::vector<Eigen::MatrixXd> mut1;
    std::vector<Eigen::MatrixXd> pmut;
    std::vector<Eigen::MatrixXd> covariance1;

    // GPS
    onboard_3d_tracker::EIFasoc_P2P lastGPSdata;

    // Constructor & destructor
    DistanceAsociationEIF();
    ~DistanceAsociationEIF();

    // Clear saved GPS measurements so that they are not added and integrated into the filter
    // in the next iteration
    void clearGPSdataVector();

    // This function prepares the GPS measurements to be integrated by the filter
    void manageGPSdata(multidrone_msgs::TargetStateArray gpsdata);

    // This function will add the contributions to each track
    // To do this, it will go through the list of pairings of each track and search if they have arrived
    // measures of the corresponding target of the paired drone
    void addContributions();

    // Function that saves the vector and information matrix for the next iteration
    // It is the last step in the iteration of the EIF filter
    void saveEstimation();

    // Update the status of the tracks with the last calculation made by the EIF filter
    // Also save the filter result in the canonical form of the Kalman filter
    // (mut1 and covariance1)
    // Also add one to the life of the track (suppose this function is called
    // at the end of each iteration)
    // If a target has been alive for too long without the refreshment or addition of a pairing
    // the track is removed due to disuse
    void updateTrackListStates();

    // Add the prediction to the final estimate of the EIF for each track
    void addPrediction();

    // Initialize the variables for the new iteration of the EIF
    void initilizeEIFiteration();

    // Save the last table with sunshine data and EIF
    // Also call push_targets3Dtable to save the corresponding part
    // of the data in the target table of the time window
    void push_EIFasocTable(std::vector <onboard_3d_tracker::EIFasoc_P2P> newTable);
    
    // This function adds a "virtual" device with the latest GPS measures
    // before calling the previous function
    void push_EIFasocTableWithGPSdata(std::vector <onboard_3d_tracker::EIFasoc_P2P> newTable);

    // Enter a new 3D target table to be associated
    void push_targets3Dtable(std::vector <onboard_3d_tracker::Targets3DArray_P2P> newTable);
    
    // Update the track list with a prediction model
    // Call in the prediction phase of the Bayesian filter
    void updateTrackListStatePredictionModel();

    // Update the pairings by calling the two methods
    // of association in the correct order
    void updateTrackListPairings();

    // Update the pairing vector with the targets3D table
    // All targets are added to the process
    void updateTrackListPairingsTimeWindow();

    
    // Update the pairing vector with the targets3D table
    // It should be indicated which targets should be added to the process
    void updateTrackListPairingsTimeWindow(std::vector <std::vector <bool>> markedTargets);

    // Set the initial values ​​of the EIF filter that will be given when creating the track
    void setInitialState(Eigen::MatrixXd sigmat1_m, Eigen::MatrixXd ept1_m);

    // Draw the trackList
    void print_trackList();

    // Set the prediction model
    void setPredictionModel(Eigen::MatrixXd m);
    void setPredictionModelTrust(Eigen::MatrixXd m);
    void setVisualMeasureTrust(Eigen::MatrixXd m);
    void setGPSMeasureTrust(Eigen::MatrixXd m);

    // Devuelve el número de elementos de la trackList
    int getTrackListSize();

    // Returns the number of trackList elements
    Track getTrack(unsigned int index);

    // Draw a targets3Dtable
    void draw_targets3Dtable(unsigned int index);
    void draw_targets3Dtables();

    void set_asociation_window(int value);
    int get_asociation_window();

    void set_asociation_th(double value);
    double get_asociation_th();

    void set_track_asociation_th(double value);
    double get_track_asociation_th();

    void set_fixed_sigmaGPS(bool value);
    bool get_fixed_sigmaGPS();

    void set_max_track_time_without_pairing_refresh(unsigned int value);
    unsigned int get_max_track_time_without_pairing_refresh();

    void set_visual_tracks_common_id_offset(unsigned int value);
    unsigned int get_visual_tracks_common_id_offset();

    
    // Paint the targets marked to be associated using the time window list
    void drawMarkedTargets(std::vector <std::vector <bool>> m);

private:
    std::vector <Track> trackList;  // The list must be sorted by tracks ids
                                    // that way they will always be the first gps tracks and then the visuals
    int asociation_window;
    double asociation_th;       // asociation_threshold
    double track_asociation_th;       // asociation_threshold using EIF updated tracks
    unsigned int max_time_without_pairing_refresh;  // Maximum time that a live track can be without having been that any pairing has been added or refreshed
    std::deque <std::vector <onboard_3d_tracker::Targets3DArray_P2P>> targets3Dtables; // Table of targets3D tables to match
    std::vector <pairing> pairings; // Pairing vector
    std::vector <std::vector <bool>> markedTargets;
    Eigen::MatrixXd G;  // Prediction model
    Eigen::MatrixXd R;  // Prediction model noise
    Eigen::MatrixXd Qvisual;  // Visual measurements noise
    Eigen::MatrixXd Qgps;  // GPS measurements noise
    bool fixed_sigmaGPS;	// Indicates whether the fixed Qgps or the Qgps_k that comes with the GPS measurement is used
    Eigen::MatrixXd Hgps;  // Jacobian observation model for gps measurements

    unsigned int visual_tracks_common_id_offset;    // As of which id of the purely visual tracks are assigned (tracks with GPS will have ids below this value)

    // EIF info
    Eigen::MatrixXd sigmat1_initial;
    Eigen::MatrixXd ept1_initial;

    std::vector <onboard_3d_tracker::EIFasoc_P2P> EIFasocTable; // Save the association table with EIF data from the last iteration

    // GPS
    //onboard_3d_tracker::EIFasoc_P2P lastGPSdata;

    // Sort the trackList by the id of the tracks
    void sortTrackList();

    // Change the id of a track and reorder the trackList
    void changeTrackID(unsigned int track_index, unsigned int newID);

    // Add two targets, the first of them type track gps, to the trackList
    void addGPStargetPair2TrackList(target_local GPStarget, target_local visualTarget);

    // Delete a track from the track list given its index
    // Also delete the corresponding items in the EIF data lists
    // Returns -1 if the track could not be deleted or 0 in case of success
    int eraseTrack(unsigned int index);

    // Calculate the contribution given the visual measurements of a target
    void calculateVisualContribution(const Eigen::Ref<const Eigen::MatrixXd>& z, const Eigen::Ref<const Eigen::Matrix4d>& T, const Eigen::Ref<const Eigen::MatrixXd>& pmut, const Eigen::Ref<const Eigen::MatrixXd>& Q, Eigen::Ref<Eigen::MatrixXd> ept_contrib, Eigen::Ref<Eigen::MatrixXd> omegat_contrib);

    // Calculate the contribution given by a measure
    void calculateContribution(const Eigen::Ref<const Eigen::MatrixXd>& z, const Eigen::Ref<const Eigen::MatrixXd>& h, const Eigen::Ref<const Eigen::MatrixXd>& H, const Eigen::Ref<const Eigen::MatrixXd>& pmut, const Eigen::Ref<const Eigen::MatrixXd>& Q, Eigen::Ref<Eigen::MatrixXd> ept_contrib, Eigen::Ref<Eigen::MatrixXd> omegat_contrib);


    // Update the track list pairings using the distance of each target
    // of the new measurement table with the states of each track
    // Call in the update phase after adding the new measurement table
    // with the push_targets3Dtable function
    void updateTrackListPairingsByState(std::vector <Track> &tl, std::vector <std::vector <bool>> &markedTargets);

    // Update the track list with a prediction model
    // Call in the prediction phase of the Bayesian filter
    void updateTrackListStatePredictionModel(std::vector <Track> &tl);

    // Add a proposed pairing to the track list
    // It is assumed that the target has not been reliably associated
    // for the tracks predicted by the EIF and therefore the pairing targets
    // do not appear in the list of Tracks, and therefore a track must be created. The only one
    // exception is that in this same iteration the target has been matched in another
    // comparison of drone targets and then if there is a track with any of the
    // targets to add from the pairing. In this case, the missing target is added to the
    // track, a new one is not created
    int addPairingCouple2TrackList(pairingCouple pair);

    // Search for a target in the trackList, if it is found it returns the index
    // of the track where it is located. In case of not finding it returns -1
    int findTargetLocalTrackList(std::vector <Track> &tl, target_local_id target);

    // Search for a target in the trackList, if it is found it returns the index
    // of the track where it is located. In case of not finding it returns -1
    int findTargetLocalPairingList(pairing p, target_local_id target);

    // Search the index of the first visual track in the track list
    // If there are no visual tracks, the offset of
    // position of the visual tracks
    // This function can also be used to calculate the number of GPS tracks in the list
    int findFirstVisualTrackIndex();
    int GPSTracksCount();
    
    // Add a track to the track list with a paired pair
    // The common id assigned to the track pairings is returned
    // The first free id is chosen in the list
    // The track is created with a first pairingCouple
    // It is assumed that no other track exists with the pair of targets added
    int addVisualTrack(pairingCouple pair);
    int addVisualTrack(target_local t);
    // Add a track to the track list with past matches as an argument to the past state as an argument
    void addTrack(pairing pairings, geometry_msgs::Point trackPos);
  
    // Add a track using the GPS id itself
    // It is assumed that each GPS has a different id and therefore there will not be two tracks
    // with the same id
    // Reorder the list by adding the new track
    void addGPSTrack(int common_id, geometry_msgs::Point pos);
    // This version of the function also allows you to add a visual target in the pairings of the new track
    void addGPSTrack(int common_id, geometry_msgs::Point pos, target_local_id visual_target_id);

    // Search the track list for the next free ID
    int nextFreeIDTrackList();

    // Search the track list for the next free ID of the visual tracks
    int nextFreeIDVisualTrackList();
    
    // Search the pairing list if there is a target of a drone in a pairing
    // if so, it returns the number of deleted targets and deletes the pairing,
    // otherwise it does nothing and returns -1
    int eraseTargetFromPairing(pairing &p, target_local_id target);

    // Search the track list if there is a target for a drone in a pairing
    // if so it returns the number 0 and clears the pairing (s),
    // otherwise it does nothing and returns -1
    int eraseTargetFromTrackVector(std::vector <Track> &tl, target_local_id target);
 
    // Search (if any) for a track with the id passed as an argument in the track list
    // If yes, return the track index in the list
    int findTrack(unsigned int track_id);

    // Search (if any) for a GPS track with the id passed as an argument in the track list
    // If yes, return the track index in the list
    // Otherwise return -1
    int findGPSTrack(unsigned int track_id);

    // Calculate the distance between two targets3D of two different drones in one of the saved tables
    // from the list of targets3D tables
    // There must be at least two drones in the table
    // Returns 0 if the indicated targtes3D have been found, otherwise it returns -1
    int findTargets3Ddistance(unsigned int table_index, target_local_id target1, target_local_id target2, double &d);

    // Calculate the distance between a target and a track in an instant time t
    // If there is no track or measure of that target at that moment -1 is returned
    // Returns 0 if the track and target have been found, and therefore the calculated distance
    int findTrackTargets3Ddistance(unsigned int table_index, unsigned int track_index, target_local_id target, double &d);

    // Search (if it exists) the index of the vector of a drone in the target3D table given its id (the id of the drone)
    // Return -1 if you don't find the drone, or the index if you find it
    int findDeviceTableIndex(unsigned int table_index, unsigned int drone_id);

    // Search for a value in an unsigned 16-bit integer vector (ID vectors) 
    // Returns -1 if it doesn't find the value
    int findIndexUint16(unsigned short int d, std::vector<unsigned short int> v);
    
    // Calculate the Euclidean distance between two points
    double euclideanDistance(geometry_msgs::Point p1, geometry_msgs::Point p2);

    void initialize();

    void printPairingCoupleVector(std::vector <pairingCouple> v);

};
