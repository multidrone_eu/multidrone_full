#include "ros/ros.h"
#include <vector>
#include <deque>
#include <geometry_msgs/Point.h>

/*** STRUCTURES ***/
typedef struct target_local_id{
    int device_id;
    int target_local_id;
} target_local_id;

typedef struct pairing{
    std::vector <target_local_id> targets;    // List of ids for each target drone of this match [drone_id,target_id]
    int common_id;  // Common ID assigned with which the matching targets will be treated
} pairing;
/******************/

class Track
{
public:

pairing pairings;   // pairings
geometry_msgs::Point state; // Common track state (position)
unsigned int time_since_last_pairing;   // Time without having been refreshed a pairing with the track. Parameter used to delete an unused track for too long
unsigned int life_time;     // Track life time (iterations)
std::deque <geometry_msgs::Point> stateTW;

// Constructor & destructor
Track();
Track(pairing p, geometry_msgs::Point state0, unsigned int time_since_last_pairing, unsigned int life_time);
~Track();

// Add a new status to the status time window of a track in the track list
// When the state time window is full, the old states are deleted
void pushStateTimeWindow(geometry_msgs::Point state, int asociation_window);

};