// c++
#include <string>
#include <vector>
#include <iostream>
#include <pthread.h>
#include <thread>
#include <chrono>

// ROS
#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Header.h>
#include <std_msgs/Int8.h>
#include <actionlib/server/simple_action_server.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Point.h>
#include <image_transport/image_transport.h>

// OpenCv
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cv_bridge/cv_bridge.h>

// Boost
#include <boost/filesystem.hpp>

// multidrone_ros_msges
#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <visualanalysis_msgs/Verify.h>
#include <visualanalysis_msgs/Detect.h>

// Darknet
#ifdef GPU_MODE

#include "cuda_runtime.h"
#include "curand.h"
#include "cublas_v2.h"

#endif
extern "C" {
#include "network.h"
#include "detection_layer.h"
#include "region_layer.h"
#include "cost_layer.h"
#include "utils.h"
#include "parser.h"
#include "box.h"
#include "darknet_ros/image_interface.h"
#include <sys/time.h>
}

extern "C" void ipl_into_image(IplImage *src, image im);

extern "C" image ipl_to_image(IplImage *src);

extern "C" void show_image_cv(image p, const char *name, IplImage *disp);

#ifdef DARKNET_FILE_PATH
std::string darknetFilePath_ = DARKNET_FILE_PATH;
#else
#error Path of darknet repository is not defined in CMakeLists.txt.
#endif

using namespace std;

network *net;
bool net_initialized = false;
char **names;
image **alphabet;
float nms = .45;
int minSize = 0;
int nClasses = 1;
cv::Mat current_img;
ros::Publisher detections_pub;
ros::Publisher ver_rois_pub;
float thresh = 0.2;
float hier_thresh = 0.5;
int counter = 0;
bool save_detections;
string save_path;
int init_w_img = 0;

cv::Rect ConvertToCVRect(visualanalysis_msgs::TargetPositionROI2D msg) {
    return cv::Rect(msg.x, msg.y, msg.w, msg.h);
}

cv::Rect CheckLimits(cv::Rect roi) {
    if (0 <= roi.x && 0 <= roi.width && roi.x + roi.width <= current_img.cols && 0 <= roi.y && 0 <= roi.height &&
        roi.y + roi.height <= current_img.rows) {
        return roi;
    } else {
        roi.x = std::max(0, roi.x);
        roi.y = std::max(0, roi.y);
        if (roi.height == -1) roi.height = current_img.rows - roi.y;
        if (roi.width == -1) roi.width = current_img.cols - roi.x;
        roi.height = std::min(roi.height, current_img.rows - roi.y);
        roi.width = std::min(roi.width, current_img.cols - roi.x);
        return roi;
    }
}

cv::Rect CheckLimitsContext(cv::Rect roi) {
    int pad = (int) (1.5 * sqrt(roi.width * roi.height));
    cv::Rect new_roi = roi;
    new_roi.x -= pad;
    new_roi.y -= pad;
    new_roi.width += 2 * pad;
    new_roi.height += 2 * pad;
    if (0 <= new_roi.x && 0 <= new_roi.width && new_roi.x + new_roi.width <= current_img.cols && 0 <= new_roi.y &&
        0 <= new_roi.height &&
        new_roi.y + new_roi.height <= current_img.rows) {
        return new_roi;
    } else {
        new_roi.x = std::max(0, new_roi.x);
        new_roi.y = std::max(0, new_roi.y);
        new_roi.height = std::min(new_roi.height, current_img.rows - roi.y - 1);
        new_roi.width = std::min(new_roi.width, current_img.cols - roi.x - 1);
        return new_roi;
    }
}

bool DetectionRequest(visualanalysis_msgs::Detect::Request &req, visualanalysis_msgs::Detect::Response &res) {
    if (!net_initialized) {
        return false;
    }
    visualanalysis_msgs::TargetPositionROI2D detarea = req.Area;
    // message that rois are going to be published (for visualisation purposes)
    visualanalysis_msgs::TargetPositionROI2DArray RoiArray;

    minSize = req.Area.size;
    cv::Rect cv_detarea = CheckLimits(ConvertToCVRect(detarea));
    ROS_INFO("Detection around ROI: %d %d %d %d", cv_detarea.x, cv_detarea.y, cv_detarea.width, cv_detarea.height);
    if (init_w_img == 1) {
        current_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
        init_w_img = 0;
    }
    if (!current_img.empty() && cv_detarea.width != 0) {
        cv::Mat img_detarea = current_img(cv_detarea);
        IplImage *img_ = new IplImage(img_detarea);
        image im = ipl_to_image(img_);
        image sized = letterbox_image(im, net->w, net->h);
        layer l = net->layers[net->n - 1];
        float *X = sized.data;
        network_predict(net, X);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, 0, 0, 1, &nboxes);
        if (nms) do_nms_sort(dets, l.w * l.h * l.n, l.classes, nms);

        ROS_INFO("Im : %dx%d", im.w, im.h);
        int i, j;
        int count = 0;
        for (i = 0; i < nboxes; ++i) {
            for (j = 0; j < nClasses; ++j) {
                if (dets[i].prob[j] > 0) {

                    visualanalysis_msgs::TargetPositionROI2D msgROI;
                    msgROI.id = count;
                    msgROI.det_score = dets[i].prob[j];
                    ROS_INFO("Box : %.2f, %.2f, %.2f, %.2f  %.2f", dets[i].bbox.x, dets[i].bbox.y, dets[i].bbox.w,
                             dets[i].bbox.h, dets[i].prob[j]);
                    msgROI.x = (int) ((dets[i].bbox.x - dets[i].bbox.w / 2.) * im.w);
                    msgROI.y = (int) ((dets[i].bbox.y - dets[i].bbox.h / 2.) * im.h);
                    msgROI.w = (int) (dets[i].bbox.w * im.w);
                    msgROI.h = (int) (dets[i].bbox.h * im.h);
                    count++;

                    visualanalysis_msgs::TargetPositionROI2D temp = msgROI;
                    temp.x = temp.x + detarea.x;
                    temp.y = temp.y + detarea.y;
                    ROS_INFO("Detection : %d, %d, %d, %d with score %f", msgROI.x, msgROI.y, msgROI.w, msgROI.h,
                             msgROI.det_score);
                    res.ArrayROI.push_back(temp);
                    RoiArray.targets.push_back(temp);
                }
            }
        }
        ROS_INFO("****  %d DETECTIONS ", count);
        free_detections(dets, nboxes);
        detections_pub.publish(RoiArray);
        return true;
    }
    ROS_INFO("Problem with image acquisition.");
    return false;
}


float Iou_ratio(cv::Rect A, cv::Rect B) {
    float ratio = (A & B).area() / float((A | B).area());
    return ratio;
}


visualanalysis_msgs::TargetPositionROI2D ReidentificationIoU(vector<visualanalysis_msgs::TargetPositionROI2D> ArrayROI,
                                                             visualanalysis_msgs::TargetPositionROI2D ver_msg) {

    if (ArrayROI.size() == 1) {
        return ArrayROI[0];
    }

    visualanalysis_msgs::TargetPositionROI2D corr;
    double maxratio = 0;
    cv::Rect trk = ConvertToCVRect(ver_msg);
    int id = -1;

    for (int i = 0; i < ArrayROI.size(); i++) {
        ROS_INFO("detection  %d %d %d %d", ArrayROI[i].x, ArrayROI[i].y, ArrayROI[i].w, ArrayROI[i].h);
        cv::Rect det = ConvertToCVRect(ArrayROI[i]);
        float ratio = Iou_ratio(det, trk);

        if (ratio > maxratio) {
            maxratio = ratio;
            id = i;
        }
        ROS_INFO("ratio of id %d: %f", ArrayROI[i].id, ratio);
    }

    if (maxratio > 0) {
        return ArrayROI[id];
    } else {
        return ArrayROI[0];
    }

}

bool VerificationRequest(visualanalysis_msgs::Verify::Request &req, visualanalysis_msgs::Verify::Response &res) {
    visualanalysis_msgs::TargetPositionROI2D ver_msg;
    visualanalysis_msgs::TargetPositionROI2DArray RoiArray;
    ver_msg = req.ROI;
    cv::Rect ver_roi = ConvertToCVRect(ver_msg);
    ver_roi = CheckLimitsContext(ver_roi);
    ROS_INFO("Verifying ROI with id %d %d %d %d %d", req.ROI.id, req.ROI.x, req.ROI.y, req.ROI.w, req.ROI.h);
    if (!current_img.empty()) {
        cv::Mat img_ver = current_img(ver_roi);
        IplImage *img_ = new IplImage(img_ver);
        float x = std::max(ver_roi.x, ver_roi.y);
        int base = 128;
        int result = base + 2 * base * round((x - base) / base);
        if (net->w != result || net->h != result)
            resize_network(net, result, result);

        image im = ipl_to_image(img_);
        image sized = letterbox_image(im, net->w, net->h);
        layer l = net->layers[net->n - 1];
        float *X = sized.data;
        network_predict(net, X);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        if (nms > 0) do_nms_sort(dets, nboxes, l.classes, nms);

        int i, j;
        int count = 0;
        for (i = 0; i < nboxes; ++i) {
            for (j = 0; j < nClasses; ++j) {
                if (dets[i].prob[j] > 0) {
                    visualanalysis_msgs::TargetPositionROI2D msgROI;
                    if (dets[i].bbox.w < 0.02 || dets[i].bbox.h < 0.02)
                        continue;
                    msgROI.id = count;
                    msgROI.det_score = dets[i].prob[j];
                    msgROI.x = (int) ((dets[i].bbox.x - dets[i].bbox.w / 2.) * im.w);
                    msgROI.y = (int) ((dets[i].bbox.y - dets[i].bbox.h / 2.) * im.h);
                    msgROI.w = (int) (dets[i].bbox.w * im.w);
                    msgROI.h = (int) (dets[i].bbox.h * im.h);
                    count++;

                    visualanalysis_msgs::TargetPositionROI2D temp = msgROI;
                    temp.x = temp.x + ver_roi.x;
                    temp.y = temp.y + ver_roi.y;
                    RoiArray.targets.push_back(temp);
                }
            }
        }
        ROS_INFO("****  %d VERIFICATION DETECTIONS ", (int) RoiArray.targets.size());
        free_detections(dets, nboxes);
        ver_rois_pub.publish(RoiArray);

        if (RoiArray.targets.size() == 1) {
            ROS_INFO("ONE box found, return score : %f", RoiArray.targets[0].det_score);
            res.ver_score = RoiArray.targets[0].det_score;
        } else if (RoiArray.targets.size() > 1) {
            ROS_INFO("!!!! Warning: multiple boxes found, checking IoU...");
            visualanalysis_msgs::TargetPositionROI2D corr = ReidentificationIoU(RoiArray.targets, ver_msg);
            res.ver_score = corr.det_score;
        } else if (RoiArray.targets.size() == 0) {
            ROS_INFO("!!! no boxes found");
            res.ver_score = 0;
        }
        return true;
    }
    return false;
}

void imageCallback(const sensor_msgs::ImageConstPtr &msg) {
    current_img = cv_bridge::toCvShare(msg, "rgb8")->image;
    ROS_INFO("frame %d", (int) msg->header.seq);
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "darknet_ros");
    ros::NodeHandle nodeHandle("");
    ros::NodeHandle pnh("~");

    static int drone_id_;
    nodeHandle.param<int>("drone_id", drone_id_, 1);

    image_transport::ImageTransport it(nodeHandle);
    string image_topic = "/drone_" + std::to_string(drone_id_) + "/shooting_camera";
    image_transport::Subscriber subscriber_image = it.subscribe(image_topic, 25, imageCallback);
    ROS_INFO("Reading images from %s", image_topic.c_str());

    std::string cfgPath;
    char *cfg;
    std::string weightsPath;
    char *weights;
    std::string namesPath;

    std::string path = ros::package::getPath("drone_visual_analysis") + string("/");

    nodeHandle.getParam("/init_tracker_mode", init_w_img);

    pnh.getParam("cfg_path", cfgPath);
    ROS_INFO("Model: %s", cfgPath.c_str());
    cfgPath = path + string("models/detectors/yolo/cfg/") + cfgPath;
    cfg = new char[cfgPath.length() + 1];
    strcpy(cfg, cfgPath.c_str());

    pnh.getParam("weights_path", weightsPath);
    ROS_INFO("Weights: %s", weightsPath.c_str());
    weightsPath = path + string("models/detectors/yolo/weights/") + weightsPath;
    weights = new char[weightsPath.length() + 1];

    strcpy(weights, weightsPath.c_str());

    net = load_network(cfg, weights, 0);
    net_initialized = true;
    set_batch_network(net, 1);
    srand(2222222);

    ros::ServiceServer service = nodeHandle.advertiseService("detection", DetectionRequest);
    detections_pub = nodeHandle.advertise<visualanalysis_msgs::TargetPositionROI2DArray>("detection_rois", 1);

    ros::spin();
    return 0;
}
