#include <ros/ros.h>
#include <ros/spinner.h>
#include <ros/package.h>
#include <cstdlib>
#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <drone_visual_analysis/Verify_classifier_update.h>
#include <drone_visual_analysis/Reidentify.h>
#include <multidrone_msgs/FollowTarget.h>
#include <multidrone_msgs/TargetType.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3.h>
#include <multidrone_msgs/CameraStatus.h>
#include <multidrone_msgs/GimbalStatus.h>
#include <visualanalysis_msgs/Verify.h>
#include <visualanalysis_msgs/Detect.h>
#include <visualanalysis_msgs/Track.h>
#include <interface_test/interface_test.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <string>
#include <fstream>
#include <math.h>

using namespace cv;
using namespace std;

visualanalysis_msgs::Track trk_srv;
ros::Publisher det_area_pub;

bool it21_23 = false;
int im_counter = 0;
string image_topic;

// tracking params
bool start = false;
float search_variable = 1.2;
bool target_found = false;

// services
ros::ServiceClient trk_client;
ros::ServiceClient reid_client;
ros::ServiceClient ver_client;
ros::ServiceClient det_client;
ros::ServiceClient ver_upd_client;
int init_tracker_mode;

// subscribed messages
cv::Mat current_img;
sensor_msgs::Image current_img_msg;
bool has_gps_pos = false;
geometry_msgs::Vector3 last_gps_position;
ros::Time last_gps_time;
uint last_img_seq;


int TargetType;

float trk_score_th_;
float ver_score_th_;
float ver_score_th;

int minSize = 32;
int minSize_cntr = 0;

bool init_with_roi = false;
visualanalysis_msgs::TargetPositionROI2D init_roi;

// convert cv Rect to visualanalysis_msgs::TargetPositionROI2D
visualanalysis_msgs::TargetPositionROI2D ConvertToROI(cv::Rect cvrect) {
    visualanalysis_msgs::TargetPositionROI2D msgROI;
    msgROI.x = cvrect.x;
    msgROI.y = cvrect.y;
    msgROI.w = cvrect.width;
    msgROI.h = cvrect.height;
    msgROI.id = -1;
    return msgROI;
}

visualanalysis_msgs::TargetPositionROI2D multiply_ROI(visualanalysis_msgs::TargetPositionROI2D msg, float rate) {
    int center_x = msg.x - msg.w / 2;
    int center_y = msg.y - msg.h / 2;
    msg.w = (int) (msg.w * rate);
    msg.h = (int) (msg.h * rate);
    msg.x = center_x - msg.w / 2;
    msg.y = center_y - msg.h / 2;
    return msg;
}

// convert visualanalysis_msgs::TargetPositionROI2D to cv Rect
cv::Rect ConvertToCVRect(visualanalysis_msgs::TargetPositionROI2D msg) {
    cv::Rect cvrect;
    cvrect.x = msg.x;
    cvrect.y = msg.y;
    cvrect.width = msg.w;
    cvrect.height = msg.h;
    return cvrect;
}

float Area(cv::Rect A) {
    return float(A.width * A.height);
}


float Euclidean_distance(cv::Point2d a, cv::Point2d b) {
    return sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}


void print_info(double ver_score_th_, double trk_score_th_, int drone_id_) {
    cout << "drone_id: " << drone_id_ << endl;
    cout << "ver_score_th: " << ver_score_th_ << endl;
    cout << "trk_score_th: " << trk_score_th_ << endl;
}

// IT 21-23
void imageCallbackTest(const sensor_msgs::ImageConstPtr &msg) {
    run_test(string("it21_23"), msg->header.stamp, it21_23);
    // save image to check with sent images
    current_img = cv_bridge::toCvShare(msg, "bgr8")->image;
}


void imageCallback(const sensor_msgs::ImageConstPtr &msg) {
    //ROS_INFO();
    current_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    last_img_seq = msg->header.seq;
}

void get_gps_pixel_position(const geometry_msgs::Vector3 &msg) {
    has_gps_pos = true;
    last_gps_position = msg;
    last_gps_time = ros::Time::now();
}

visualanalysis_msgs::TargetPositionROI2D FindClosestTarget(vector<visualanalysis_msgs::TargetPositionROI2D> ArrayROI) {
    int id = 0;
    double min_distance = 10000.;

    cv::Point2d center;
    if (has_gps_pos) {
        ROS_INFO("Finding target closest to GPS position %.2f,%.2f", last_gps_position.x, last_gps_position.y);
        center.x = last_gps_position.x;
        center.y = last_gps_position.y;
    } else {
        ROS_INFO("Finding target closest to center of frame");
        center.x = current_img.cols / 2;
        center.y = current_img.rows / 2;
    }

    for (int i = 0; i < ArrayROI.size(); i++) {


        cv::Point2d a(int(ArrayROI[i].x + ArrayROI[i].w / 2), int(ArrayROI[i].y + ArrayROI[i].h / 2));
        float distance = Euclidean_distance(a, center);
        ROS_INFO("detection  %d %d %d %d dist: %.4f", ArrayROI[i].x, ArrayROI[i].y, ArrayROI[i].w, ArrayROI[i].h,
                 distance);
        if (distance < min_distance) {
            id = i;
            min_distance = distance;
        }
    }
    ROS_INFO("Choosing detection %d %d %d %d", ArrayROI[id].x, ArrayROI[id].y, ArrayROI[id].w, ArrayROI[id].h);
    return ArrayROI[id];
}

bool gps_in_frame() {
    bool in_frame = true;
    if (has_gps_pos && ros::Time::now() - last_gps_time < ros::Duration(2)) {
        if (last_gps_position.x > current_img.cols) {
            ROS_INFO("GPS off limits (target is to the right)");
            in_frame = false;
        }
        if (last_gps_position.y > current_img.rows) {
            ROS_INFO("GPS off limits (target is lower)");
            in_frame = false;
        }
        if (last_gps_position.z < 0) {
            ROS_INFO("GPS off limits (target is behind the camera)");
            in_frame = false;
        }
        if (last_gps_position.x < 0) {
            ROS_INFO("GPS off limits (target is to the left)");
            in_frame = false;
        }
        if (last_gps_position.y < 0) {
            ROS_INFO("GPS off limits (target is higher)");
            in_frame = false;
        }
    } else {
        in_frame = false;
    }
    return in_frame;
}

cv::Rect CheckLimits(cv::Rect roi) {
    cv::Rect _roi = roi & cv::Rect(0, 0, current_img.cols, current_img.rows);
    return _roi;
}


visualanalysis_msgs::TargetPositionROI2D CheckLimits(visualanalysis_msgs::TargetPositionROI2D roi) {
    cv::Rect cvroi = ConvertToCVRect(roi);
    cv::Rect _roi = cvroi & cv::Rect(0, 0, current_img.cols, current_img.rows);
    visualanalysis_msgs::TargetPositionROI2D VSroi = ConvertToROI(_roi);
    return VSroi;
}

visualanalysis_msgs::TargetPositionROI2D CheckLimits(visualanalysis_msgs::TargetPositionROI2D &roi, cv::Mat img) {
    roi.x = std::max(0, roi.x);
    roi.y = std::max(0, roi.y);
    roi.w = std::min(roi.w, img.cols - roi.x);
    roi.h = std::min(roi.h, img.cols - roi.h);
    return roi;
}


visualanalysis_msgs::TargetPositionROI2D CalculateArea(visualanalysis_msgs::TargetPositionROI2D ROI) {
    int avg = (int) (search_variable * (ROI.w + ROI.h)) / 2;
    visualanalysis_msgs::TargetPositionROI2D search_area;
    search_area.x = ROI.x - avg;
    search_area.y = ROI.y - avg;
    search_area.w = ROI.w + avg * 2;
    search_area.h = ROI.h + avg * 2;

    search_area = CheckLimits(search_area);
    return search_area;
}

void drawRect(cv::Rect rect, cv::Scalar color, int size) {
    rect = CheckLimits(rect);
    cv::rectangle(current_img, rect, color, size);
}

void drawScore(float score, cv::Rect rect, int x, int y, cv::Scalar color) {
    char s[30];
    snprintf(s, 10, "%f", score);
    cv::putText(current_img, s, Point(rect.x - x, rect.y - y), FONT_HERSHEY_PLAIN, 2, color, 2);
}


void drawID(int score, cv::Rect rect, int x, int y, cv::Scalar color) {
    char s2[30];
    snprintf(s2, 10, "%d", score);
    cv::putText(current_img, s2, Point(rect.x - x, rect.y - y), FONT_HERSHEY_PLAIN, 2, color, 2);
}


float Iou_ratio(cv::Rect A, cv::Rect B) {
    float ratio = (A & B).area() / float((A | B).area());
    return ratio;
}


visualanalysis_msgs::TargetPositionROI2D ReidentificationIoU(vector<visualanalysis_msgs::TargetPositionROI2D> ArrayROI,
                                                             visualanalysis_msgs::TargetPositionROI2D ver_msg) {
    if (ArrayROI.size() == 1) {
        return ArrayROI[0];
    }

    visualanalysis_msgs::TargetPositionROI2D corr;
    double maxratio = 0;
    cv::Rect trk = ConvertToCVRect(ver_msg);
    int id = -1;

    for (int i = 0; i < ArrayROI.size(); i++) {
        cv::Rect det = ConvertToCVRect(ArrayROI[i]);
        float ratio = Iou_ratio(det, trk);
        if (ratio > maxratio) {
            maxratio = ratio;
            id = i;
        }
    }

    if (maxratio > 0) {
        return ArrayROI[id];
    } else {
        return ArrayROI[0];
    }
}


bool Reinit_tracking(visualanalysis_msgs::TargetPositionROI2D corr) {
    trk_srv.request.targets.push_back(corr);
    trk_srv.request.cmd = 1;
    if (corr.w != 0 && corr.h != 0) {
        if (trk_client.call(trk_srv)) {
            ROS_INFO("tracking reinitialized");
            return true;
            minSize = corr.w;
        }
    }
    return false;
}


bool Detect(visualanalysis_msgs::TargetPositionROI2D msg) {
    visualanalysis_msgs::Detect det;

    det.request.Area = CalculateArea(msg);
    det.request.Area.size = min(current_img.cols, minSize);

    det_area_pub.publish(det.request.Area);

    det.request.id = msg.id;
    visualanalysis_msgs::TargetPositionROI2D corr;

    ROS_INFO("Sending a service of detection around area %d %d %d %d", det.request.Area.x, det.request.Area.y,
             det.request.Area.w, det.request.Area.h);
    if (det_client.call(det)) {
        if (det.response.ArrayROI.size() > 0) {
            target_found = true;
            corr = ReidentificationIoU(det.response.ArrayROI, msg);
            corr.id = msg.id;
            if (!current_img.empty()) {
                cv::Rect cvcorr(corr.x, corr.y, corr.w, corr.h);
                cvcorr = CheckLimits(cvcorr);
                corr.x = cvcorr.x;
                corr.y = cvcorr.y;
                corr.w = cvcorr.width;
                corr.h = cvcorr.height;

                if (Reinit_tracking(corr)) {
                    return true;
                    minSize_cntr = 0;
                }
            }
        }

        if (det.response.ArrayROI.size() == 0) {
            if (search_variable < 100.00) {
                search_variable = search_variable * 1.5;
                minSize_cntr++;
            }
        }
    }
    return false;

}

// callback for when tracker produces a new ROI
void Joint_Tracking_and_Detection(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr &msg) {
    visualanalysis_msgs::Detect det_srv;
    visualanalysis_msgs::Verify ver_srv;
    drone_visual_analysis::Reidentify reid_srv;
    drone_visual_analysis::Verify_classifier_update ver_cls_upd_srv;

    // loop over possible targets
    for (int i = 0; i < msg->targets.size(); i++) {
        if (gps_in_frame()) {
            float distance_pixels_x = abs(last_gps_position.x - (msg->targets[i].x + msg->targets[i].w / 2.));
            float distance_pixels_y = abs(last_gps_position.y - (msg->targets[i].y + msg->targets[i].h / 2.));
            if (distance_pixels_x > 0.3 * current_img.cols || distance_pixels_y > 0.3 * current_img.rows) {
//                ROS_INFO("Target too far from GPS signal");
                if (msg->targets[i].trk_score > trk_score_th_) {
//                    ROS_INFO("But tracker confidence is high, tracking...");
                } else {
                    target_found = false;
                }
            }
        }
        if (!target_found) {
            ROS_INFO("Target lost, performing detection...");
            //************************************************************
            // if target is lost, perform only detection until it is found
            //************************************************************
            // search area is entire frame
            det_srv.request.Area.w = current_img.cols;
            det_srv.request.Area.h = current_img.rows;
            det_srv.request.Area.x = 0;
            det_srv.request.Area.y = 0;

            std_msgs::Header header;
            header.seq = last_img_seq;
            header.stamp = ros::Time::now();
            cv_bridge::CvImage img_bridge;
            img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::RGB8, current_img);
            img_bridge.toImageMsg(det_srv.request.img);

            if (det_client.call(det_srv)) {
                ROS_INFO("Found %d possible targets", (int) det_srv.response.ArrayROI.size());
                int max_id = -1;
                float max_score = 0;
                if (gps_in_frame()) {
                    ver_srv.request.ROI = FindClosestTarget(det_srv.response.ArrayROI);
                    ver_srv.request.ROI.id = msg->targets[i].id;
                    ver_client.call(ver_srv);
                    ROS_INFO("Re-id request for ROI id %d: %.6f", ver_srv.request.ROI.id, ver_srv.response.ver_score);
                    if (ver_srv.response.ver_score > ver_score_th) {
                        visualanalysis_msgs::TargetPositionROI2D new_roi = ver_srv.request.ROI;
                        new_roi.id = msg->targets[i].id;
                        ROS_INFO("Re-initializing tracker with ROI %d: [%d %d %d %d]", new_roi.id, new_roi.x, new_roi.y,
                                 new_roi.w, new_roi.h);
                        //re-init tracker
                        target_found = true;
                        ver_score_th = ver_score_th_;
                        trk_srv.request.targets.clear();
                        trk_srv.request.targets.push_back(new_roi);
                        trk_srv.request.cmd = 1;
                        trk_client.call(trk_srv);

                        // set last_box in verifier
                        ver_cls_upd_srv.request.target = new_roi;
                        ver_upd_client.call(ver_cls_upd_srv);
                        continue;
                    }
                }
                // No GPS or mismatch, look for best match with verifier
                for (int j = 0; j < det_srv.response.ArrayROI.size(); j++) {
                    det_srv.response.ArrayROI[j].id = msg->targets[i].id;
                    img_bridge.toImageMsg(reid_srv.request.img);
                    reid_srv.request.target = det_srv.response.ArrayROI[j];
                    reid_client.call(reid_srv);
                    det_srv.response.ArrayROI[j].trk_score = reid_srv.response.trk_score;
                    ver_srv.request.ROI = det_srv.response.ArrayROI[j];

                    ver_client.call(ver_srv);
                    ROS_INFO("Re-id request for ROI id %d: %.6f", det_srv.response.ArrayROI[j].id,
                             ver_srv.response.ver_score);
                    if (ver_srv.response.ver_score > 0.9) {
                        max_id = j;
                        max_score = ver_srv.response.ver_score;
                        break;  // for speed considerations
                    }
                    if (ver_srv.response.ver_score > max_score) {
                        max_score = ver_srv.response.ver_score;
                        max_id = j;
                    }
                }
                if (max_id != -1 and max_score > ver_score_th) {
                    visualanalysis_msgs::TargetPositionROI2D new_roi = det_srv.response.ArrayROI[max_id];
                    new_roi.id = msg->targets[i].id;
                    ROS_INFO("Re-initializing tracker with ROI %d: [%d %d %d %d]", new_roi.id, new_roi.x, new_roi.y,
                             new_roi.w, new_roi.h);
                    //re-init tracker
                    target_found = true;
                    ver_score_th = ver_score_th_;
                    trk_srv.request.targets.clear();
                    trk_srv.request.targets.push_back(new_roi);
                    trk_srv.request.cmd = 1;
                    trk_client.call(trk_srv);

                    // set last_box in verifier
                    ver_cls_upd_srv.request.target = new_roi;
                    ver_upd_client.call(ver_cls_upd_srv);
                } else {
                    ROS_INFO("None of the possible targets match");
                }
            } else {
                ROS_ERROR("Error communicating with detector!");
            }
            if (!target_found) {
                ver_score_th *= 0.8;
            }
        } else {
            // ROS_INFO("Tracker confidence: %.2f/%.2f", msg->targets[i].trk_score, trk_score_th_);
            //************************************************************
            // if target is not lost, verification when needed
            //************************************************************
            if (msg->targets[i].trk_score < trk_score_th_) {
                target_found = false;
            } else {
                // set last_box in verifier
                ver_cls_upd_srv.request.target = msg->targets[i];
                ver_upd_client.call(ver_cls_upd_srv);
            }
        }
    }
    trk_srv.request.targets.clear();
}


bool follow_target(multidrone_msgs::FollowTarget::Request &req, multidrone_msgs::FollowTarget::Response &res) {
    ROS_INFO("Received request to follow target, waiting for image message from %s...", image_topic.c_str());
    target_found = false;

    visualanalysis_msgs::Detect det_srv;
    visualanalysis_msgs::TargetPositionROI2D tracked_roi;
    visualanalysis_msgs::TargetPositionROI2D det_area;

    if (current_img.cols == 0 || current_img.rows == 0) {
        ROS_INFO("Image hasn't been received yet.");
        return false;
    }
    cv::Mat img_ = current_img.clone();

    while (!target_found) {
        // if in GPS mode but no gps signal available, return false
        if (req.target_type.type == 1) {
            if (!gps_in_frame()) {
                ROS_INFO("GPS position not available or not in frame limits!");
                continue;
            }
        }
        // detection area is entire image
        det_srv.request.Area.x = 0;
        det_srv.request.Area.y = 0;
        det_srv.request.Area.w = img_.cols;
        det_srv.request.Area.h = img_.rows;
        det_srv.request.id = req.target_id;
        det_srv.request.TargetType = req.target_type.type;      // target_type.type = 1 means visual+gps
        ROS_INFO("Received request to follow target id %d", req.target_id);

        // pass image to detector if in init_tracker_mode 1
        if (init_tracker_mode == 1) {
            std_msgs::Header header;
            header.seq = last_img_seq;
            header.stamp = ros::Time::now();
            cv_bridge::CvImage img_bridge;
            img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::RGB8, img_);
            img_bridge.toImageMsg(det_srv.request.img);
        }

        // call detection service, which returns ArrayROI of possible targets
        if (det_client.call(det_srv)) {
            ROS_INFO("Detection service responded...");
            if (!det_srv.response.ArrayROI.empty()) {
                ROS_INFO("...not empty!");
                tracked_roi = FindClosestTarget(det_srv.response.ArrayROI);

                // if has_gps, check if tracked target is compliant
                if (has_gps_pos) {
                    if (!gps_in_frame()) {
                        target_found = false;
                        return false;
                    }
                }

                target_found = true;

                tracked_roi.id = req.target_id;
                tracked_roi.frame = last_img_seq;
                tracked_roi = CheckLimits(tracked_roi, img_);
                ROS_INFO("Tracked ROI %d: [%d %d %d %d]", tracked_roi.id, tracked_roi.x, tracked_roi.y, tracked_roi.w,
                         tracked_roi.h);

                trk_srv.request.targets.clear();
                trk_srv.request.targets.push_back(tracked_roi);
                trk_srv.request.cmd = 0;    // command for initializing tracker
                if (init_tracker_mode == 1) {
                    std_msgs::Header header;
                    header.seq = last_img_seq;
                    header.stamp = ros::Time::now();
                    cv_bridge::CvImage img_bridge;
                    img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::RGB8, img_);
                    img_bridge.toImageMsg(trk_srv.request.img);
                }
                ROS_INFO("Calling tracker initialization service with ROI(s): ");
                for (int j = 0; j < trk_srv.request.targets.size(); j++)
                    ROS_INFO("  [%d %d %d %d]", trk_srv.request.targets[j].x, trk_srv.request.targets[j].y,
                             trk_srv.request.targets[j].w, trk_srv.request.targets[j].h);
                trk_client.call(trk_srv);

                // update roi in verifier
                drone_visual_analysis::Verify_classifier_update ver_cls_upd_srv;
                ver_cls_upd_srv.request.target = tracked_roi;
                ver_upd_client.call(ver_cls_upd_srv);
                return true;
            }
            ROS_INFO("...but detection array is empty!");
        }
    }
    return false;
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "master_visual_analysis.launch");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    string path_to_node = ros::package::getPath("drone_visual_analysis") + string("/");

    nh.param<float>("trk_score_th", trk_score_th_, 0.3);
    pnh.param<float>("ver_score_th", ver_score_th_, 0.2);
    ROS_INFO("TRK THRESH: %.5f", trk_score_th_);
    ROS_INFO("VER THRESH: %.5f", ver_score_th_);
    pnh.getParam("init_tracker_mode", init_tracker_mode);
    if (init_tracker_mode == 1) {
        ROS_INFO("Initializing tracker with same frame.");
    }

    static int drone_id_;

    pnh.param<int>("/drone_id", drone_id_, 1);
    ROS_INFO("DRONE ID: %d", drone_id_);

    image_transport::ImageTransport it(nh);
    if (!nh.hasParam(string("/drone_") + to_string(drone_id_) + "/drone_visual_analysis/camera_topic")) {
        image_topic = string("/drone_") + to_string(drone_id_) + string("/shooting_camera");
    } else {
        nh.getParam(string("/drone_") + to_string(drone_id_) + "/drone_visual_analysis/camera_topic", image_topic);
    }

    ROS_INFO("Image topic: %s", image_topic.c_str());
    image_transport::Subscriber subscriber_image = it.subscribe(image_topic, 1, imageCallback);

    string roi_topic = string("visual_analysis/target_position_roi_2d_array/");
    ros::Subscriber sub2 = nh.subscribe(roi_topic, 1, Joint_Tracking_and_Detection);

    ros::Subscriber sub_gps = nh.subscribe("pixel_position", 1, get_gps_pixel_position);

    det_area_pub = nh.advertise<visualanalysis_msgs::TargetPositionROI2D>("det_Area", 1);

    // Initialize service
    ros::ServiceServer service = nh.advertiseService("follow_target", follow_target);

    // Initialize tracker
    visualanalysis_msgs::TargetPositionROI2DArray RoiArray;

    // Used services
    trk_client = nh.serviceClient<visualanalysis_msgs::Track>("tracking");
    reid_client = nh.serviceClient<drone_visual_analysis::Reidentify>("reidentification");
    ver_client = nh.serviceClient<visualanalysis_msgs::Verify>("verification");
    det_client = nh.serviceClient<visualanalysis_msgs::Detect>("detection");
    ver_upd_client = nh.serviceClient<drone_visual_analysis::Verify_classifier_update>("verifier_update");

    ros::AsyncSpinner spinner(4);
    spinner.start();
    ros::waitForShutdown();
//    ros::spin();
    return 0;
}
