#!/usr/bin/env python

import numpy as np
import cv2
import rospy
import rospkg
from sensor_msgs.msg import Image
from visualanalysis_msgs.msg import TargetPositionROI2D, TargetPositionROI2DArray
from geometry_msgs.msg import Vector3
from visualanalysis_msgs.srv import *
from drone_visual_analysis.srv import *
from multidrone_msgs.srv import *
from cv_bridge import CvBridge, CvBridgeError


class MVA:
    def __init__(self, drone_id):
        self.drone_id = drone_id
        self.bridge = CvBridge()
        self.img = None

        # params
        self.trk_thresh = rospy.get_param("trk_score_th", 0.5)
        self.ver_thresh = rospy.get_param("~ver_score_th", 0.2)
        self.ver_thresh_orig = self.ver_thresh
        self.init_w_img = rospy.get_param("~init_tracker_mode", 0)
        rospy.loginfo('TRK: {}, VER: {}'.format(self.trk_thresh, self.ver_thresh))

        # callbacks
        self.image_topic = rospy.get_param('/drone_{}/drone_visual_analysis/camera_topic'.format(drone_id),
                                           '/drone_{}/shooting_camera'.format(drone_id))
        self.image_sub = rospy.Subscriber(self.image_topic, Image, self.image_callback,
                                          queue_size=1,
                                          buff_size=2 ** 24)
        self.roi_topic = '/drone_{}/visual_analysis/target_position_roi_2d_array'.format(drone_id)
        rospy.Subscriber(self.roi_topic, TargetPositionROI2DArray, self.joint_detection_tracking)
        self.gps_topic = '/drone_{}/pixel_position'.format(drone_id)
        rospy.Subscriber(self.gps_topic, Vector3, self.gps_callback, queue_size=50)
        self.timer = rospy.Timer(rospy.Duration(4), self.timed_detection)

        # services
        self.follow_srv = rospy.Service("/drone_{}/follow_target".format(drone_id), FollowTarget, self.follow_target)
        self.trk_client = rospy.ServiceProxy("tracking", Track)
        self.reid_client = rospy.ServiceProxy("reidentification", Reidentify)
        self.det_client = rospy.ServiceProxy("detection", Detect)
        self.det_ver_cl = rospy.ServiceProxy("verification_by_detection", Verify)
        self.ver_client = rospy.ServiceProxy("verification", Verify)
        self.ver_upd_cl = rospy.ServiceProxy("verifier_update", Verify_classifier_update)

        # publishers
        self.area_pub = rospy.Publisher("detection_area", TargetPositionROI2D, queue_size=1)

        # maps, etc
        self.is_gps_target = {}
        self.target_found = False
        self.has_gps = False
        self.redetect_attempts = 0
        self.target = None

    def image_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "rgb8")
            self.img = cv2.resize(cv_image, (640, 480))
        except CvBridgeError as e:
            rospy.logerr(e)
        return

    def gps_callback(self, msg):
        self.has_gps = True
        self.gps_time = rospy.Time.now()
        self.gps_msg = msg

    def gps_pos_valid(self):
        valid = True
        if self.has_gps:
            time_diff = rospy.Time.now() - self.gps_time
            rospy.loginfo(
                "GPS pos {},{},{} - {}s ago".format(self.gps_msg.x, self.gps_msg.y, self.gps_msg.z, time_diff.to_sec()))
            if time_diff.to_sec() > 2.:
                rospy.loginfo("GPS signal received too long ago")
            if self.gps_msg.x < 0 or self.gps_msg.x < 0.2 * self.img.shape[1]:
                rospy.loginfo("GPS off limits (target is to the left)")
                valid = False
            elif self.gps_msg.x > self.img.shape[1] or self.gps_msg.x > 0.8 * self.img.shape[1]:
                rospy.loginfo("GPS off limits (target is to the right)")
                valid = False
            if self.gps_msg.y < 0 or self.gps_msg.y < 0.2 * self.img.shape[0]:
                rospy.loginfo("GPS off limits (target is lower)")
                valid = False
            elif self.gps_msg.y > self.img.shape[0] or self.gps_msg.y > 0.8 * self.img.shape[0]:
                rospy.loginfo("GPS off limits (target is higher)")
                valid = False
            if self.gps_msg.z < 0:
                rospy.loginfo("GPS off limits (target is behind camera)")
                valid = False
        else:
            self.has_gps = False
            valid = False
        return valid

    def follow_target(self, req):
        rospy.loginfo("Received request to follow target, waiting for image from {}".format(self.image_topic))
        self.target_found = False

        det_srv = DetectRequest()
        trk_srv = TrackRequest()

        r = rospy.Rate(1)
        while not self.target_found:
            rospy.loginfo("Looking for target...")
            # look for target
            if self.img is None:
                rospy.loginfo("Image not received yet")
                r.sleep()
                continue

            current_img = self.img.copy()
            # check for combined visual/gps target
            if req.target_type.type == 1 and not self.gps_pos_valid():
                rospy.loginfo("Invalid or unavailable GPS position")
                r.sleep()
                continue

            det_srv.Area.x, det_srv.Area.y = 0, 0
            det_srv.Area.w, det_srv.Area.h = current_img.shape[1], current_img.shape[0]
            det_srv.id = req.target_id

            if self.init_w_img:
                rospy.loginfo("Passing current image to detector...")
                image_message = self.bridge.cv2_to_imgmsg(current_img, encoding="bgr8")
                det_srv.img = image_message
                trk_srv.img = image_message

            det_srv_res = self.det_client.call(det_srv)
            if det_srv_res.ArrayROI:
                # not empty
                rospy.loginfo('Found {} possible targets'.format(len(det_srv_res.ArrayROI)))
                gps = self.gps_msg if self.gps_pos_valid() and det_srv.TargetType == 1 else None
                roi, dist = find_closest(det_srv_res.ArrayROI, current_img.shape[0], current_img.shape[1], gps)
                if det_srv.TargetType == 1 and not self.gps_pos_valid():
                    rospy.loginfo("GPS lost...")
                    r.sleep()
                    continue

                # if we're here we found the target
                roi.id = req.target_id
                roi = check_limits(roi, current_img.shape[0], current_img.shape[1])

                # send track request
                trk_srv.targets = []
                trk_srv.targets.append(roi)
                trk_srv.cmd = 0  # 0: initialize tracker
                self.trk_client.call(trk_srv)
                self.target_found = True

                # update roi in verifier
                ver_srv = Verify_classifier_updateRequest()
                ver_srv.target = roi
                self.ver_upd_cl.call(ver_srv)

                if gps is not None:
                    self.is_gps_target[roi.id] = True
                else:
                    self.is_gps_target[roi.id] = False
                return True
            else:
                rospy.loginfo("No targets found")
                r.sleep()
                continue
        return False

    def joint_detection_tracking(self, msg):
        BB = msg.targets[0]
        rospy.loginfo("[Im.no: %d] [Roi: %d %d %d %d]", msg.seq, BB.x, BB.y, BB.w, BB.h)

        # subscribe to tracker ROI, decide whether it's going ok
        det_req = DetectRequest()
        trk_req = TrackRequest()
        ver_req = VerifyRequest()
        reid_req = ReidentifyRequest()
        ver_upd_req = Verify_classifier_updateRequest()

        n_targets = len(msg.targets)
        for i in range(n_targets):
            target = msg.targets[i]
            if not self.target_found:
                # ************************************************************
                #   target is lost, ask for detection until it is found again
                # ************************************************************
                current_img = self.img.copy()
                det_req.Area.x, det_req.Area.y = 0, 0
                det_req.Area.w, det_req.Area.h = current_img.shape[1], current_img.shape[0]
                det_req.id = target.id
                if self.init_w_img:
                    rospy.loginfo("Passing current image to detector...")
                    image_message = self.bridge.cv2_to_imgmsg(current_img, encoding="bgr8")
                    det_req.img = image_message
                    reid_req.img = image_message
                    trk_req.img = image_message

                det_res = self.det_client.call(det_req)
                n_candidates = len(det_res.ArrayROI)
                if det_res.ArrayROI:
                    # not empty
                    rospy.loginfo("Found {} possible targets".format(n_candidates))
                    if self.is_gps_target[target.id] and self.gps_pos_valid():
                        # find closest and check with verifier
                        best_match, best_dist = find_closest(det_res.ArrayROI, 0, 0, self.gps_msg)
                        ver_req.ROI = best_match
                        ver_req.ROI.id = target.id
                        reid_req.id = target.id
                        reid_req.target = best_match
                        reid_res = self.reid_client.call(reid_req)
                        best_match.trk_score = reid_res.trk_score
                        ver_res = self.ver_client.call(ver_req)
                        if ver_res.ver_score > self.ver_thresh:
                            best_match.id = target.id
                            self.target_found = True
                            self.ver_thresh = self.ver_thresh_orig
                            trk_req.cmd = 1  # update tracker
                            best_match = check_limits(best_match, current_img.shape[1], current_img.shape[0])
                            trk_req.targets.append(best_match)
                    else:
                        # visual only target, look for best match
                        max_id = -1
                        max_ver = 0.
                        for j in range(n_candidates):
                            candidate = det_res.ArrayROI[j]
                            candidate.id = target.id
                            reid_req.id = target.id
                            reid_req.target = candidate
                            reid_res = self.reid_client.call(reid_req)
                            candidate.trk_score = reid_res.trk_score
                            ver_req.ROI = candidate
                            ver_res = self.ver_client.call(ver_req)
                            if ver_res.ver_score > max_ver:
                                max_ver = ver_res.ver_score
                                max_id = j
                        if max_id > -1 and max_ver > self.ver_thresh:
                            best_match = det_res.ArrayROI[max_id]
                            best_match.id = target.id
                            self.target_found = True
                            self.target = best_match
                            trk_req.targets.append(best_match)
                            trk_req.cmd = 1
                            self.trk_client.call(trk_req)
                            # update verifier
                            ver_upd_req.target = best_match
                            self.ver_upd_cl.call(ver_upd_req)
                        else:
                            self.target_found = False
                else:
                    rospy.loginfo("No candidates found")
                if not self.target_found:
                    self.ver_thresh *= 0.9
            else:
                # ************************************************************
                #       if target is not lost, verification when needed
                # ************************************************************
                if target.trk_score < self.trk_thresh:
                    self.target_found = False
                else:
                    # update verifier
                    ver_upd_req.target = target
                    self.ver_upd_cl.call(ver_upd_req)
                    self.target = target

    def timed_detection(self, timer):
        if self.target_found and self.img is not None and self.target:
            rospy.loginfo("Periodic detection ({} failed attempts so far)".format(self.redetect_attempts))
            current_img = self.img.copy()
            det_req = DetectRequest()
            det_area = TargetPositionROI2D()
            target_area = self.target.w * self.target.h
            target_center = self.target.x + self.target.w / 2., self.target.y + self.target.h / 2.
            context_area = (target_area / 0.05) * 1.2 ** self.redetect_attempts
            dim_w = np.sqrt(context_area)
            dim_h = dim_w
            det_area.x = target_center[0] - dim_w / 2.
            det_area.y = target_center[1] - dim_h / 2.
            det_area.w = dim_w
            det_area.h = dim_h
            det_area = check_limits(det_area, current_img.shape[0], current_img.shape[1])
            rospy.loginfo("Detection in {},{},{},{}".format(det_area.x, det_area.y, det_area.w, det_area.h))
            det_req.Area = det_area
            det_req.TargetType = 1
            self.area_pub.publish(det_area)
            if self.init_w_img:
                image_message = self.bridge.cv2_to_imgmsg(current_img, encoding="bgr8")
                det_req.img = image_message
            det_res = self.det_client.call(det_req)
            if det_res.ArrayROI:
                rospy.loginfo("Found {} targets".format(len(det_res.ArrayROI)))
                # not empty, so probably the target
                roi, dist = find_closest(det_res.ArrayROI, current_img.shape[0], current_img.shape[1], self.target)
                rospy.loginfo("Dist from self.target: {}".format(dist))
                if dist > 0.1 * max(current_img.shape[0], current_img.shape[1]):
                    self.target_found = False
                    self.redetect_attempts += 1
                elif max(roi.w, roi.h) > 1.5 * max(self.target.w, self.target.h):
                    self.target_found = False
                    self.redetect_attempts += 1
                else:
                    self.target_found = True
                    self.redetect_attempts = 0
            else:
                rospy.loginfo("No targets found!")
                self.target_found = False
                self.redetect_attempts += 1
        return


def check_limits(roi, h, w):
    roi.x = max(0, min(roi.x, w))
    roi.y = max(0, min(roi.y, h))
    roi.w = min(roi.w, w - roi.x)
    roi.h = min(roi.h, h - roi.y)
    return roi


def find_closest(array_roi, h, w, gps=None):
    if gps:
        point = gps.x, gps.y
    else:
        point = w / 2., h / 2.
    nodes = np.asarray([(roi.x + roi.w / 2., roi.y + roi.h / 2.) for roi in array_roi])
    node = np.asarray(point)
    dist_2 = np.sum(np.abs(nodes - node), axis=1)
    ind = np.argmin(dist_2)
    return array_roi[ind], dist_2[ind]


if __name__ == '__main__':
    rospy.init_node('MasterVisualAnalysis', anonymous=True)
    rospack = rospkg.RosPack()
    drone_id = rospy.get_param("~drone_id", 1)

    base_path = rospack.get_path("drone_visual_analysis")

    mva = MVA(drone_id)

    rospy.loginfo('Reading images from {}'.format(mva.image_topic))
    rospy.loginfo('Reading ROI from {}'.format(mva.roi_topic))
    if mva.init_w_img == 1:
        rospy.loginfo("Initializing tracker(s) with detection frame")
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
