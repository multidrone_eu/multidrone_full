#!/usr/bin/env python
# based on https://github.com/foolwood/DaSiamRPN
import rospy
import rospkg
import torch

torch.no_grad()

from run_SiamRPN import *
from tracker_utils import rect_2_cxy_wh

from sensor_msgs.msg import Image
from std_msgs.msg import Header
from cv_bridge import CvBridge, CvBridgeError
from visualanalysis_msgs.msg import TargetPositionROI2D, TargetPositionROI2DArray, VisualControlError
from visualanalysis_msgs.srv import *
from multidrone_msgs.srv import SetFramingType
from drone_visual_analysis.srv import *
from multidrone_msgs.msg import CameraStatus, GimbalStatus
from geometry_msgs.msg import PoseStamped


class Tracker():
    def __init__(self, model_name='SiamRPNBIG.model'):
        rospack = rospkg.RosPack()
        base_path = rospack.get_path('drone_visual_analysis')
        self.model = get_model(base_path, model_name)

        self.state = {}
        self.cfg = TrackerConfig()
        self.cfg.update(self.model.cfg)
        self.bridge = CvBridge()
        self.img = None
        self.base_path = base_path
        self.initialized = False
        self.init_w_img = rospy.get_param("init_tracker_mode", 1)
        self.tracker_states = {}

        self.gimbal_status = GimbalStatus()
        self.camera_status = CameraStatus()
        self.drone_pose = PoseStamped()

    def setup_comm(self, drone_id=1, width=640, height=480):
        self.image_sub = rospy.Subscriber("/drone_{}/shooting_camera".format(drone_id), Image, self.image_callback,
                                          queue_size=1,
                                          buff_size=2 ** 24)

        self.gimbal_sub = rospy.Subscriber("/drone_{}/gimbal/status".format(drone_id), Image, self.gimbal_callback,
                                           queue_size=1)
        self.camera_sub = rospy.Subscriber("/drone_{}/camera/status".format(drone_id), Image, self.camera_callback,
                                           queue_size=1)
        self.ualpose_sub = rospy.Subscriber("/drone_{}/ual/pose".format(drone_id), Image, self.ualpose_callback,
                                            queue_size=1)

        self.vce_pub = rospy.Publisher('/drone_{}/visual_analysis/visual_control_error'.format(drone_id),
                                       VisualControlError, queue_size=10)
        self.frame_srv = rospy.Service('/drone_{}/set_framing_type'.format(drone_id),
                                       SetFramingType, self.set_framing_type)
        self.width = width
        self.height = height
        self.targetROICenterX = self.width / 2
        self.targetROICenterY = self.height / 2
        self.targetFrameCoverage = 0.2

        self.tracking_srv = rospy.Service("/drone_{}/tracking".format(drone_id), Track, self.control_tracker)
        self.reidentification_srv = rospy.Service("/drone_{}/reidentification".format(drone_id), Reidentify,
                                                  self.reidentify_box)
        self.tracking_pub = rospy.Publisher("/drone_{}/visual_analysis/target_position_roi_2d_array/".format(drone_id),
                                            TargetPositionROI2DArray, queue_size=1)
        self.cur_header = None

    def handle_set_framing_type(self, req):
        self.targetROICenterX = req.target_roi_center_x
        self.targetROICenterY = req.target_roi_center_y
        rospy.loginfo("Target x, y: %.2f %.2f", self.targetROICenterX, self.targetROICenterY)
        framingTypeMsg = req.target_framing_type
        framingTypeVal = framingTypeMsg.type
        # ELS framing shot type
        if framingTypeVal == 0:
            self.targetFrameCoverage = 0.03
        # VLS framing shot type
        elif framingTypeVal == 1:
            self.targetFrameCoverage = 0.13
        # LS framing shot type
        elif framingTypeVal == 2:
            self.targetFrameCoverage = 0.30
        # MS framing shot type
        elif framingTypeVal == 3:
            self.targetFrameCoverage = 0.50
        # MCU framing shot type
        elif framingTypeVal == 4:
            self.targetFrameCoverage = 0.67
        # CU framing shot type
        elif framingTypeVal == 5:
            self.targetFrameCoverage = 0.80
        else:
            self.targetFrameCoverage = 0.2
        return True

    def image_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            self.img = cv_image
            self.cur_header = data.header
            # rospy.loginfo("img")
        except CvBridgeError as e:
            print e
            self.img = None
            return
        self.update(data.header.seq)
        return

    def update(self, seq):
        if self.initialized:
            # self.state = SiamRPN_track(self.state, self.img)      # single target
            targets_array = TargetPositionROI2DArray()
            # rospy.loginfo('Tracking %d targets', len(self.tracker_states))
            target_ids = self.tracker_states.keys()
            for i in range(len(self.tracker_states)):
                t_id = target_ids[i]
                self.tracker_states[t_id] = SiamRPN_track(self.tracker_states[t_id], self.img)
                target = TargetPositionROI2D()
                target.x, target.y = int(
                    self.tracker_states[t_id]['target_pos'][0] - self.tracker_states[t_id]['target_sz'][0] * 0.5), \
                                     int(self.tracker_states[t_id]['target_pos'][1] -
                                         self.tracker_states[t_id]['target_sz'][1] * 0.5)
                target.w, target.h = int(self.tracker_states[t_id]['target_sz'][0]), int(
                    self.tracker_states[t_id]['target_sz'][1])
                target.trk_score = self.tracker_states[t_id]['score']
                target.id = t_id
                targets_array.targets.append(target)
            # self.logfile.write("{},{},{},{},{}\n".format(seq, target.x, target.y, target.w, target.h))
            targets_array.header = self.cur_header
            targets_array.seq = seq
            targets_array.gimbal = self.gimbal_status
            targets_array.camera = self.camera_status
            targets_array.drone_pose = self.drone_pose
            self.tracking_pub.publish(targets_array)
            self.publish_error(targets_array)

    def publish_error(self, targets_array):
        BB = targets_array.targets[0]
        header = Header()
        if out_of_limits(BB, self.height, self.width):
            rospy.logwarn("ROI is out of frame, publishing zeros")
            visualCE = VisualControlError(header, 0., 0., 0.)
        else:
            xError = (BB.x + BB.w / 2) - self.targetROICenterX
            yError = (BB.y + BB.h / 2) - self.targetROICenterY
            widthFrameCoverage = float(BB.w) / self.width
            heightFrameCoverage = float(BB.h) / self.height
            BBFrameCoverage = max(widthFrameCoverage, heightFrameCoverage)
            zoomError = BBFrameCoverage - self.targetFrameCoverage
            visualCE = VisualControlError(header, float(xError), float(yError), float(zoomError))
        self.vce_pub.publish(visualCE)

    def control_tracker(self, req):
        # img = cv2.imread(req.img_path)
        resp = TrackResponse()

        if req.cmd == 0:
            # init tracker
            rospy.loginfo("Initializing tracker(s)")
            start_time = rospy.get_time()
            for target in req.targets:
                init_rect = np.asarray([target.x, target.y, target.w, target.h])
                target_pos, target_sz = rect_2_cxy_wh(init_rect)
                if self.init_w_img:
                    img = self.bridge.imgmsg_to_cv2(req.img, "bgr8")
                else:
                    img = self.img
                self.tracker_states[target.id] = SiamRPN_init(img, target_pos, target_sz, self.model, self.cfg)
                self.initialized = True
            end_time = rospy.get_time()
            rospy.loginfo("Initialization took: %.2fs", end_time - start_time)
            resp.result = 1

        elif req.cmd == 1:
            # reinitialize tracker
            rospy.loginfo("Re-Initializing tracker")
            for target in req.targets:
                rect = np.asarray([target.x, target.y, target.w, target.h])
                target_pos, target_sz = rect_2_cxy_wh(rect)
                # TODO: reinit tracker or not??
                if self.init_w_img:
                    img = self.bridge.imgmsg_to_cv2(req.img, "bgr8")
                else:
                    img = self.img

                self.tracker_states[target.id] = SiamRPN_reinit(img, target_pos, target_sz, self.model,
                                                                self.tracker_states[target.id])
            resp.result = 1

        else:
            resp.result = 0
        return resp

    def reidentify_box(self, req):
        img = self.bridge.imgmsg_to_cv2(req.img, "rgb8")
        resp = ReidentifyResponse()
        target = req.target
        box = np.asarray([target.x, target.y, target.w, target.h])
        score = SiamRPN_eval(self.tracker_states[target.id], box, img)
        resp.trk_score = score
        return resp

    def gimbal_callback(self, msg):
        self.gimbal_status = msg

    def camera_callback(self, msg):
        self.camera_status = msg

    def ualpose_callback(self, msg):
        self.gimbal_status = msg


def out_of_limits(roi, h, w):
    if roi.x + roi.w < 0 or roi.x > w:
        return True
    if roi.y + roi.h < 0 or roi.h > h:
        return True
    return False


def main(args):
    model_name = rospy.get_param("~model_name", "SiamRPNOTB.model")
    tr = Tracker(model_name)
    drone_id = rospy.get_param("~drone_id", 1)
    width = rospy.get_param("~width", 640)
    height = rospy.get_param("~height", 480)
    tr.setup_comm(drone_id, width, height)
    rospy.init_node('SiamRPN', anonymous=True, log_level=rospy.DEBUG)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    main(sys.argv)
