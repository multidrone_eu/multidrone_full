#!/usr/bin/env python
# based on https://github.com/got-10k/siamfc
import rospy
import rospkg
import os

from siamfc import *
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from visualanalysis_msgs.msg import TargetPositionROI2D, TargetPositionROI2DArray
from visualanalysis_msgs.srv import *
from drone_visual_analysis.srv import *
from multidrone_msgs.msg import CameraStatus, GimbalStatus
from geometry_msgs.msg import PoseStamped


class Tracker():
    def __init__(self, model_name='model_e50.pth'):
        rospack = rospkg.RosPack()
        base_path = rospack.get_path('drone_visual_analysis')

        # setup GPU device if available
        self.cuda = torch.cuda.is_available()
        self.device = torch.device('cuda:0' if self.cuda else 'cpu')

        # setup model
        self.net = SiamFC(size=0.25)
        net_path = os.path.join(base_path, 'models', 'trackers', 'siamfc', model_name)
        if net_path is not None:
            self.net.load_state_dict(torch.load(
                net_path, map_location=lambda storage, loc: storage))
        self.net = self.net.to(self.device)

        self.trackers = {}
        self.bridge = CvBridge()
        self.img = None
        self.base_path = base_path
        self.initialized = False
        self.init_w_img = rospy.get_param("~init_tracker_mode", 1)

        self.gimbal_status = GimbalStatus()
        self.camera_status = CameraStatus()
        self.drone_pose = PoseStamped()

    def setup_comm(self, drone_id=1):
        self.image_sub = rospy.Subscriber("/drone_{}/shooting_camera".format(drone_id), Image, self.image_callback,
                                          queue_size=1,
                                          buff_size=2 ** 24)

        self.gimbal_sub = rospy.Subscriber("/drone_{}/gimbal/status".format(drone_id), Image, self.gimbal_callback,
                                           queue_size=1)
        self.camera_sub = rospy.Subscriber("/drone_{}/camera/status".format(drone_id), Image, self.camera_callback,
                                           queue_size=1)
        self.ualpose_sub = rospy.Subscriber("/drone_{}/ual/pose".format(drone_id), Image, self.ualpose_callback,
                                            queue_size=1)

        self.tracking_srv = rospy.Service("/drone_{}/tracking".format(drone_id), Track, self.control_tracker)
        self.reidentification_srv = rospy.Service("/drone_{}/reidentification".format(drone_id), Reidentify,
                                                  self.reidentify_box)
        self.tracking_pub = rospy.Publisher("/drone_{}/visual_analysis/target_position_roi_2d_array/".format(drone_id),
                                            TargetPositionROI2DArray, queue_size=1)

    def image_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            self.img = cv_image
        except CvBridgeError as e:
            print e
            self.img = None
            return
        self.update(data.header.seq)
        return

    def update(self, seq):
        if self.initialized:
            targets_array = TargetPositionROI2DArray()
            target_ids = self.trackers.keys()
            for i in range(len(self.trackers)):
                box, score = self.trackers[target_ids[i]].update(self.img)
                target = TargetPositionROI2D()
                target.x, target.y, target.w, target.h = box
                target.trk_score = score
                target.id = target_ids[i]
                targets_array.targets.append(target)
                targets_array.seq = seq

            targets_array.gimbal = self.gimbal_status
            targets_array.camera = self.camera_status
            targets_array.drone_pose = self.drone_pose
            self.tracking_pub.publish(targets_array)

    def control_tracker(self, req):
        resp = TrackResponse()
        target = req.targets[0]

        if req.cmd == 0:
            # init tracker
            start_time = rospy.get_time()

            if self.init_w_img:
                img = self.bridge.imgmsg_to_cv2(req.img, "bgr8")
            else:
                img = self.img
            rospy.loginfo('Initializing %d trackers...', len(req.targets))
            for i in range(len(req.targets)):
                target = req.targets[i]
                init_rect = np.asarray([target.x, target.y, target.w, target.h])
                self.trackers[target.id] = TrackerSiamFC(self.net, self.device)
                self.trackers[target.id].init(img, init_rect)
            self.initialized = True
            end_time = rospy.get_time()
            resp.result = 1

        elif req.cmd == 1:
            # reinitialize tracker
            if self.init_w_img:
                img = self.bridge.imgmsg_to_cv2(req.img, "bgr8")
            else:
                img = self.img

            for i in range(len(req.targets)):
                target = req.targets[i]
                init_rect = np.asarray([target.x, target.y, target.w, target.h])
                self.trackers[target.id].reinit(img, init_rect)
            resp.result = 1

        else:
            resp.result = 0
        return resp

    def reidentify_box(self, req):
        img = self.bridge.imgmsg_to_cv2(req.img, "rgb8")
        resp = ReidentifyResponse()
        target = req.target
        box = np.asarray([target.x, target.y, target.w, target.h])
        score = self.trackers[target.id].eval(img, box)
        resp.trk_score = score
        return resp

    def gimbal_callback(self, msg):
        self.gimbal_status = msg

    def camera_callback(self, msg):
        self.camera_status = msg

    def ualpose_callback(self, msg):
        self.gimbal_status = msg


def main(args):
    rospy.init_node('SiamFC', anonymous=True, log_level=rospy.DEBUG)
    tr = Tracker()
    drone_id = rospy.get_param("~drone_id", 1)
    rospy.loginfo("DRONE ID: %d", drone_id)
    tr.setup_comm(drone_id)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    main(sys.argv)
