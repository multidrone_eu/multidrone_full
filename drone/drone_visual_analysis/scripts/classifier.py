#!/usr/bin/env python

import rospkg
import rospy
import os

from visualanalysis_msgs.srv import Verify, VerifyResponse
from drone_visual_analysis.srv import *
from visualanalysis_msgs.msg import TargetPositionROI2DArray

import numpy as np
from scipy.spatial.distance import euclidean
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib


class Classifier():
    def __init__(self, type=None, filename=None, drone_id=1):
        if filename is not None and not os.path.exists(filename):
            rospy.logerr('Model {} not found!'.format(filename))
            exit(0)
        elif type == 'SVC':
            self.model = SVC()
            self.is_trained = False
        elif type == 'MLP':
            self.model = MLPClassifier()
            self.is_trained = False
        elif filename is not None:
            self.load(filename)
        else:
            assert False, 'Not implemented'
        self.verification_srv = rospy.Service("verification".format(drone_id), Verify, self.verify_box)
        self.get_last_box = rospy.Service("verifier_update".format(drone_id), Verify_classifier_update,
                                          self.update_box)

        self.verification_pub = rospy.Publisher("visual_analysis/verification_target_position_roi_2d_array/",
                                                TargetPositionROI2DArray, queue_size=1)
        self.last_box = {}
        self.ver_thr = rospy.get_param("ver_score_th", 0.2)
        self.n_failures = 1.

    def load(self, filename):
        self.model = joblib.load(filename)
        self.is_trained = True

    def train(self, X, y):
        self.model.fit(X, y)
        self.is_trained = True

    def predict(self, X, prob=True):
        if self.is_trained:
            if X.ndim < 2:
                X = X.reshape(1, -1)
            if prob:
                out = self.model.predict_proba(X)[:, 1]
            else:
                out = self.model.predict(X)
            return out
        else:
            return -1

    def save(self, filename):
        joblib.dump(self.model, filename)

    def update_box(self, req):
        resp = Verify_classifier_updateResponse()
        self.last_box[req.target.id] = np.asarray([req.target.x, req.target.y, req.target.w, req.target.h])
        return resp

    def verify_box(self, msg):
        resp = VerifyResponse()
        box = np.asarray([msg.ROI.x, msg.ROI.y, msg.ROI.w, msg.ROI.h])
        dist = euclidean((box[0] + box[2] / 2, box[1] + box[3] / 2),
                         (self.last_box[msg.ROI.id][0] + self.last_box[msg.ROI.id][2] / 2,
                          self.last_box[msg.ROI.id][1] + self.last_box[msg.ROI.id][3] / 2))
        dist /= (self.last_box[msg.ROI.id][2] + self.last_box[msg.ROI.id][3]) * 0.5
        dist /= self.n_failures
        x = np.asarray([dist, msg.ROI.trk_score, msg.ROI.det_score])
        resp.ver_score = self.predict(x, True)
        if resp.ver_score > self.ver_thr:
            self.n_failures = 1.
        else:
            self.n_failures += 1.
        return resp


if __name__ == '__main__':
    rospy.init_node('target_verifier', anonymous=True, log_level=rospy.DEBUG)
    rospack = rospkg.RosPack()
    base_path = rospack.get_path("drone_visual_analysis")

    drone_id = rospy.get_param("~drone_id", 1)
    tracker_name = rospy.get_param("~tracker_name")

    clf = Classifier(filename=os.path.join(base_path, 'models', 'verifiers', '{}_mlp.pickle'.format(tracker_name)),
                     drone_id=drone_id)

    rospy.loginfo("Reading from: %s",
                  os.path.join(base_path, 'models', 'verifiers', '{}_mlp.pickle'.format(tracker_name)))

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
