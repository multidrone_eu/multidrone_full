# MULTIDRONE FULL - Drone #

This folder contains all the code of the drones. It may not contain subfolders but ROS packages.

## List of packages ##

* action_executer (IST)
* camera_control (IST)
* darknet_ros (AUTH) -- To be removed?
* drone_visual_analysis (AUTH)
* gimbal_interface (IST)
* gimbal_interface_small (IST) -- What is the difference?
* gscam (AUTH) -- To be removed?
* onboard_scheduler (USE)
* onboard_3d_tracker (USE)
* video_streamer (AUTH)
* visualiser (AUTH) -- Can be included in drone_visual_analysis?

### Dependencies ###

* libgstreamer: `sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev`