#include <ros/ros.h>
#include <ros/package.h>
#include <multidrone_msgs/SetFramingType.h>
#include <multidrone_msgs/FramingType.h>

#include <fstream>

int main(int argc, char **argv) {
    int count = 0;
    ros::init(argc, argv, "set_framing_type_requester");

    ros::NodeHandle nh;

    std::ofstream logfile;
    std::string logfilename = nh.param<std::string>("logfilename", "log/request.log");
    std::string path_to_node = ros::package::getPath("dummy_service_requester") + std::string("/");
    ROS_INFO("Logging to %s", (path_to_node + logfilename).c_str());
    logfile.open(path_to_node + logfilename);

    static int target_id = nh.param<int>("target_id", 0);
    int target_framing_type = nh.param<int>("framing_type", 3);

    ros::ServiceClient follow_target_client = nh.serviceClient<multidrone_msgs::SetFramingType>("/set_framing_type");
    multidrone_msgs::SetFramingType req;
    req.request.target_id = (uint8_t)target_id;
    req.request.target_framing_type = multidrone_msgs::FramingType();
    req.request.target_framing_type.type = (uint8_t)target_framing_type;

    ros::Rate loop_rate(20);
    while (ros::ok()) {
        if (set_framing_type_client.call(req)) {
            ROS_INFO("Received positive request response, terminating.");
            return 1;
        }
        ROS_INFO("Received negative request response, retrying...");
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }

    logfile.close();
    return 0;
}
