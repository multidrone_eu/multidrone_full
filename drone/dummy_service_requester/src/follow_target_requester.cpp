#include <ros/ros.h>
#include <ros/package.h>
#include <multidrone_msgs/FollowTarget.h>
#include <multidrone_msgs/TargetType.h>

#include <fstream>

int main(int argc, char **argv) {
    int count = 0;
    ros::init(argc, argv, "follow_target_requester");

    ros::NodeHandle nh;

    std::ofstream logfile;
    std::string logfilename = nh.param<std::string>("logfilename", "log/request.log");
    std::string path_to_node = ros::package::getPath("dummy_service_requester") + std::string("/");
    ROS_INFO("Logging to %s", (path_to_node + logfilename).c_str());
    logfile.open(path_to_node + logfilename);

    static int target_id = nh.param<int>("target_id", 0);
    int target_type = nh.param<int>("target_type", 1);

    ros::ServiceClient follow_target_client = nh.serviceClient<multidrone_msgs::FollowTarget>("/drone_1/follow_target");
    multidrone_msgs::FollowTarget req;
    req.request.target_id = (uint8_t)target_id;
    req.request.target_type = multidrone_msgs::TargetType();
    req.request.target_type.type = (uint8_t)target_type;

    ros::Rate loop_rate(0.5);
    while (ros::ok()) {
        if (follow_target_client.call(req)) {
            ROS_INFO("Received positive request response, terminating.");
            return 1;
        }
        ROS_INFO("Received negative request response, retrying...");
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }

    logfile.close();
    return 0;
}
