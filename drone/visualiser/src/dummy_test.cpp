#include "ros/ros.h"
#include <cstdlib>
#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <multidrone_msgs/FollowTarget.h>
#include "visualanalysis_msgs/Verify.h"
#include "visualanalysis_msgs/Detect.h"
#include <multidrone_msgs/SetFramingType.h>
#include "multidrone_msgs/FollowTarget.h"
#include "multidrone_msgs/TargetType.h"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <string>
#include <fstream>
#include <math.h>
using namespace cv;
using namespace std;

ros::ServiceClient test_client;
ros::ServiceClient test_frame_type;

multidrone_msgs::FollowTarget follow_tar;
multidrone_msgs::TargetType target_type;


multidrone_msgs::FramingType framing_type;


multidrone_msgs::SetFramingType frame_type;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "dummy_test");
	ros::NodeHandle nh;

#ifdef vis_mode
	//image_transport::ImageTransport it(nh);
	//image_transport::Subscriber sub3 = it.subscribe("/videofile/image_raw", 1,imageCallback);
#endif

	// Initialize service

	


	test_client = nh.serviceClient<multidrone_msgs::FollowTarget> ("follow_target");
	//test_frame_type = nh.serviceClient<multidrone_msgs::SetFramingType> ("set_framing_type");

	//framing_type.type =3;
	
	//frame_type.request.target_framing_type = framing_type;
	//frame_type.request.target_roi_center_x = 960/2;
	//frame_type.request.target_roi_center_y = 540/2;
	//test_frame_type.call(frame_type);

	
	
	target_type.type = 1;
	follow_tar.request.target_type = target_type;
	follow_tar.request.target_id = 1;
	test_client.call(follow_tar);

	ros::spin();
	return 0;
}
