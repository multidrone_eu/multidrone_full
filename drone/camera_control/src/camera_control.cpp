/*
 * SBUS interface to Blackmagic Micro Cinema Camera.
 *
 * Copyright 2018 Instituto Superior Tecnico
 *
 * Written by Bruno Gomes <bgomes@isr.tecnico.ulisboa.pt>
 */

#include <ros/ros.h>
#include <stdio.h>
#include <fcntl.h>
#include <poll.h>

#include <libusbp-1/libusbp.hpp>

#include <fstream>
#include <unistd.h>
#include <iostream>

#include <asm/ioctls.h>
#include <asm/termbits.h>
#include <sys/ioctl.h>
#include <multidrone_msgs/CameraStatus.h>
#include <multidrone_msgs/CameraControl.h>


#define SBUS_WR_FREQ	100 //Hz
#define PUBLISH_STATUS_FREQ	30 //Hz

#define	PUSH_BUTTON_TIME	0.2 //seconds
#define	PUSH_HOLD_BUTTON_TIME 1 //seconds

#define	SBUS_FRAME_HEADER	0x0f
#define	SBUS_FRAME_FOOTER	0x00
#define	SBUS2_FRAME_FOOTER	0x04
#define	SBUS_PACKET_LENGTH	25
#define	SBUS_NUMBER_CHANNELS	18

/* Default SBUS values for channels without trim */
#define	SBUS_MIN_OFFSET	173
#define	SBUS_MID_OFFSET	992
#define	SBUS_MAX_OFFSET	1811

#define	DEFAULT_SBUS_RECORD_CH	13
#define	DEFAULT_SBUS_IRIS_CH	7
#define	DEFAULT_SBUS_FOCUS_CH	1
#define	DEFAULT_SBUS_AFOCUS_CH	12
#define	DEFAULT_SBUS_ZOOM_CH	3
#define	DEFAULT_SBUS_ISO_CH	9
#define	DEFAULT_SBUS_SHUTTER_CH	10
#define	DEFAULT_SBUS_WHITE_CH	11
#define	DEFAULT_SBUS_AUDIO_CH	15
#define	DEFAULT_SBUS_FRAME_CH	5
#define	DEFAULT_SBUS_CODEC_CH	6

#define	BMMCC_MIN_OFFSET	44
#define	BMMCC_MID_OFFSET	128
#define	BMMCC_MAX_OFFSET	212

#define	DEFAULT_BMMCC_RECORD	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_IRIS	BMMCC_MIN_OFFSET
#define	DEFAULT_BMMCC_FOCUS	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_AFOCUS	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_ZOOM	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_ISO	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_SHUTTER	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_WHITE	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_AUDIO	BMMCC_MID_OFFSET
#define	DEFAULT_BMMCC_FRAME	135 //30FPS
#define	DEFAULT_BMMCC_CODEC	BMMCC_MIN_OFFSET //RAW

#define	VENDOR_ID 1027  //vendor id
#define	PRODUCT_ID 24577 //product id

enum{RECORD_CH, IRIS_CH, FOCUS_CH, AFOCUS_CH, ZOOM_CH, ISO_CH, SHUTTER_CH, WHITE_CH, AUDIO_CH, FRAME_CH, CODEC_CH, NFUNCTIONS};

static ros::Publisher cstatus_publisher;

static struct pollfd pfd[1];

static multidrone_msgs::CameraStatus cs;
static unsigned int SBUSreadData[SBUS_NUMBER_CHANNELS];

static int SBUSch[NFUNCTIONS];
static int BMMCCdata[NFUNCTIONS];

struct BMMCCcmdtiming
{
	unsigned char function;
	unsigned char value;
	ros::Time timeout;
};

static std::vector<BMMCCcmdtiming> BMMCCcmd;


/*
 * Open serial port device TTY @ 100000 since its the SBUS std baud and 8E2.
 * Return the serial port file descriptor, or -1 on failure.
 */
static int serial_open(const char *tty)
{
	int fd;
	struct termios2 t;

	fd = open(tty, O_RDWR);// | O_NOCTTY);
	if (fd < 0)
		return -1;

	memset(&t, 0, sizeof t);

	t.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL
			 | IXON);
	/* Check for parity errors and ignore frame or parity errors */
	t.c_iflag |= (INPCK | IGNPAR);
	t.c_oflag &= ~OPOST;
	t.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

	/* Serial port 8E2, no control lines, 100Kbps, BOTHER to specify speed directly in c_[io]speed member*/
	t.c_cflag &= ~(CSIZE | CRTSCTS | PARODD | CBAUD);
	t.c_cflag |= (CS8 | CSTOPB | CLOCAL | PARENB | BOTHER | CREAD);
	t.c_ispeed = 100000;
	t.c_ospeed = 100000;
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;

	if (ioctl(fd, TCSETS2, &t)){
		close(fd);
		return -1;
	}

	return fd;
}


/*
 * Read SBUS Frames.
 */
static bool readsbus(int fd)
{
	bool rxSBUSframe = false;
	int n;
	static unsigned char buf[128];
	static unsigned head;
	unsigned tail = 0;

	n = read(fd, buf + head, sizeof buf - head);

	if(n <= 0)
		return false;

	head += n;

	for(tail=0; head - tail >= SBUS_PACKET_LENGTH; tail ++){

		if(buf[tail] == SBUS_FRAME_HEADER && (buf[tail+SBUS_PACKET_LENGTH-1] == SBUS_FRAME_FOOTER || (buf[tail+SBUS_PACKET_LENGTH-1] & 0x0F) == SBUS2_FRAME_FOOTER)){

			SBUSreadData[0] = ((buf[tail + 1]		| buf[tail + 2] << 8)	& 0x07FF);
			SBUSreadData[1] = ((buf[tail + 2] >> 3	| buf[tail + 3] << 5)	& 0x07FF);
			SBUSreadData[2] = ((buf[tail + 3] >> 6	| buf[tail + 4] << 2	| buf[tail + 5] << 10) & 0x07FF);
			SBUSreadData[3] = ((buf[tail + 5] >> 1	| buf[tail + 6] << 7)	& 0x07FF);
			SBUSreadData[4] = ((buf[tail + 6] >> 4	| buf[tail + 7] << 4)	& 0x07FF);
			SBUSreadData[5] = ((buf[tail + 7] >> 7	| buf[tail + 8] << 1	| buf[tail + 9] << 9) & 0x07FF);
			SBUSreadData[6] = ((buf[tail + 9] >> 2	| buf[tail + 10] << 6)	& 0x07FF);
			SBUSreadData[7] = ((buf[tail + 10] >> 5	| buf[tail + 11] << 3)	& 0x07FF);
			SBUSreadData[8] = ((buf[tail + 12]	| buf[tail + 13] << 8)	& 0x07FF);
			SBUSreadData[9] = ((buf[tail + 13] >> 3	| buf[tail + 14] << 5)	& 0x07FF);
			SBUSreadData[10] = ((buf[tail + 14] >> 6	| buf[tail + 15] << 2	| buf[tail + 16] << 10) & 0x07FF);
			SBUSreadData[11] = ((buf[tail + 16] >> 1	| buf[tail + 17] << 7)	& 0x07FF);
			SBUSreadData[12] = ((buf[tail + 17] >> 4	| buf[tail + 18] << 4)	& 0x07FF);
			SBUSreadData[13] = ((buf[tail + 18] >> 7	| buf[tail + 19] << 1	| buf[tail + 20] << 9) & 0x07FF);
			SBUSreadData[14] = ((buf[tail + 20] >> 2	| buf[tail + 21] << 6)	& 0x07FF);
			SBUSreadData[15] = ((buf[tail + 21] >> 5	| buf[tail + 22] << 3)	& 0x07FF);

			/* Byte24 Flags (DCH1,DCH2,FrameLost,failsate,NA,NA,NA,NA) */
			SBUSreadData[16] = (buf[tail + 23] & 0x01);
			SBUSreadData[17] = (buf[tail + 23] & 0x02) >> 1;

			if (buf[tail + 23] & 0x08)
				cs.rcstatus_failsafe = true;
			else
				cs.rcstatus_failsafe = false;

			tail += SBUS_PACKET_LENGTH - 1;

			rxSBUSframe = true;
#if 0
			ROS_INFO("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d",
				SBUSreadData[0]>>3,
				SBUSreadData[1]>>3,
				SBUSreadData[2]>>3,
				SBUSreadData[3]>>3,
				SBUSreadData[4]>>3,
				SBUSreadData[5]>>3,
				SBUSreadData[6]>>3,
				SBUSreadData[7]>>3,
				SBUSreadData[8]>>3,
				SBUSreadData[9]>>3,
				SBUSreadData[10]>>3,
				SBUSreadData[11]>>3,
				SBUSreadData[12]>>3,
				SBUSreadData[13]>>3,
				SBUSreadData[14]>>3,
				SBUSreadData[15]>>3,
				SBUSreadData[16],
				SBUSreadData[17]);
#endif
		}
	}

	if (tail){
		head -=tail;
		if (head)
			memmove(buf, buf+tail, head);
	}

	return rxSBUSframe;
}

/*
 * Write SBUS Frame.
 */
static bool writesbus(int fd, unsigned int* output)
{
	int n;
	unsigned char packet[SBUS_PACKET_LENGTH];

	packet[0] = SBUS_FRAME_HEADER;
	packet[1] = (unsigned char) (output[0] & 0x07FF);
	packet[2] = (unsigned char) ((output[0] & 0x07FF)>>8 | (output[1] & 0x07FF)<<3);
	packet[3] = (unsigned char) ((output[1] & 0x07FF)>>5 | (output[2] & 0x07FF)<<6);
	packet[4] = (unsigned char) ((output[2] & 0x07FF)>>2);
	packet[5] = (unsigned char) ((output[2] & 0x07FF)>>10 | (output[3] & 0x07FF)<<1);
	packet[6] = (unsigned char) ((output[3] & 0x07FF)>>7 | (output[4] & 0x07FF)<<4);
	packet[7] = (unsigned char) ((output[4] & 0x07FF)>>4 | (output[5] & 0x07FF)<<7);
	packet[8] = (unsigned char) ((output[5] & 0x07FF)>>1);
	packet[9] = (unsigned char) ((output[5] & 0x07FF)>>9 | (output[6] & 0x07FF)<<2);
	packet[10] = (unsigned char) ((output[6] & 0x07FF)>>6 | (output[7] & 0x07FF)<<5);
	packet[11] = (unsigned char) ((output[7] & 0x07FF)>>3);
	packet[12] = (unsigned char) ((output[8] & 0x07FF));
	packet[13] = (unsigned char) ((output[8] & 0x07FF)>>8 | (output[9] & 0x07FF)<<3);
	packet[14] = (unsigned char) ((output[9] & 0x07FF)>>5 | (output[10] & 0x07FF)<<6);
	packet[15] = (unsigned char) ((output[10] & 0x07FF)>>2);
	packet[16] = (unsigned char) ((output[10] & 0x07FF)>>10 | (output[11] & 0x07FF)<<1);
	packet[17] = (unsigned char) ((output[11] & 0x07FF)>>7 | (output[12] & 0x07FF)<<4);
	packet[18] = (unsigned char) ((output[12] & 0x07FF)>>4 | (output[13] & 0x07FF)<<7);
	packet[19] = (unsigned char) ((output[13] & 0x07FF)>>1);
	packet[20] = (unsigned char) ((output[13] & 0x07FF)>>9 | (output[14] & 0x07FF)<<2);
	packet[21] = (unsigned char) ((output[14] & 0x07FF)>>6 | (output[15] & 0x07FF)<<5);
	packet[22] = (unsigned char) ((output[15] & 0x07FF)>>3);

	/* Byte24 Flags (DCH1,DCH2,FrameLost,failsate,NA,NA,NA,NA) */
	packet[23] = (unsigned char) ((output[16] & 0x0001) | (output[17] & 0x0001)<<1);
	packet[24] = SBUS_FRAME_FOOTER;

	n = write(fd, packet, SBUS_PACKET_LENGTH);

	if (n <= 0)
		return false;
	else
		if(n != SBUS_PACKET_LENGTH){
			//ROS_ERROR("camera_control: ONLY wrote %u bytes out of %d", n, SBUS_PACKET_LENGTH);
			return false;
		}
	return true;
}

/*
 *Add camera commands to array
 */
void pushBMMCCcmd(unsigned char BMMCCfunction, unsigned char value, ros::Time timeout)
{
	struct BMMCCcmdtiming cmd;

	cmd.function = BMMCCfunction;
	cmd.value = value;
	cmd.timeout = timeout;
	BMMCCcmd.push_back(cmd);
}


/*
 *Handle camera commands array
 */
void checkBMMCCcmd(void)
{
	if(BMMCCcmd.empty())
		return;

	for(auto cmd = BMMCCcmd.begin(); cmd != BMMCCcmd.end();){
		if(cmd->timeout <= ros::Time::now()){
			BMMCCdata[cmd->function] = BMMCC_MID_OFFSET;
			cmd = BMMCCcmd.erase(cmd);
		}
		else{
			BMMCCdata[cmd->function] = cmd->value;
			++cmd ;
		}
	}
}

/*
 * Receive control commands
 */
bool cameracontrolServiceCallback (multidrone_msgs::CameraControl::Request &req, multidrone_msgs::CameraControl::Response &res)
{

	if(req.value < BMMCC_MIN_OFFSET)
		 req.value = BMMCC_MIN_OFFSET;
	else
		if(req.value > BMMCC_MAX_OFFSET)
			 req.value = BMMCC_MAX_OFFSET;

	ROS_INFO("Camera_Control: Control received %d %d %d", req.control_function, req.value, req.manual_ctrl);

	switch(req.control_function){

		/*push-button like camera functions*/
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_RECORD): // Status: Values range in [On OFF]
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_AFOCUS): // Status: No status
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_ISO): //Status: Values range in [200 400 800 1600]
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_SHUTTER): //
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_WHITE): // Status: Values range [2500K : 8000K]

			pushBMMCCcmd(req.control_function, req.value, ros::Time::now()+ros::Duration(PUSH_BUTTON_TIME));
			break;

		/*push and hold like camera functions*/
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_ZOOM):

			pushBMMCCcmd(req.control_function, req.value, ros::Time::now()+ros::Duration(PUSH_HOLD_BUTTON_TIME));
			break;

		/*knob like camera functions*/
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_IRIS):
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_FOCUS):
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_AUDIO): //TODO
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_FRAME):
		case(multidrone_msgs::CameraControl::Request::CONTROL_FUNCTION_CODEC):

			BMMCCdata[req.control_function] = req.value;
			break;

		default:
			ROS_WARN("camera_control: Unmatched camera funtion to control");
	}

	/* Check if camera is to be manually controlled by the cameraman's radio */
	if (req.manual_ctrl != cs.manual_ctrl){
		cs.manual_ctrl = req.manual_ctrl;

		ROS_INFO(cs.manual_ctrl ? "camera_control: MANUAL Remote Control":"camera_control: AUTOMATIC Control");
	}

	return true;
}


/*
 * Send SBUS frames
 */
void timerCallback_write(const ros::TimerEvent& event)
{
	static int write_error_cnt;
	unsigned int SBUSwriteData[SBUS_NUMBER_CHANNELS];

	/* initialize with BMMCC_MID_OFFSET << 3 */
	memset(SBUSwriteData, 1024, sizeof SBUSwriteData);

	checkBMMCCcmd();

	for(int i=0; i<NFUNCTIONS; i++)
		SBUSwriteData[SBUSch[i]-1] = BMMCCdata[i] << 3;

	/* If manual control then write what was read */
	if(cs.manual_ctrl)
		for(int i=0; i<SBUS_NUMBER_CHANNELS; i++)
			SBUSwriteData[i] = SBUSreadData[i];

	if (writesbus(pfd[0].fd, SBUSwriteData)){

		cs.trigger_record = (unsigned char) (SBUSwriteData[SBUSch[RECORD_CH]-1] >> 3);
		cs.iris = (unsigned char) (SBUSwriteData[SBUSch[IRIS_CH]-1] >> 3);
		cs.focus = (unsigned char) (SBUSwriteData[SBUSch[FOCUS_CH]-1] >> 3);
		cs.auto_focus = (unsigned char) (SBUSwriteData[SBUSch[AFOCUS_CH]-1] >> 3);
		cs.zoom = (unsigned char) (SBUSwriteData[SBUSch[ZOOM_CH]-1] >> 3);
		cs.iso = (unsigned char) (SBUSwriteData[SBUSch[ISO_CH]-1] >> 3);
		cs.shutter_angle = (unsigned char) (SBUSwriteData[SBUSch[SHUTTER_CH]-1] >> 3);
		cs.white_balance = (unsigned char) (SBUSwriteData[SBUSch[WHITE_CH]-1] >> 3);
		cs.audio_levels = (unsigned char) (SBUSwriteData[SBUSch[AUDIO_CH]-1] >> 3);
		cs.frame_rate = (unsigned char) (SBUSwriteData[SBUSch[FRAME_CH]-1] >> 3);
		cs.codec = (unsigned char) (SBUSwriteData[SBUSch[CODEC_CH]-1] >> 3);

		write_error_cnt = 0;
		cs.SBUS_write_error = false;
	}
	else{
		write_error_cnt ++;

		if( write_error_cnt > SBUS_WR_FREQ) /* 1s of consecutive write error */
			cs.SBUS_write_error = true;
	}
}

/*
 * Publish camera_status
 */
void timerCallback_camera_status(const ros::TimerEvent& event)
{
	cs.header.stamp = ros::Time::now();

	cstatus_publisher.publish(cs);
}

void main_with_exceptions(std::string &port_name)
{
	const uint16_t vendor_id = 0x403;
	const uint16_t product_id = 0x6001;

    libusbp::device device = libusbp::find_device_with_vid_pid(vendor_id, product_id);
    if (!device)
    {
        std::cerr << "Device not found." << std::endl;
    }
		else{
			libusbp::serial_port port(device);//, interface_number, composite);
	    port_name = port.get_name();
		}
}


int main(int argc, char **argv)
{

	static int drone_id_;
	static std::string portName_;
	static int rcstatus_offline_count;

	ros::init(argc, argv, "camera_control");
	ros::NodeHandle n;
	ros::NodeHandle pnh("~");

	pnh.param<int>("drone_id", drone_id_, 1);
	// pnh.param<std::string>("portName", portName_, "/dev/ttyUSB0");

	/*Map SBUS channel number with camera functions*/
	pnh.param<int>("SBUS_RECORD_CH", SBUSch[RECORD_CH], DEFAULT_SBUS_RECORD_CH);
	pnh.param<int>("SBUS_IRIS_CH", SBUSch[IRIS_CH], DEFAULT_SBUS_IRIS_CH);
	pnh.param<int>("SBUS_FOCUS_CH", SBUSch[FOCUS_CH], DEFAULT_SBUS_FOCUS_CH);
	pnh.param<int>("SBUS_AFOCUS_CH", SBUSch[AFOCUS_CH], DEFAULT_SBUS_AFOCUS_CH);
	pnh.param<int>("SBUS_ZOOM_CH", SBUSch[ZOOM_CH], DEFAULT_SBUS_ZOOM_CH);
	pnh.param<int>("SBUS_ISO_CH", SBUSch[ISO_CH], DEFAULT_SBUS_ISO_CH);
	pnh.param<int>("SBUS_SHUTTER_ANGLE_CH", SBUSch[SHUTTER_CH], DEFAULT_SBUS_SHUTTER_CH);
	pnh.param<int>("SBUS_WHITE_BALANCE_CH", SBUSch[WHITE_CH], DEFAULT_SBUS_WHITE_CH);
	pnh.param<int>("SBUS_AUDIO_CH", SBUSch[AUDIO_CH], DEFAULT_SBUS_AUDIO_CH);
	pnh.param<int>("SBUS_FRAME_RATE_CH", SBUSch[FRAME_CH], DEFAULT_SBUS_FRAME_CH);
	pnh.param<int>("SBUS_CODEC_CH", SBUSch[CODEC_CH], DEFAULT_SBUS_CODEC_CH);

	/*Select camera starting functions values*/
	pnh.param<int>("BMMCC_IRIS_INIT", BMMCCdata[IRIS_CH], DEFAULT_BMMCC_IRIS);
	pnh.param<int>("BMMCC_FOCUS_INIT", BMMCCdata[FOCUS_CH], DEFAULT_BMMCC_FOCUS);
	pnh.param<int>("BMMCC_AUDIO_INIT", BMMCCdata[AUDIO_CH], DEFAULT_BMMCC_AUDIO);
	pnh.param<int>("BMMCC_FRAME_RATE_INIT", BMMCCdata[FRAME_CH], DEFAULT_BMMCC_FRAME);
	pnh.param<int>("BMMCC_CODEC_INIT", BMMCCdata[CODEC_CH], DEFAULT_BMMCC_CODEC);
	BMMCCdata[RECORD_CH] = DEFAULT_BMMCC_RECORD;
	BMMCCdata[AFOCUS_CH] = DEFAULT_BMMCC_AFOCUS;
	BMMCCdata[ZOOM_CH] = DEFAULT_BMMCC_ZOOM;
	BMMCCdata[ISO_CH] = DEFAULT_BMMCC_ISO;
	BMMCCdata[SHUTTER_CH] = DEFAULT_BMMCC_SHUTTER;
	BMMCCdata[WHITE_CH] = DEFAULT_BMMCC_WHITE;

//Timers
	ros::Timer write_timer = n.createTimer(ros::Duration(1.0/SBUS_WR_FREQ), timerCallback_write);
	// ros::Timer publish_timer = n.createTimer(ros::Duration(1.0/PUBLISH_STATUS_FREQ), timerCallback_camera_status);

//Publisher
	cstatus_publisher = n.advertise<multidrone_msgs::CameraStatus>("drone_"+std::to_string(drone_id_)+"/camera_status", 1);

//Service Server
	ros::ServiceServer camera_control_service = n.advertiseService("drone_"+std::to_string(drone_id_)+"/camera_control",cameracontrolServiceCallback);

	pfd[0].events = POLLIN;


	while (ros::ok()) {
		ros::spinOnce();
		main_with_exceptions(portName_);
		pfd[0].fd = serial_open(portName_.c_str());

		if (pfd[0].fd < 0){
			sleep(5);
			continue;
		}

		ROS_INFO("camera_control: connected at port  %s", portName_.c_str());

		ros::Rate r(30);
		while (ros::ok()) {
			ros::spinOnce();
			if (!poll(pfd, 1, 1)){
				rcstatus_offline_count++;
				if (rcstatus_offline_count > 3000) /*~3s without read serial comm*/
					cs.rcstatus_connected = false;
			}
			if (pfd[0].revents){
				if(readsbus(pfd[0].fd)){
					cs.rcstatus_connected = true;
					rcstatus_offline_count = 0;
				}
			}

			/* Disconnected? Reopen the interface*/
			if (cs.SBUS_write_error){
				cs.rcstatus_connected = false;
				ROS_ERROR("camera_control: SBUS write error");
				break;
			}
			cstatus_publisher.publish(cs);
			r.sleep();
		}
		close(pfd[0].fd);
	}

	return 0;
}
