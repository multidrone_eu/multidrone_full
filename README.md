# MULTIDRONE FULL #

This repository contains all the code for running the system designed for the [MULTIDRONE project](https://multidrone.eu) in the ground station and in the drones.

[This website](http://www.aiia.csd.auth.gr/LAB_PROJECTS/MULTIDRONE/AUTH_MULTIDRONE_Software.html) describes the main functionalities and involved modules of the system.

The code is free for academic, R&D and non-commercial use only. Licensing is required for commercial exploitation.

### How do I get set up? ###

```
git clone https://bitbucket.org/multidrone_eu/multidrone_full.git
```

### Dependencies ###

* [grvc-ual](https://github.com/grvcTeam/grvc-ual) at tag [v2.1](https://github.com/grvcTeam/grvc-ual/tree/v2.1)
* [geodesy (from geographic_info)](http://wiki.ros.org/geodesy): `sudo apt-get install ros-kinetic-geographic-info`
* libgstreamer: `sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev`
* Visual nodes dependencies: follow instructions in common/build_utils/build_vision_instructions.md
* libusbp : https://github.com/pololu/libusbp
