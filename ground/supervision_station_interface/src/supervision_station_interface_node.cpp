/**
 * MULTIDRONE Project:
 * 	Prototyping functions to test the overall system specifications.
 *
 * Supervision Station interface node
 *
**/

#include <ros/ros.h>
#include <multidrone_msgs/ActionStatus.h>
#include <multidrone_msgs/TargetStateArray.h>
#include <geometry_msgs/PoseStamped.h>
// #include <multidrone_msgs/Stream.h> //!TODO: Change for the new message
#include <multidrone_msgs/SupervisorAlarm.h>
#include <multidrone_msgs/DroneTelemetry.h>

static ros::Subscriber action_status_sub;
static ros::Subscriber target_sub;
static ros::Subscriber drone_pose_sub;
// static ros::Subscriber stream_sub;
static ros::Subscriber drone_telemetry_sub;

static ros::ServiceClient supervisor_alarm_srv;


static geometry_msgs::Point	target_pos_msg;


void targetCallback(const multidrone_msgs::TargetStateArray::ConstPtr& msg)
{
	ROS_INFO("Message from target.");
	// TODO: update current info on target
	// target_pos_msg.x = 0;
	// target_pos_msg.y = 1;
	// target_pos_msg.z = msg->geo_position.altitude;
}

void actionStatusCallback(const multidrone_msgs::ActionStatus::ConstPtr& msg)
{
	ROS_INFO("Action status received.");
	// TODO:
}

void dronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
	ROS_INFO("Drone pose received.");
	// TODO:
}

// void streamCallback(const multidrone_msgs::Stream::ConstPtr& msg)
// {
// 	ROS_INFO("Drone camera stream received");
// 	// TODO:
// }

void droneTelemetryCallback(const multidrone_msgs::DroneTelemetry::ConstPtr& msg)
{
	ROS_INFO("Drone telemetry received");
	// TODO:
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "supervision_station_node");
	ros::NodeHandle n;

	// Subscribers
	target_sub = n.subscribe("targets_pose", 10, targetCallback);
	action_status_sub = n.subscribe("drone_X/action_status", 10, actionStatusCallback);
	drone_pose_sub = n.subscribe("drone_X/ual/pose", 10, dronePoseCallback);
	// stream_sub = n.subscribe("drone_X/stream", 1000, streamCallback);	
	drone_telemetry_sub = n.subscribe("drone_X/full_telemetry", 10, droneTelemetryCallback);

	//Publishers


	//Service Clients
	supervisor_alarm_srv = n.serviceClient<multidrone_msgs::SupervisorAlarm>("execution/alarm");
	// TODO: send alarm request
	multidrone_msgs::SupervisorAlarm srv;
	srv.request.header.stamp = ros::Time::now();
	if (supervisor_alarm_srv.call(srv))
		ROS_INFO_STREAM("Supervisor alarm response received");
	else
		ROS_WARN("Supervisor alarm response failed");

	ros::spin();

	return 0;
}
