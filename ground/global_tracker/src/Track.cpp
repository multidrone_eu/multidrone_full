#include <Track.h>

// Constructor
Track::Track(){
}
Track::Track(pairing pairings0, geometry_msgs::Point state0, unsigned int time_since_last_pairing0, unsigned int life_time0){
    pairings = pairings0;
    state = state0;
    time_since_last_pairing = time_since_last_pairing0;
    life_time = life_time0;

    // Inicialización de ventana de tiempo de estados 
    // El estado pasado como argumento es el primer elemento de la ventana
    stateTW.push_back(state0);
}
// Destructor
Track::~Track(){}

// Añade un nuevo estado a la ventana de tiempo de estados de un track de la lista de tracks
// Cuando la ventana de tiempo de estados está llena se van borrando los estados antiguos
void Track::pushStateTimeWindow(geometry_msgs::Point state, int asociation_window){
    // Se añade al inicio del vector la nueva tabla
    stateTW.push_front(state);
    // Si se ha superado el tamaño máximo para la ventana de tiempo se borra el último elemento
    if(stateTW.size() > asociation_window){
        stateTW.pop_back();
    }
}