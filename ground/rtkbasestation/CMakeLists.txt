cmake_minimum_required(VERSION 2.8.3)
project(rtkbasestation)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
	roscpp
	std_msgs
	mavros_msgs
	cmake_modules)

catkin_package(
	CATKIN_DEPENDS roscpp)

include_directories(${catkin_INCLUDE_DIRS})

add_executable(rtkbase src/rtcm_mavros.cpp)

target_link_libraries( rtkbase
  ${catkin_LIBRARIES}
  usbp-1)
  
